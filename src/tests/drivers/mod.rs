/*
 * Copyright (c) 2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes tests for built-in drivers
 */


mod null;
mod portfs;

use ::alloc::sync::Arc;
use ::alloc::vec::Vec;
use usync::RwLock;
use crate::task::thread::SysThread;

pub fn run_driver_tests(_threads: &mut Vec<Arc<RwLock<SysThread>>>){
	null::test_null_devs();
	portfs::test_portfs_rpc();
}
/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
