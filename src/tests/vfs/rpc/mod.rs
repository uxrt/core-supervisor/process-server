/*
 * Copyright (c) 2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module tests the VFS RPC layer.
 */

pub mod client;
mod resmgr;

use alloc::sync::Arc;
use alloc::vec::Vec;
use usync::RwLock;

use crate::task::thread::SysThread;
use self::client::run_vfs_rpc_client_tests;
use self::resmgr::run_vfs_resmgr_tests;

pub fn run_vfs_rpc_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	run_vfs_rpc_client_tests();
	run_vfs_resmgr_tests(threads);
}
