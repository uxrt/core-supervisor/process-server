/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module tests the client side of the VFS RPC layer.
 */

use alloc::string::String;
use alloc::vec::Vec;
use core::str;
use core::mem::size_of;

use zerocopy::{
	AsBytes,
	FromBytes,
};

use uxrt_transport_layer::{
	FileDescriptorRef,
	FileDescriptor,
	IOError,
	SRV_ERR_2BIG,
	SRV_ERR_BADF,
	SRV_ERR_INVAL,
	SRV_ERR_NOENT,
	SRV_ERR_NOTDIR,
	ServerStatus,
};
use uxrt_vfs_rpc::{
	AT_FDCWD,
	AT_FDINVALID,
	O_DIRECTORY,
	O_RDONLY,
	RESOLVE_EMPTY_PATH,
	S_IFCHR,
	S_IFDIR,
	S_IFMT,
	VFSCommonHeader,
	gid_t,
	mode_t,
	off_t,
	open_how,
	timespec,
	rpc_new_bufs_open,
	rpc_init_open,
	stat,
	uid_t,
};

use uxrt_vfs_rpc::client::ClientMessage;

use uxrt_vfs_rpc_client::VFSRPCClient;

use uxrt_vfs_rpc_client::dir::DirClient;

use crate::vfs::rpc::get_default_rpc_client;

fn check_server_error(res: IOError, expected: ServerStatus, fn_name: &str) -> Result<(), ()> {
	let errno = if let IOError::ServerError(e) = res {
		e
	}else{
		0
	};

	if res != IOError::ServerError(expected){
		 warn!("unexpected result from {}: {}, {}", fn_name, res, errno);
		Err(())
	}else{
		Ok(())
	}
}

fn test_open_10<T: FileDescriptor>(client: &VFSRPCClient<T>, how: &open_how) -> Result<usize, IOError>{
	let mut bufs = rpc_new_bufs_open!();
	let how_buf = how.as_bytes();
	rpc_init_open!(bufs, -1, "/foo".as_bytes(), &how_buf[0..how_buf.len() - 8]);
	client.send_msg(&mut bufs)
}

fn test_open_11<T: FileDescriptor>(client: &VFSRPCClient<T>) -> Result<usize, IOError>{
	let mut bufs = rpc_new_bufs_open!();
	let how_buf = &[0u8; size_of::<open_how>() + 8];
	rpc_init_open!(bufs, -1, "/foo".as_bytes(), &how_buf[..]);
	client.send_msg(&mut bufs)
}

fn test_open_12<T: FileDescriptor>(client: &VFSRPCClient<T>) -> Result<usize, IOError>{
	let mut bufs = rpc_new_bufs_open!();
	let how_buf = &[1u8; size_of::<open_how>() + 8];
	rpc_init_open!(bufs, -1, "/foo".as_bytes(), &how_buf[..]);
	client.send_msg(&mut bufs)
}

fn test_open_13<T: FileDescriptor>(client: &VFSRPCClient<T>) -> Result<usize, IOError>{
	let mut how_buf_ext = [1u8; size_of::<open_how>() + 8];
	for i in size_of::<open_how>()..how_buf_ext.len() {
		how_buf_ext[i] = 0;
	}
	let mut bufs = rpc_new_bufs_open!();
	rpc_init_open!(bufs, -1, "/foo".as_bytes(), &how_buf_ext[..]);
	let res = client.send_msg(&mut bufs);
	res
}

fn test_open(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running open tests");
	info!("running open test 0");
	let how = open_how {
		flags: 0xdeadbeef,
		mode: 0o755,
		resolve: 0x12345678,
	};
	let mut res = client.openat2(-1, "/proc/fscontext/self/ports/server/1000000000".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 0 failed with unexpected error");

	info!("running open test 1");
	res = client.openat2(-1, "/proc/fscontext/self/ports/server/foo".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 1 failed with unexpected error");

	info!("running open test 2");
	res = client.openat2(-1, "/proc/fscontext/self/ports/server/bar".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 2 failed with unexpected error");

	info!("running open test 3");
	res = client.openat2(-1, "/proc/fsscontext/self/portsfoo".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 3 failed with unexpected error");

	info!("running open test 3");
	res = client.openat2(-1, "/proc/fsscontext/self/portsfoo/bar".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 4 failed with unexpected error");

	info!("running open test 4");
	res = client.openat2(-1, "/foo".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 5 failed with unexpected error");

	info!("running open test 5");
	res = client.openat2(-1, "/foobar".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 5 failed with unexpected error");

	info!("running open test 6");
	res = client.openat2(-1, "/foo/bar".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 6 failed with unexpected error");

	info!("running open test 7");
	res = client.openat2(-1, "foo".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 7 failed with unexpected error");

	info!("running open test 8");
	res = client.openat2(-1, "proc/fsspace/self/ports/0".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 8 failed with unexpected error");

	info!("running open test 9");
	res = client.openat2(-1, "/bar".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 9 failed with unexpected error");

	info!("running open test 10");
	res = test_open_10(&client, &how).expect_err("open test 10 falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 10 failed with unexpected error");

	info!("running open test 11");
	res = test_open_11(&client).expect_err("open test 11 falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 11 failed with unexpected error");

	info!("running open test 12");
	res = test_open_12(&client).expect_err("open test 12 falsely succeeded");
	check_server_error(res, SRV_ERR_2BIG, "openat2").expect("open test 12 failed with unexpected error");

	info!("running open test 13");
	res = test_open_13(&client).expect_err("open test 13 falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 13 failed with unexpected error");

	info!("running open test 14");
	res = client.openat2(AT_FDCWD, "/:hide".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 14 failed with unexpected error");

	info!("running open test 16");
	res = client.openat2(AT_FDCWD, "/:hide/foo".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 16 failed with unexpected error");

	info!("running open test 17");
	res = client.openat2(AT_FDCWD, "".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 17 failed with unexpected error");

	info!("running open test 18");
	res = client.openat2(AT_FDINVALID, "".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "openat2").expect("open test 17 failed with unexpected error");

	info!("running open test 19");
	res = client.openat2(AT_FDCWD, "/dev/full/".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOTDIR, "openat2").expect("open test 19 failed with unexpected error");

	info!("running open test 20");
	res = client.openat2(AT_FDCWD, "/dev/full/.".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOTDIR, "openat2").expect("open test 20 failed with unexpected error");

	info!("running open test 21");
	res = client.openat2(AT_FDCWD, "/dev/full/./".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOTDIR, "openat2").expect("open test 21 failed with unexpected error");

	info!("running open test 22");
	res = client.openat2(AT_FDCWD, "/dev/full/./.".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOTDIR, "openat2").expect("open test 22 failed with unexpected error");

	info!("running open test 23");
	res = client.openat2(AT_FDCWD, "/dev/full/././".as_bytes(), &how).expect_err("openat2 test falsely succeeded");
	check_server_error(res, SRV_ERR_NOTDIR, "openat2").expect("open test 23 failed with unexpected error");
}

fn test_openfd(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running openfd tests");
	let res = client.openfd(1, 0xabcdef).expect("openfd test failed");
	assert!(res == 0xabcd, "unexpected result from openfd: {}", res);
}

fn test_dup(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running dup tests");
	let res = client.dup3(i32::MAX - 1, i32::MAX, 0).expect_err("dup3 test failed");
	check_server_error(res, SRV_ERR_BADF, "dup3").unwrap();
}

fn test_close(_client: &VFSRPCClient<FileDescriptorRef>){
	/*info!("running close tests");
	client.close(0).expect("close test failed");*/
}

fn test_fcntl(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running fcntl tests");
	let mut buf: [u8; 1] = [0];
	let res = client.fcntl(0, 1, &[], &mut buf).expect("fcntl test failed");
	assert!(res == 1, "unexpected message size from fcntl: {}", res);
	assert!(buf[0] == 0xab, "unexpected result from fcntl: {}", buf[0]);
}

/*fn test_fd2path(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running fd2path tests");
	let mut path = [0u8; 16];
	let res = client.fd2path(0, &mut path).expect("fd2path test failed");
	assert!(res == 4, "unexpected result from fd2path: {}", res);
	let path_utf8 = str::from_utf8(&path[0..res]).expect("non-UTF-8 sequence in path");
	assert!(path_utf8 == "/foo", "unexpected path returned from fd2path: {} (len: {})", path_utf8, path_utf8.len());

}*/

fn check_stat_buf(path: &str, statbuf: &stat, uid: uid_t, gid: gid_t, mode: u32) {
	info!("stat for {}:", path);
	info!("dev: {}", statbuf.st_dev);
	info!("ino: {}", statbuf.st_ino);
	info!("mode: {:o}", statbuf.st_mode);
	info!("nlink: {}", statbuf.st_nlink);
	info!("uid: {}", statbuf.st_uid);
	info!("gid: {}", statbuf.st_gid);
	info!("rdev: {}", statbuf.st_rdev);
	info!("size: {}", statbuf.st_size);
	info!("blksize: {}", statbuf.st_blksize);
	info!("blocks: {}", statbuf.st_blocks);
	info!("atim: {} {}", statbuf.st_atim.tv_sec, statbuf.st_atim.tv_nsec);
	info!("mtim: {} {}", statbuf.st_mtim.tv_sec, statbuf.st_mtim.tv_nsec);
	info!("ctim: {} {}", statbuf.st_ctim.tv_sec, statbuf.st_ctim.tv_nsec);

	assert!(statbuf.st_ino != 0, "inode number for {} is zero", path);
	assert!(statbuf.st_uid == uid, "unexpected uid {} for {} (should be {})", statbuf.st_uid, path, uid);
	assert!(statbuf.st_gid == gid, "unexpected gid {} for {} (should be {})", statbuf.st_gid, path, gid);
	assert!(statbuf.st_mode == mode as mode_t, "unexpected mode {:o} for {} (should be {:o})", statbuf.st_mode, path, mode);
}

pub fn test_stat_single(client: &VFSRPCClient<FileDescriptorRef>, path: &str, mode: u32){
	//TODO: check that the inode and port match between calls
	info!("running stat tests on {}", path);
	let mut statbuf = Default::default();
	info!("opening {}", path);
	let f = client.open(path.as_bytes(), O_RDONLY as u64).expect("open() failed");
	info!("fstat()ing FD");
	client.fstat(f, &mut statbuf).expect("fstat() failed");
	check_stat_buf(path, &statbuf, 0, 0, mode);
	info!("closing FD");
	client.close(f).expect("close() failed");
	info!("stat()ing {}", path);
	client.stat(path.as_bytes(), &mut statbuf).expect("stat() failed");
	check_stat_buf(path, &statbuf, 0, 0, mode);
}

fn test_stat(client: &VFSRPCClient<FileDescriptorRef>){
	test_stat_single(client, "/dev/null", S_IFCHR | 0o666);
	test_stat_single(client, "/dev", S_IFDIR | 0o755);
}

fn test_readlink(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running readlink tests");
	let mut path = [0u8; 16];
	let res = client.readlinkat(AT_FDCWD, "/bar".as_bytes(), &mut path).expect_err("readlinkat test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "readlinkat").expect("readlink test 0 failed with unexpected error");
	let res = client.readlinkat(AT_FDCWD, "/dev".as_bytes(), &mut path).expect_err("readlinkat test falsely succeeded");
	check_server_error(res, SRV_ERR_INVAL, "readlinkat").expect("readlink test 1 failed with unexpected error");
	let res = client.readlinkat(AT_FDCWD, "/dev/null".as_bytes(), &mut path).expect_err("readlinkat test falsely succeeded");
	check_server_error(res, SRV_ERR_INVAL, "readlinkat").expect("readlink test 2 failed with unexpected error");
}

pub fn test_chmod_single(client: &VFSRPCClient<FileDescriptorRef>, path: &str, orig_mode: u32){
	info!("testing fchmod on {}", path);
	let file_type = orig_mode & S_IFMT;
	let mut statbuf = Default::default();
	info!("opening {}", path);
	let f = client.open(path.as_bytes(), O_RDONLY as u64).expect("open() failed");
	info!("fchmod()ing FD for {}", path);
	client.fchmod(f, 0o777).expect("fchmod() failed");
	info!("fstat()ing FD for {} to check mode", path);
	client.fstat(f, &mut statbuf).expect("fstat() failed");
	check_stat_buf(path, &statbuf, 0, 0, file_type as u32 | 0o777);
	info!("closing FD for {}", path);
	client.close(f).expect("close() failed");

	info!("testing chmod on {}", path);
	client.chmod(path.as_bytes(), (orig_mode & !S_IFMT) as mode_t).expect("chmod() failed");

	client.stat(path.as_bytes(), &mut statbuf).expect("stat() failed");
	check_stat_buf(path, &statbuf, 0, 0, orig_mode);
}

fn test_chmod(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running chmod tests");

	test_chmod_single(client, "/dev/null", S_IFCHR | 0o666);
	test_chmod_single(client, "/dev", S_IFDIR | 0o755);
}

fn test_chown(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running chown tests");
	test_chown_single(client, "/dev/null", S_IFCHR | 0o666);
	test_chown_single(client, "/dev", S_IFDIR | 0o755);
}

pub fn test_chown_single(client: &VFSRPCClient<FileDescriptorRef>, path: &str, mode: u32){
	info!("testing fchown() on {}", path);
	let mut statbuf = Default::default();
	info!("opening {}", path);
	let f = client.open(path.as_bytes(), O_RDONLY as u64).expect("open() failed");
	info!("fchown()ing FD for {}", path);
	client.fchown(f, 1, 1).expect("fchown() failed");
	client.fstat(f, &mut statbuf).expect("fstat() failed");
	check_stat_buf(path, &statbuf, 1, 1, mode);
	info!("closing FD for {}", path);
	client.close(f).expect("close() failed");

	info!("chown()ing {}", path);
	client.chown(path.as_bytes(), 0, 0).expect("chmod() failed");

	client.stat(path.as_bytes(), &mut statbuf).expect("stat() failed");
	check_stat_buf(path, &statbuf, 0, 0, mode);
}

fn test_utimens(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running utimens tests");
	let mut times: [timespec; 2] = Default::default();
	times[0].tv_sec = 0xdeadbeef;
	times[0].tv_nsec = 0x87654321;
	times[1].tv_sec = 0xbeefdead;
	times[1].tv_nsec = 0x12345678;

	client.futimens(1, &times).expect("utimensat test failed");
}

fn test_link(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running link tests");
	client.linkat(1, "/foo".as_bytes(), 2, "/bar".as_bytes(), 1234).expect("linkat test failed");
}

fn test_symlink(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running symlink tests");
	client.symlinkat("/foo".as_bytes(), 2, "/bar".as_bytes()).expect("symlinkat test failed");
}

fn test_mknod(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running mknod tests");
	client.mknodat(1, "/foo".as_bytes(), 1234, 5678).expect("mknodat test failed");
}

fn test_rename(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running rename tests");
	client.renameat2(1, "/foo".as_bytes(), 2, "/bar".as_bytes(), 1234).expect("renameat2 test failed");
}

fn test_unlink(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running unlink tests");
	let res = client.unlinkat(1, "/foo".as_bytes(), 1234).expect_err("unlinkat test falsely succeeded");
	check_server_error(res, SRV_ERR_NOENT, "unlinkat").unwrap();
}

/*fn test_chdir(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running chdir tests");
	client.chdir("/foo".as_bytes()).expect("chdir test failed");
}

fn test_fchdir(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running fchdir tests");
	client.fchdir(1).expect("fchdir test failed");
}*/

fn test_getpages(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running getpages tests");
	client.getpages(1, 0xdeadbeef, 64).expect("getpages test failed");
}

fn test_mount(client: &VFSRPCClient<FileDescriptorRef>){
	info!("running mount tests");
	client.mount(1, "/foo".as_bytes(), "bar".as_bytes(), "baz=qux".as_bytes()).expect("mount test failed");
}

fn name_list_contains(read: &[String], expected: &str) -> bool {
	for i in 0..read.len() {
		if read[i] == expected {
			return true;
		}
	}
	false
}

fn test_readdir(dir_client: &mut DirClient, keys: &mut Vec<off_t>, names: &mut Vec<String>, expect_present: &[&str], expect_absent: &[&str]){
	info!("contents:");
	while let Some(entry) = dir_client.readdir().expect("failed to read directory entry") {
		let utf8_name = String::from(str::from_utf8(entry.get_name()).expect("invalid directory entry name"));
		info!("found entry: {} (pos: {})", utf8_name, entry.d_off);
		assert!(!keys.contains(&entry.d_off), "duplicate key/offset found in directory");
		assert!(!names.contains(&utf8_name), "duplicate name found in directory");
		keys.push(entry.d_off);
		names.push(utf8_name);
	}
	for i in 0..expect_present.len() {
		assert!(name_list_contains(names, &expect_present[i]), "expected name {} absent", expect_present[i]);
	}
	for i in 0..expect_absent.len() {
		assert!(!name_list_contains(names, &expect_absent[i]), "expected name {} present", expect_absent[i]);
	}
}

pub fn test_dir_inner(dir_client: &mut DirClient, expect_present: &[&str], expect_absent: &[&str]){
	let mut names = Vec::new();
	let mut keys = Vec::new();
	test_readdir(dir_client, &mut keys, &mut names, expect_present, expect_absent);
	info!("rewinding directory");
	dir_client.rewinddir();
	let mut names = Vec::new();
	let mut keys = Vec::new();
	names.clear();
	keys.clear();
	test_readdir(dir_client, &mut keys, &mut names, expect_present, expect_absent);
	info!("seeking directory to {}", keys.len() / 2);
	dir_client.seekdir(keys[keys.len() / 2]);
	names.clear();
	keys.clear();
	//expect_present isn't checked here because there's no guarantee the
	//names will be present in the part that gets read
	test_readdir(dir_client, &mut keys, &mut names, &[], expect_absent);
}

pub fn test_dir(dir: &str, expect_present: &[&str], expect_absent: &[&str]){
	info!("running directory tests on {}", dir);
	let mut dir_client = DirClient::opendir(dir.as_bytes()).expect("failed to open directory");
	test_dir_inner(&mut dir_client, expect_present, expect_absent);
	let how = open_how {
		flags: (O_RDONLY | O_DIRECTORY) as u64,
		mode: 0,
		resolve: RESOLVE_EMPTY_PATH as u64,
	};
	info!("re-opening directory FD with empty path");
	let dirfd = dir_client.dirfd();
	let rpc = get_default_rpc_client();
	let new_dirfd = rpc.openat2(dirfd, &[], &how).expect("could not reopen directory with empty pathname");
	dir_client = DirClient::fdopendir(new_dirfd).expect("could not create directory descriptor for reopened directory");
	test_dir_inner(&mut dir_client, expect_present, expect_absent);
}

fn test_hide_dir_fail(){
	info!("trying to open hide mount as a directory");
	if let Err(err) = DirClient::opendir("/:hide".as_bytes()) {
		assert!(err.to_errno() == SRV_ERR_NOENT, "opening /:hide failed with unexpected error {}", err.to_errno());
	}else{
		panic!("opening /:hide falsely succeeded");
	}
}

pub fn run_vfs_rpc_client_tests(){
	let client = get_default_rpc_client();

	test_open(&client);
	test_openfd(&client);
	test_dup(&client);
	test_close(&client);
	test_fcntl(&client);
	//test_fd2path(&client);
	test_stat(&client);
	test_readlink(&client);
	test_chmod(&client);
	test_chown(&client);
	test_utimens(&client);
	test_link(&client);
	test_symlink(&client);
	test_mknod(&client);
	test_rename(&client);
	test_unlink(&client);
	/*test_chdir(&client);
	test_fchdir(&client);*/
	test_getpages(&client);
	test_mount(&client);

	let expect_names = ["dmesg", "full", "null", "zero"];
	test_dir("/dev", &expect_names, &[]);
	test_dir("/dev/", &expect_names, &[]);
	test_dir("/dev/.", &expect_names, &[]);
	test_dir("/dev/./", &expect_names, &[]);
	test_dir("/dev/./.", &expect_names, &[]);
	test_dir("/dev/././", &expect_names, &[]);
	test_hide_dir_fail();
}
