/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes core code for testing the VFS and transport layer.
 */

use alloc::vec::Vec;
use alloc::collections::{BTreeMap, BTreeSet};
use alloc::sync::Arc;
use core::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use core::mem::size_of;
use usync::{Mutex, RwLock};

use sel4_thread::{
	ThreadReturn,
	WrappedThread,
};

use sel4::{
	CapRights,
	ErrorDetails,
	PAGE_BITS,
	PAGE_SIZE,
	seL4_CPtr,
};

use sel4_alloc::AllocatorBundle;
use sel4_alloc::utspace::UtZone;
use sel4_alloc::vspace::{
	VSpaceManager,
	PageDeallocType,
};

use sel4_alloc::VSpaceReservation;
use sel4_alloc::vspace::HierReservation;

use uxrt_procfs_client::{
	PORT_CHANNEL_GET_RECV_TOTAL_SIZE,
	PORT_CHANNEL_GET_REPLY_SIZE,
};

use uxrt_procfs_client::port::{
	PortFSClient,
	PortType,
};

use uxrt_transport_layer::{
	AccessMode,
	FIXED_BUF_MAX_SIZE,
	FileDescriptor,
	HEADER_SIZE,
	IOError,
	MsgHeader,
	MsgType,
	Offset,
	OffsetType,
	RecvLongMsgBuffer,
	SendLongMsgBuffer,
	ServerChannelOptions,
	ServerStatus,
	UnsafeArray,
};

use crate::vfs::rpc::get_default_rpc_client;
use crate::global_heap_alloc::get_kobj_alloc;
use crate::task::thread::SysThread;

use crate::vfs::transport::get_fd;

use crate::{
	dump_heap,
	dump_utspace,
};

use super::super::{
	SERVER_FIRST,
	USE_MD_ACCESSORS,
	allocate_fds,
};
use super::{
	channel_test_wait,
	run_channel_test,
	run_channel_test_closure,
	run_channel_test_send_size_only,
};
use super::random::run_random_tests;

const SUSPEND_TEST_BUF_SIZE: usize = 10485760;
static SERVER_SUSPEND_CANCELLATIONS: AtomicUsize = AtomicUsize::new(0);
static CLIENT_SUSPEND_CANCELLATIONS: AtomicUsize = AtomicUsize::new(0);

fn check_suspend_error_server(res: Result<usize, IOError>, allow_reply_sequence: bool) -> Result<Result<usize, bool>, IOError> {
	check_suspend_error_base(res, &SERVER_SUSPEND_CANCELLATIONS, allow_reply_sequence)
}

fn check_suspend_error_server_offset(res: Result<(usize, Offset), IOError>, allow_reply_sequence: bool) -> Result<Result<usize, bool>, IOError> {
	check_suspend_error_base(res.and_then(|(size, _)| { Ok(size) }),
				 &SERVER_SUSPEND_CANCELLATIONS, allow_reply_sequence)
}

fn check_suspend_error_client_offset(res: Result<(usize, usize), IOError>) -> Result<Option<(usize, usize)>, IOError> {
	let f = |inner: Result<usize, bool>| {
		if !inner.is_err(){
			Ok(Some((res.unwrap().0, res.unwrap().1)))
		}else{
			Ok(None)
		}
	};
	check_suspend_error_base(res.and_then(|(size, _)| { Ok(size) }),
				 &CLIENT_SUSPEND_CANCELLATIONS, false).and_then(f)
}

fn check_suspend_error_base(res: Result<usize, IOError>, counter: &AtomicUsize, allow_reply_sequence: bool) -> Result<Result<usize, bool>, IOError> {
	let err = match res {
		Ok(s) => return Ok(Ok(s)),
		Err(e) => e,
	};
	match err {
		IOError::SyscallError(sys_err) => {
			let details = if let Some(d) = sys_err.details() {
				d
			}else{
				return Err(err);
			};
			match details {
				ErrorDetails::ThreadCancelled => {
					info!("thread cancellation received");
					counter.fetch_add(1, Ordering::Relaxed);
					Ok(Err(false))
				},
				ErrorDetails::ReplySequenceError => {
					info!("reply sequence error occurred");
					if allow_reply_sequence {
						Ok(Err(true))
					}else{
						Err(err)
					}
				}
				_ => Err(err),
			}
		},
		_ => Err(err),
	}
}

fn channel_suspend_server(channel_id: i32, message_id: i32) {
	info!("channel_suspend_server");

	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);

	let use_md_accessors = USE_MD_ACCESSORS.load(Ordering::Relaxed);

	let (reply_buf_addr, recv_buf_addr) = allocate_extended_bufs(SUSPEND_TEST_BUF_SIZE, SUSPEND_TEST_BUF_SIZE);

	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut read_bufs = unsafe {[
		RecvLongMsgBuffer::new(&mut header_buf),
		RecvLongMsgBuffer::new_raw(
		recv_buf_addr as *mut u8,
		SUSPEND_TEST_BUF_SIZE,
	)]};

	let write_bufs = unsafe {[SendLongMsgBuffer::new_raw(
		reply_buf_addr as *mut u8,
		SUSPEND_TEST_BUF_SIZE,
	)]};

	info!("channel_suspend_server got FD");
	loop {
		info!("channel_suspend_server: receiving message");
		let cw_size = match check_suspend_error_server(
			channel_fd.mreadv(&mut read_bufs[..], message_id), true)
			.expect("server encountered unexpected error when receiving message") {
			Ok(s) => s - size_of::<MsgHeader>(),
			Err(is_sequence) => {
				if is_sequence {
					info!("got sequence error when receiving");
				}
				continue;
			},
		};
		let header = MsgHeader::from_long_bufs(&mut read_bufs[..]).expect("could not get message header").clone();
		let msgtype = header.msgtype_enum().expect("invalid message type returned in header");
		let whence = header.whence_enum().expect("invalid message type returned in header");
		let offset = header.offset;
		info!("test_server_copy_common: got message: {} {} {} {:?} {} {:?}", cw_size, header.client_write_size, header.client_read_size, msgtype, header.offset, whence);
		if msgtype == MsgType::Clunk {
			info!("channel_suspend_server: done");
			return;
		}

		if use_md_accessors {
			info!("channel_suspend_server: reading from message descriptor");
			if check_suspend_error_server(
					message_fd.readv(&mut [RecvLongMsgBuffer::new(&mut read_bufs[1])]), true)
					.expect("could not read from client send buffer").is_err(){
				continue;
			}
		}

		if use_md_accessors {
			info!("channel_suspend_server: writing to message descriptor");
			if check_suspend_error_server_offset(
					message_fd.wpwritev(&write_bufs, 0, OffsetType::Start), true)
					.expect("cannot write to client reply buffer").is_err() {
				continue;
			}
		}

		info!("channel_suspend_server: sending reply");
		if let Err(is_sequence) = check_suspend_error_server_offset(
				message_fd.mwpwritev(&write_bufs, offset as Offset, OffsetType::End, SUSPEND_TEST_BUF_SIZE as ServerStatus), true)
				.expect("failed to reply to message"){
			if is_sequence {
				info!("got sequence error when replying");
			}else{
				continue;
			}
		}
	}
}

static CLIENT_EXITED: AtomicBool = AtomicBool::new(false);

pub fn channel_suspend_client(client_id: i32) -> Option<usize>{
	CLIENT_EXITED.store(false, Ordering::Release);
	let (send_buf_addr, recv_buf_addr) = allocate_extended_bufs(SUSPEND_TEST_BUF_SIZE, SUSPEND_TEST_BUF_SIZE);

	let fd = get_fd(client_id);

	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new_raw(
		recv_buf_addr as *mut u8,
		SUSPEND_TEST_BUF_SIZE
	)]};

	let send_buf_info = unsafe {[SendLongMsgBuffer::new_raw(
		send_buf_addr as *mut u8,
		SUSPEND_TEST_BUF_SIZE
	)]};
	for i in 0..10 {
		info!("channel_suspend_client: sending message: {} {:p}", i, &send_buf_info);
		let res = fd.writereadv(&send_buf_info, &mut recv_buf_info);
		info!("channel_suspend_client: got result: {:?}", res);
		let (send_reply_size, received_reply_size) = match check_suspend_error_client_offset(res).expect("cannot write/read client FD"){
			Some((s, r)) => (s,r),
			None => continue,
		};

		assert!(send_reply_size == SUSPEND_TEST_BUF_SIZE, "server replied with invalid size {} instead of {}", send_reply_size, SUSPEND_TEST_BUF_SIZE);
		assert!(received_reply_size == SUSPEND_TEST_BUF_SIZE, "invalid reply size received: {} instead of {}", received_reply_size, SUSPEND_TEST_BUF_SIZE);
		check_buffer(recv_buf_addr, SUSPEND_TEST_BUF_SIZE);
		info!("client: done");
	}
	CLIENT_EXITED.store(true, Ordering::Release);
	None
}

pub fn run_channel_suspend_test(threads: &mut Vec<Arc<RwLock<SysThread>>>, suspend_client: bool, suspend_server: bool) {
	let (channel_port_id, server_channel_id, client_id, server_message_id, message_port_id) = allocate_fds(PortType::DirectChannel, (PORT_CHANNEL_GET_REPLY_SIZE | PORT_CHANNEL_GET_RECV_TOTAL_SIZE) as u64, AccessMode::ReadWrite);

	CLIENT_SUSPEND_CANCELLATIONS.store(0, Ordering::Relaxed);
	SERVER_SUSPEND_CANCELLATIONS.store(0, Ordering::Relaxed);

	info!("getting threads");
	let mut server = threads[0].write();
	let mut client = threads[1].write();

	let server_wrapper = move || -> Option<ThreadReturn>{
		channel_suspend_server(server_channel_id, server_message_id);
		None
	};

	let client_wrapper = move || -> Option<ThreadReturn>{
		channel_suspend_client(client_id);
		None
	};
	if SERVER_FIRST.load(Ordering::Relaxed) {
		info!("starting server");
		server.run(server_wrapper).expect("could not start server thread");
		channel_test_wait(&client, &server);
		info!("starting client");
		client.run(client_wrapper).expect("could not start client thread");
	}else{
		info!("starting client");
		client.run(client_wrapper).expect("could not start client thread");
		channel_test_wait(&client, &server);
		info!("starting server");
		server.run(server_wrapper).expect("could not start server thread");
	}

	for _ in 0..100{
		sel4::yield_now();
	}
	for i in 0..50000 {
		if suspend_server {
			info!("suspending server: {}", i);
			server.suspend().expect("could not resume server");
			for _ in 0..500{
				sel4::yield_now();
			}
			//info!("resuming server");
			server.resume().expect("could not resume server");
		}
		if suspend_client {
			info!("suspending client: {}", i);
			client.suspend().expect("could not resume client");
			for _ in 0..500{
				sel4::yield_now();
			}
			//info!("resuming client");
			client.resume().expect("could not resume client");
		}
		if CLIENT_EXITED.load(Ordering::Relaxed){
			break;
		}
	}

	info!("waiting for client to exit");
	client.get_exit_endpoint().unwrap().recv_refuse_reply();
	client.suspend().expect("failed to suspend client");
	info!("client done");

	if CLIENT_SUSPEND_CANCELLATIONS.load(Ordering::Relaxed) == 0 &&
		SERVER_SUSPEND_CANCELLATIONS.load(Ordering::Relaxed) == 0 {
		panic!("no cancellations occurred");
	}

	let rpc_client = get_default_rpc_client();
	let portfs_client = PortFSClient::new(None, true);

	info!("deallocating client FD");
	portfs_client.unlink(channel_port_id).expect("cannot unlink channel port");
	rpc_client.close(client_id).expect("cannot close client FD");

	if server.get_sched_context().is_none() {
		info!("binding scheduling context");
		let kobj_alloc = get_kobj_alloc();
		server.new_sched_context(&kobj_alloc).expect("could not create scheduling context");
	}else{
		info!("waiting for server to exit");
		server.get_exit_endpoint().unwrap().recv_refuse_reply();
	}
	info!("suspending server thread");
	server.suspend().expect("cannot suspend server thread");

	rpc_client.close(server_channel_id).expect("cannot close server FD");
	portfs_client.unlink(message_port_id).expect("cannot unlink message port");
	rpc_client.close(server_message_id).expect("cannot close client FD");

	deallocate_extended_test_buffers();
	info!("Heap status after FD deallocation:");
	dump_heap();
	info!("UTSpace status after FD deallocation:");
	dump_utspace();
}

pub fn check_buffer(addr: usize, size: usize){
	if size == 0 {
		return;
	}
	let dummy: u32 = 0;
	let buf: UnsafeArray<u32> = unsafe { UnsafeArray::new(size / size_of::<u32>(), addr, core::mem::size_of::<u32>(), &dummy as *const u32 as usize) };
	for i in 0..size / size_of::<u32>(){
		if buf[i] != i as u32 {
			panic!("buf[{}] has unexpected value {}", i, buf[i]);
		}
	}
	info!("check_buffer done");
}

fn fill_buffer(addr: usize, size: usize){
	//info!("fill_buffer: {:x} {}", addr, size);
	if size == 0 {
		return;
	}
	let dummy: u32 = 0;
	let mut buf: UnsafeArray<u32> = unsafe { UnsafeArray::new(size, addr, core::mem::size_of::<u32>(), &dummy as *const u32 as usize) };
	for i in 0..size / size_of::<u32>(){
		buf[i] = i as u32;
	}
	//info!("fill_buffer done");
}

fn zero_buffer(addr: usize, size: usize){
	//info!("fill_buffer: {:x} {}", addr, size);
	if size == 0 {
		return;
	}
	let dummy: u32 = 0;
	let mut buf: UnsafeArray<u32> = unsafe { UnsafeArray::new(size, addr, core::mem::size_of::<u32>(), &dummy as *const u32 as usize) };
	for i in 0..size / size_of::<u32>(){
		buf[i] = 0;
	}
	//info!("fill_buffer done");
}

fn map_vector(res: &HierReservation){
	let mut vectors = INFO_VECTORS.lock();
	let start_addr = res.start_vaddr();
	let pages = vectors.remove(&start_addr).expect("no vector found");
	drop(vectors);
	let kobj_alloc = get_kobj_alloc();
	info!("map_vector: {:x}", start_addr);
	kobj_alloc.vspace().map_at_vaddr_raw(&pages, start_addr, PAGE_BITS as usize, res, CapRights::all(), 0, &kobj_alloc).expect("cannot map message vector");
}


#[derive(Copy, Clone, PartialEq)]
enum BufferType {
	SendBuffer,
	ReceiveBuffer,
	InfoVector,
}

static BUFFER_TYPES: Mutex<BTreeMap<usize, BufferType>> = Mutex::new(BTreeMap::new());
static BUFFER_ADDRESSES: Mutex<BTreeSet<usize>> = Mutex::new(BTreeSet::new());
static BUFFER_RANGES: Mutex<BTreeMap<usize, usize>> = Mutex::new(BTreeMap::new());
static INFO_VECTORS: Mutex<BTreeMap<usize, Vec<seL4_CPtr>>> = Mutex::new(BTreeMap::new());

pub fn add_fault_range(start: usize, end: usize){
	info!("add_fault_range: {:x} {:x}", start, end);
	BUFFER_RANGES.lock().insert(start, end);
}

pub fn handle_extended_test_page_fault(fault_addr: usize){
	info!("handle_extended_test_page_fault: {:x}", fault_addr);
	let kobj_alloc = get_kobj_alloc();
	let res = kobj_alloc.vspace().get_reservation(fault_addr).expect("no reservation found");

	let res_start = res.start_vaddr();
	let res_end = res.end_vaddr();
	info!("res: {:x} {:x}", res_start, res_end);

	let buffer_type = *BUFFER_TYPES.lock().get(&res_start).expect("unknown buffer address");

	let ranges = BUFFER_RANGES.lock();
	let (range_start, range_size) = if ranges.is_empty() || buffer_type == BufferType::InfoVector {
		info!("no ranges found");
		(res_start, res_end - res_start)
	}else{
		info!("ranges found");
		let mut found_start = 0;
		let mut found_end = 0;
		for (&start, &end) in ranges.range(res_start..res_end+PAGE_SIZE){
			if fault_addr >= start && fault_addr < end {
				found_start = start;
				found_end = end;
				break;
			}
		}
		assert!(found_start != 0, "no range found for {:x}", fault_addr);
		assert!(found_start >= res_start && found_start <= res_end, "range start {:x} out of bounds: {:x} {:x}", found_start, res_start, res_end);
		assert!(found_end >= res_start && found_end <= res_end, "range end {:x} out of bounds: {:x} {:x}", found_end, res_start, res_end);

		(found_start, found_end - found_start)
	};

	info!("range: {:x} {:x}", range_start, range_start + range_size);

	if buffer_type != BufferType::InfoVector {
		kobj_alloc.vspace().allocate_and_map_at_vaddr(range_start, range_size, PAGE_BITS as usize, &res, CapRights::all(), 0, UtZone::RamAny, &kobj_alloc).expect("cannot allocate and map pages for test buffer");
	}
	info!("map done");
	match buffer_type {
		BufferType::SendBuffer => fill_buffer(range_start, range_size),
		BufferType::ReceiveBuffer => zero_buffer(range_start, range_size),
		BufferType::InfoVector => map_vector(&res),
	}
	info!("adding address to list");
	BUFFER_ADDRESSES.lock().insert(res_start);
	info!("fault handled");
}

fn round_up(size: usize) -> usize {
	let round_size = size_of::<usize>() - 1;
	((size + size_of::<usize>() * 2) + round_size) & !round_size
}

pub fn allocate_extended_send_buf_fault(send_size: usize) -> usize {
	let mut ret = 0;
	if send_size > 0 {
		let kobj_alloc = get_kobj_alloc();
		ret = kobj_alloc.vspace().reserve(send_size + PAGE_SIZE, &kobj_alloc).expect("cannot allocate client receive buffer").start_vaddr();
		BUFFER_TYPES.lock().insert(ret, BufferType::SendBuffer);
	}
	ret
}

pub fn allocate_extended_receive_buf_fault(receive_size: usize) -> usize {
	let mut ret = 0;
	if receive_size > 0 {
		let kobj_alloc = get_kobj_alloc();
		ret = kobj_alloc.vspace().reserve(receive_size + PAGE_SIZE, &kobj_alloc).expect("cannot allocate client receive buffer").start_vaddr();
		BUFFER_TYPES.lock().insert(ret, BufferType::ReceiveBuffer);
	}
	ret
}

pub fn allocate_extended_send_buf_no_fault(send_size: usize) -> usize {
	let kobj_alloc = get_kobj_alloc();
	let send_buf_addr = if send_size > 0 {
		kobj_alloc.vspace().allocate_and_map(send_size + PAGE_SIZE, PAGE_BITS as usize, CapRights::all(), 0, UtZone::RamAny, &kobj_alloc).expect("cannot allocate client receive buffer")
	}else{
		0
	};
	if send_buf_addr != 0 {
		fill_buffer(send_buf_addr, send_size);
		BUFFER_ADDRESSES.lock().insert(send_buf_addr);
	}
	send_buf_addr
}


pub fn allocate_extended_receive_buf_no_fault(mut receive_size: usize) -> usize {
	let kobj_alloc = get_kobj_alloc();
	let recv_buf_addr = if receive_size > 0 {
		receive_size = round_up(receive_size);
		kobj_alloc.vspace().allocate_and_map(receive_size + PAGE_SIZE, PAGE_BITS as usize, CapRights::all(), 0, UtZone::RamAny, &kobj_alloc).expect("cannot allocate server receive buffer")
	}else{
		0
	};
	if recv_buf_addr != 0 {
		zero_buffer(recv_buf_addr, receive_size);
		BUFFER_ADDRESSES.lock().insert(recv_buf_addr);
	}
	recv_buf_addr
}

pub fn allocate_extended_vec_base(vec: &[(usize, usize)], enable_fault: bool, mut start_offset: usize) -> usize {
	if vec.len() == 0 {
		return 0
	}
	let kobj_alloc = get_kobj_alloc();
	let size = vec.len() * size_of::<RecvLongMsgBuffer>() + PAGE_SIZE;
	let base_addr = kobj_alloc.vspace().allocate_and_map(size, PAGE_BITS as usize, CapRights::all(), 0, UtZone::RamAny, &kobj_alloc).expect("cannot allocate server receive buffer");
	BUFFER_ADDRESSES.lock().insert(base_addr);

	if start_offset > PAGE_SIZE {
		start_offset = PAGE_SIZE;
	}
	let ret = base_addr + start_offset;

	let mut ret_array = unsafe { UnsafeArray::new(vec.len(), ret, core::mem::size_of::<RecvLongMsgBuffer>(), 0 as usize) };
	for i in 0..vec.len(){
		ret_array[i] = unsafe { RecvLongMsgBuffer::new_raw(vec[i].0 as *mut u8, vec[i].1) };
	}
	if enable_fault {
		let mut pages = Vec::new();
		kobj_alloc.vspace().unmap(base_addr, size, PageDeallocType::Retrieve(&mut pages), &kobj_alloc).expect("failed to unmap pages");
		INFO_VECTORS.lock().insert(base_addr, pages);
		BUFFER_TYPES.lock().insert(base_addr, BufferType::InfoVector);
	}
	ret
}

static SEND_FAULT_ENABLED: AtomicBool = AtomicBool::new(false);
static RECEIVE_FAULT_ENABLED: AtomicBool = AtomicBool::new(false);
static VECTOR_FAULT_ENABLED: AtomicBool = AtomicBool::new(true);

pub fn allocate_extended_send_buf(send_size: usize) -> usize {
	if SEND_FAULT_ENABLED.load(Ordering::Relaxed){
		allocate_extended_send_buf_fault(send_size)
	}else{
		allocate_extended_send_buf_no_fault(send_size)
	}
}

pub fn allocate_extended_receive_buf(receive_size: usize) -> usize {
	if RECEIVE_FAULT_ENABLED.load(Ordering::Relaxed){
		allocate_extended_receive_buf_fault(receive_size)
	}else{
		allocate_extended_receive_buf_no_fault(receive_size)
	}
}

pub fn allocate_extended_send_vec(vec: &[(usize, usize)], start_offset: usize) -> usize {
	allocate_extended_vec_base(vec, SEND_FAULT_ENABLED.load(Ordering::Relaxed) && VECTOR_FAULT_ENABLED.load(Ordering::Relaxed), start_offset)
}

pub fn allocate_extended_receive_vec(vec: &[(usize, usize)], start_offset: usize) -> usize {
	allocate_extended_vec_base(vec, RECEIVE_FAULT_ENABLED.load(Ordering::Relaxed) && VECTOR_FAULT_ENABLED.load(Ordering::Relaxed), start_offset)
}

pub fn allocate_extended_bufs(send_size: usize, receive_size: usize) -> (usize, usize){
	(allocate_extended_send_buf(send_size), allocate_extended_receive_buf(receive_size))
}

pub fn run_extended_test_simple(threads: &mut Vec<Arc<RwLock<SysThread>>>, send_size: usize, client_receive_size: usize, server_receive_size: usize, expected_receive_size: usize, reply_size: usize){
	run_channel_test_closure(move |channel_id: i32, message_id: i32|{
		extended_test_simple_server(channel_id, message_id, send_size, server_receive_size, expected_receive_size, reply_size);
	},
	move |channel_id: i32| {
		extended_test_simple_client(channel_id, send_size, client_receive_size, server_receive_size, reply_size);
	},
	threads, AccessMode::ReadWrite,
	ServerChannelOptions::new().get_reply_size().get_recv_total_size());
}

pub fn extended_test_simple_server(server_channel_id: i32, server_message_id: i32, send_size: usize, receive_size: usize, expected_receive_size: usize, reply_size: usize) -> Option<usize>{
	let receive_buf_size = if receive_size > send_size {
		receive_size
	}else{
		send_size
	};
	let (reply_buf_addr, recv_buf_addr) = allocate_extended_bufs(reply_size, receive_buf_size);
	let server_channel_fd = get_fd(server_channel_id);
	let server_message_fd = get_fd(server_message_id);

	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new(&mut header_buf),
		RecvLongMsgBuffer::new_raw(
		recv_buf_addr as *mut u8,
		receive_size
	)]};

	let reply_buf_info = unsafe {[SendLongMsgBuffer::new_raw(
		reply_buf_addr as *mut u8,
		reply_size
	)]};

	info!("server: waiting for message");
	let cw_size = server_channel_fd.mreadv(&mut recv_buf_info, server_message_id).expect("could not receive message from client") - HEADER_SIZE;
	let header = MsgHeader::from_long_bufs(&mut recv_buf_info).expect("could not get header from buffer");

	info!("server: receive done");

	assert!(cw_size == expected_receive_size, "server: invalid receive size {}", cw_size);
	assert!(header.client_write_size == send_size, "server: invalid total size {}", header.client_write_size);

	check_buffer(recv_buf_addr, cw_size);

	recv_buf_info[0] = unsafe {
		RecvLongMsgBuffer::new_raw(
		recv_buf_addr as *mut u8,
		receive_buf_size,
	)};

	let cw_full_size = server_message_fd.readv(&mut recv_buf_info[0..1]).expect("reading from client write buffer failed");
	assert!(cw_full_size == send_size, "invalid size {} returned from message descriptor read", cw_full_size);
	check_buffer(recv_buf_addr, cw_full_size);

	info!("server: sending reply: {}", cw_size);
	if USE_MD_ACCESSORS.load(Ordering::Relaxed){
		server_message_fd.wpwritev(&reply_buf_info, 0, OffsetType::Start).expect("cannot write reply to client");
		server_message_fd.mwpwritev(&[], 0, OffsetType::End, cw_size as ServerStatus).expect("cannot write reply to client");

	}else{
		server_message_fd.mwpwritev(&reply_buf_info, 0, OffsetType::End, cw_size as ServerStatus).expect("cannot write reply to client");

	}
	info!("server: reply done");
	None
}

pub fn extended_test_simple_client(client_id: i32, send_size: usize, receive_size: usize, expected_send_reply_size: usize, reply_size: usize) -> Option<usize>{
	let (send_buf_addr, recv_buf_addr) = allocate_extended_bufs(send_size, receive_size);

	let fd = get_fd(client_id);

	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new_raw(
		recv_buf_addr as *mut u8,
		receive_size
	)]};

	let send_buf_info = unsafe {[SendLongMsgBuffer::new_raw(
		send_buf_addr as *mut u8,
		send_size
	)]};

	info!("client: sending message: {:p}", &send_buf_info);

	let (send_reply_size, received_reply_size) = fd.writereadv(&send_buf_info, &mut recv_buf_info).expect("cannot write/read client FD");

	assert!(send_reply_size == expected_send_reply_size, "server replied with invalid size {} instead of {}", send_reply_size, send_size);
	assert!(received_reply_size == reply_size, "invalid reply size received: {} instead of {}", received_reply_size, reply_size);
	check_buffer(recv_buf_addr, receive_size);
	info!("client: done");
	None
}

//TODO: add support for LongNBSendRecv to the transport layer
/*
pub fn run_tmp_long_ipc_test_simple_replyrecv_server(threads: &mut Vec<Arc<RwLock<SysThread>>>, server_ep_cptr: seL4_CPtr, reply_cptr: seL4_CPtr, send_size: usize, receive_size: usize, expected_receive_size: usize, reply_size: usize, passive: bool){
	let mut recv_buf_addr = tmp_allocate_receive_buf(receive_size);

	let mut server = threads[0].write();
	let kobj_alloc = get_kobj_alloc();
	let notification = kobj_alloc.cspace().allocate_slot_with_object_fixed::<Notification, _>(&kobj_alloc).expect("could not allocate initialization endpoint");

	if server.get_sched_context().is_none() {
		info!("allocating scheduling context");
		server.new_sched_context(&kobj_alloc).expect("could not create scheduling context");
	}
	drop(kobj_alloc);
	server.run(move ||{
		let mut long_size = 0;
		let mut long_total_size = 0;
		let mut recv_buf_info = seL4_LongMsgBuffer {
			base: recv_buf_addr as *mut c_void,
			len: receive_size,
		};
		info!("server: starting initial receive");
		let mut info = unsafe { seL4_MessageInfo_new(0, 0, 0, 1) };
		info = unsafe { seL4_LongNBSendRecv(notification.to_cap(), info, server_ep_cptr, 0 as *mut seL4_Word, reply_cptr, &mut recv_buf_info, 1, &mut long_size, &mut long_total_size) };
		loop {
			info!("server: receive done");
			check_buffer(recv_buf_addr, expected_receive_size);
			assert!(long_size == expected_receive_size, "server: invalid receive size {}", long_size);
			assert!(long_total_size == send_size, "server: invalid total size {}", long_total_size);
			let len = unsafe { seL4_MessageInfo_get_length(info) };
			let reply_buf_addr;
			(reply_buf_addr, recv_buf_addr) = tmp_allocate_bufs(reply_size, receive_size);
			let reply_buf_info = seL4_LongMsgBuffer {
				base: reply_buf_addr as *mut c_void,
				len: reply_size,
			};

			recv_buf_info = seL4_LongMsgBuffer {
				base: recv_buf_addr as *mut c_void,
				len: receive_size,
			};

			info!("reply addr: {:x} receive addr: {:x}", reply_buf_addr, recv_buf_addr);
			info!("reply size: {} receive size: {}", reply_size, receive_size);
			info!("len: {}", len);
			if len < 120 {
				info!("server: sending final reply");
				unsafe { seL4_LongSend(reply_cptr, seL4_MessageInfo_new(0, 0, 0, 120), &reply_buf_info, 1) };
				break;
			}
			info!("server: performing ReplyRecv");
			info = unsafe { seL4_LongReplyRecv(server_ep_cptr, seL4_MessageInfo_new(0, 0, 0, 120), 0 as *mut seL4_Word, reply_cptr, &reply_buf_info, 1, &mut recv_buf_info, 1, &mut long_size, &mut long_total_size) };
		}
		info!("server: done");
		None
	}).expect("could not start server thread");

	info!("waiting for initialization");
	notification.wait();

	if passive {
		info!("unbinding scheduling context");
		let kobj_alloc = get_kobj_alloc();
		server.deallocate_sched_context(&kobj_alloc).expect("could not unbind scheduling context");
	}
	info!("server init: done");
}
*/

/*
pub fn run_tmp_long_ipc_test_simple_replyrecv_client(threads: &mut Vec<Arc<RwLock<SysThread>>>, client_ep_cptr: seL4_CPtr, send_size: usize, receive_size: usize, reply_size: usize){
	let (send_buf_addr, recv_buf_addr) = tmp_allocate_bufs(send_size, receive_size);

	let mut client = threads[1].write();
	client.run(move ||{
		let mut received_reply_size = 0;
		let send_buf_info = seL4_LongMsgBuffer {
			base: send_buf_addr as *mut c_void,
			len: send_size,
		};
		let mut recv_buf_info = seL4_LongMsgBuffer {
			base: recv_buf_addr as *mut c_void,
			len: receive_size,
		};

		let mut long_size = 0;
		let mut info = unsafe { seL4_MessageInfo_new(0, 0, 0, 120) };
		info!("client starting first call");
		unsafe { seL4_LongCall(client_ep_cptr, info, &send_buf_info, 1, &mut recv_buf_info, 1, &mut received_reply_size) };
		assert!(received_reply_size == reply_size, "invalid reply size received: {}", received_reply_size);
		info!("first call done");
		check_buffer(recv_buf_addr, receive_size);

		info = unsafe { seL4_MessageInfo_new(0, 0, 0, 119) };
		info!("client starting second call");
		unsafe { seL4_LongCall(client_ep_cptr, info, &send_buf_info, 1, &mut recv_buf_info, 1, &mut received_reply_size) };
		assert!(received_reply_size == reply_size, "invalid reply size received: {}", received_reply_size);
		info!("client done");
		check_buffer(recv_buf_addr, receive_size);

		None
	}).expect("could not start client thread");
}
*/

fn check_unrecoverable(err: IOError, expected0: sel4::ErrorDetails, expected1: sel4::ErrorDetails) -> Result<(), IOError>{
	match err {
		IOError::SyscallError(sys_err) => {
			if sys_err.details() != Some(expected0) && sys_err.details() != Some(expected1) {
				Err(err)
			}else{
				Ok(())
			}
		},
		_ => {
			info!("other unexpected error {:?} received from IPC with invalid buffer", err);
			Err(err)
		},
	}
}

fn check_unrecoverable_invalid(err: IOError) -> Result<(), IOError> {
	let expected1 = if BOTH_INVALID.load(Ordering::Relaxed){
		sel4::ErrorDetails::InvalidRemoteIPCAddress
	}else{
		sel4::ErrorDetails::InvalidLocalIPCAddress
	};
	info!("check_unrecoverable_invalid: {:?}", expected1);
	check_unrecoverable(err, sel4::ErrorDetails::InvalidLocalIPCAddress, expected1)
}
fn check_unrecoverable_valid(err: IOError) -> Result<(), IOError> {
	check_unrecoverable(err, sel4::ErrorDetails::InvalidRemoteIPCAddress, sel4::ErrorDetails::InvalidRemoteIPCAddress)
}

fn check_unrecoverable_clunk(server_channel_id: i32, res: Result<usize, IOError>) -> Result<(), IOError>{
	res?;
	let server_channel_fd = get_fd(server_channel_id);
	if let Some(mut buf) = server_channel_fd.getbuf_r(){
		let header = MsgHeader::from_buf(&mut buf);
		if header.msgtype_enum().is_none() || header.msgtype_enum().unwrap() != MsgType::Clunk {
			Err(IOError::InvalidOperation)
		}else{
			Ok(())
		}
	}else{
		Err(IOError::InvalidOperation)
	}

}

pub fn unrecoverable_valid_server(server_channel_id: i32, server_message_id: i32){
	let server_channel_fd = get_fd(server_channel_id);

	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut data_buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new(&mut header_buf),
		RecvLongMsgBuffer::new_raw(
		&mut data_buf as *mut u8,
		8
	)]};

	info!("unrecoverable_valid_server: waiting for message");
	let res = server_channel_fd.mreadv(&mut recv_buf_info, server_message_id).expect_err("invalid buffer test returned success");
	check_unrecoverable_valid(res).expect("unrecoverable_valid_server: unexpected read error type");

	info!("result of invalid buffer test: {:?}", res);
	info!("server: receive done");
	let clunk_res = server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id);
	check_unrecoverable_clunk(server_channel_id, clunk_res).expect("failed to read clunk message");
}

pub fn unrecoverable_valid_client(client_id: i32){
	let fd = get_fd(client_id);

	let buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	info!("unrecoverable_valid_client: sending message");
	let res = fd.write(&buf).expect_err("invalid buffer write succeeded");
	check_unrecoverable_valid(res).expect("unrecoverable_valid_client: unexpected write error type");
	info!("client: done: {:?}", res);
}

static USER_INVALID_ADDRESS: AtomicBool = AtomicBool::new(false);

pub fn unrecoverable_invalid_client(client_id: i32){
	let invalid_addr = if USER_INVALID_ADDRESS.load(Ordering::Relaxed) {
		0
	}else{
		!0
	};
	let fd = get_fd(client_id);
	let send_buf_info = unsafe {[SendLongMsgBuffer::new_raw(
		invalid_addr as *const u8,
		4096
	)]};
	info!("unrecoverable_invalid_client: sending message");
	let res = fd.writev(&send_buf_info).expect_err("invalid buffer write succeeded");
	check_unrecoverable_invalid(res).expect("unrecoverable_invalid_client: unexpected write error type");

	info!("client: done: {:?}", res);
}

static BOTH_INVALID: AtomicBool = AtomicBool::new(false);

pub fn unrecoverable_invalid_server(server_channel_id: i32, server_message_id: i32){
	let invalid_addr = if USER_INVALID_ADDRESS.load(Ordering::Relaxed) {
		0
	}else{
		!0
	};
	let server_channel_fd = get_fd(server_channel_id);

	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new(&mut header_buf),
		RecvLongMsgBuffer::new_raw(
		invalid_addr as *mut u8,
		4096
	)]};

	info!("unrecoverable_invalid_server: waiting for message");
	let res = server_channel_fd.mreadv(&mut recv_buf_info, server_message_id).expect_err("invalid buffer test returned success");

	check_unrecoverable_invalid(res).expect("unrecoverable_invalid_server: unexpected read error type");
	info!("server: receive done: {:?}", res);
	let clunk_res = server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id);
	check_unrecoverable_clunk(server_channel_id, clunk_res).expect("failed to read clunk message");
}

pub fn unrecoverable_reply_invalid_server(server_channel_id: i32, server_message_id: i32){
	let server_channel_fd = get_fd(server_channel_id);
	let server_message_fd = get_fd(server_message_id);
	let invalid_addr = if USER_INVALID_ADDRESS.load(Ordering::Relaxed) {
		0
	}else{
		!0
	};

	let reply_buf_info = unsafe {[SendLongMsgBuffer::new_raw(
		invalid_addr as *const u8,
		4096
	)]};

	info!("unrecoverable_reply_invalid_server: waiting for message");
	server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id).expect("initial message read for invalid buffer test failed");
	info!("server: sending reply");
	let mut read_buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	let res = server_message_fd.read(&mut read_buf).expect("reading from client write buffer failed");
	info!("server: buffer read done: {}", res);

	let mut err = server_message_fd.wpwritev(&reply_buf_info, 0, OffsetType::Start).expect_err("writing to reply buffer with invalid buffer succeeded");
	check_unrecoverable_invalid(err).expect("unrecoverable_reply_invalid_server: unexpected write error type");
	info!("unrecoverable_reply_invalid_server: buffer write done: {:?}", err);

	err = server_message_fd.writev(&reply_buf_info).expect_err("sending reply with invalid buffer succeeded");
	check_unrecoverable_invalid(err).expect("unrecoverable_reply_invalid_server: unexpected write error type");
	info!("unrecoverable_reply_invalid_server: reply done: {:?}", err);
	let clunk_res = server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id);
	check_unrecoverable_clunk(server_channel_id, clunk_res).expect("failed to read clunk message");
}

pub fn unrecoverable_reply_invalid_client(client_id: i32){
	let fd = get_fd(client_id);
	let invalid_addr = if USER_INVALID_ADDRESS.load(Ordering::Relaxed) {
		0
	}else{
		!0
	};

	let mut recv_buf_info = unsafe {[RecvLongMsgBuffer::new_raw(
		invalid_addr as *mut u8,
		4096
	)]};

	info!("unrecoverable_reply_invalid_client: sending message");
	let res = fd.readv(&mut recv_buf_info).expect_err("invalid buffer read succeeded");
	check_unrecoverable_invalid(res).expect("unrecoverable_reply_invalid_client: unexpected read error type");
	info!("client: done: {:?}", res);
}

pub fn unrecoverable_reply_valid_server(server_channel_id: i32, server_message_id: i32){
	let server_channel_fd = get_fd(server_channel_id);
	let server_message_fd = get_fd(server_message_id);

	let mut buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];

	info!("unrecoverable_reply_valid_server: waiting for message");
	server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id).expect("initial message read for invalid buffer test failed");
	let res = server_message_fd.read(&mut buf).expect("reading from client write buffer failed");
	info!("server: buffer read done: {}", res);

	let mut err = server_message_fd.wpwrite(&buf, 0, OffsetType::Start).expect_err("writing reply to invalid buffer succeeded");
	check_unrecoverable_valid(err).expect("unrecoverable_reply_valid_server: unexpected write error type");

	info!("server: buffer write done: {}", err);

	err = server_message_fd.write(&buf).expect_err("sending reply to invalid buffer succeeded");
	check_unrecoverable_valid(err).expect("unrecoverable_reply_valid_server: unexpected write error type");

	info!("unrecoverable_reply_valid_server: reply done: {:?}", err);
	let clunk_res = server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id);
	check_unrecoverable_clunk(server_channel_id, clunk_res).expect("failed to read clunk message");
}

pub fn unrecoverable_reply_valid_client(client_id: i32){
	let fd = get_fd(client_id);

	let mut buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];

	info!("unrecoverable_reply_valid_client: sending message");
	let res = fd.read(&mut buf).expect_err("valid reply received when attempting to receive from invalid buffer");
	check_unrecoverable_valid(res).expect("unrecoverable_reply_valid_client: unexpected read error type");

	info!("client: done: {:?}", res);
}

pub fn unrecoverable_partial_server(server_channel_id: i32, server_message_id: i32){
	let server_channel_fd = get_fd(server_channel_id);
	let server_message_fd = get_fd(server_message_id);
	let invalid_addr = if USER_INVALID_ADDRESS.load(Ordering::Relaxed) {
		0
	}else{
		!0
	};

	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut recv_data_buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new(&mut header_buf),
		RecvLongMsgBuffer::new(&mut recv_data_buf),
		RecvLongMsgBuffer::new_raw(invalid_addr as *mut u8, 4096),
	]};

	let send_data_buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	let send_buf_info = unsafe {[
		SendLongMsgBuffer::new(&send_data_buf),
		SendLongMsgBuffer::new_raw(
		invalid_addr as *mut u8,
		4096
	)]};

	info!("unrecoverable_partial_server: waiting for message");
	server_channel_fd.mreadv(&mut recv_buf_info[0..2], server_message_id).expect("initial message read for invalid buffer test failed");

	info!("unrecoverable_partial_server: reading beginning of client write buffer");
	let res = server_message_fd.wpreadv(&mut recv_buf_info[1..2], 0, OffsetType::Start).expect("reading from beginning of client write buffer failed");
	info!("server: buffer read done: {:?}", res);

	info!("unrecoverable_partial_server: reading invalid part of client write buffer with valid server buffer");
	let mut err = server_message_fd.wpreadv(&mut recv_buf_info[1..2], 4, OffsetType::Start).expect_err("reading from invalid part of client write buffer succeeded");
	check_unrecoverable_valid(err).expect("unexpected error received from wpreadv");

	info!("unrecoverable_partial_server: reading valid part of client write buffer with invalid server buffer");
	err = server_message_fd.wpreadv(&mut recv_buf_info[1..3], 0, OffsetType::Start).expect_err("reading from invalid part of client write buffer succeeded");
	check_unrecoverable_invalid(err).expect("unexpected error received from wpreadv");

	info!("unrecoverable_partial_server: reading invalid part of client write buffer with invalid server buffer");
	err = server_message_fd.wpreadv(&mut recv_buf_info[1..3], 4, OffsetType::Start).expect_err("reading from invalid part of client write buffer succeeded");
	check_unrecoverable_invalid(err).expect("unexpected error received from wpreadv");

	info!("unrecoverable_partial_server: writing valid part of client read buffer with valid server buffer");
	server_message_fd.wpwritev(&send_buf_info[0..1], 0, OffsetType::Start).expect("failed to write to client read buffer");

	info!("unrecoverable_partial_server: writing invalid part of client read buffer with valid server buffer");
	err = server_message_fd.wpwritev(&send_buf_info[0..1], 4, OffsetType::Start).expect_err("writing to reply buffer with invalid buffer succeeded");
	check_unrecoverable_invalid(err).expect("unexpected error received from wpwritev");

	info!("unrecoverable_partial_server: writing valid part of client read buffer with valid server buffer");
	err = server_message_fd.wpwritev(&send_buf_info, 0, OffsetType::Start).expect_err("writing to reply buffer with invalid buffer succeeded");
	check_unrecoverable_invalid(err).expect("unexpected error received from wpwritev");

	info!("unrecoverable_partial_server: sending reply");
	server_message_fd.mwpwritev(&send_buf_info[0..1], 0, OffsetType::End, send_data_buf.len() as ServerStatus).expect("sending reply with invalid buffer succeeded");
	info!("unrecoverable_partial_server: reply done");
	let clunk_res = server_channel_fd.mreadb(FIXED_BUF_MAX_SIZE, server_message_id);
	check_unrecoverable_clunk(server_channel_id, clunk_res).expect("failed to read clunk message");
}

pub fn unrecoverable_partial_client(client_id: i32){
	let fd = get_fd(client_id);
	let invalid_addr = if USER_INVALID_ADDRESS.load(Ordering::Relaxed) {
		0
	}else{
		!0
	};

	let send_buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	let send_buf_info = unsafe {[
		SendLongMsgBuffer::new(&send_buf),
		SendLongMsgBuffer::new_raw(
		invalid_addr as *mut u8,
		4096
	)]};

	let mut recv_buf: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
	let mut recv_buf_info = unsafe {[
		RecvLongMsgBuffer::new(&mut recv_buf),
		RecvLongMsgBuffer::new_raw(
		invalid_addr as *mut u8,
		4096
	)]};

	info!("unrecoverable_partial_client: sending message");
	let res = fd.writereadv(&send_buf_info, &mut recv_buf_info).expect("test writereadv failed");
	assert!(res.0 == send_buf.len(), "server returned invalid write size {}", res.0);
	assert!(res.1 == recv_buf.len(), "server returned invalid read size {}", res.1);
	info!("client: done: {:?}", res);
}

pub fn deallocate_extended_test_buffers(){
	info!("deallocating extended buffers");
	let kobj_alloc = get_kobj_alloc();
	let vspace = kobj_alloc.vspace();
	info!("getting address list");
	let mut addresses = BUFFER_ADDRESSES.lock();
	while let Some(addr) = addresses.pop_first(){
		info!("deallocate {:x}", addr);
		let res = vspace.get_reservation(addr).expect("no reservation found");
		vspace.unreserve_and_free(res.start_vaddr(), res.end_vaddr() - res.start_vaddr(), PAGE_BITS as usize, &kobj_alloc).expect("failed to free buffer");
	}
	drop(addresses);
	info!("deallocating buffer types");
	BUFFER_TYPES.lock().clear();
	info!("deallocating buffer ranges");
	BUFFER_RANGES.lock().clear();
	info!("done");
}

pub fn run_extended_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	run_extended_tests_inner(threads, false, false, false);

	run_extended_tests_inner(threads, true, false, false);
	run_extended_tests_inner(threads, false, true, false);
	run_extended_tests_inner(threads, true, true, false);

	run_extended_tests_inner(threads, true, false, true);
	run_extended_tests_inner(threads, false, true, true);
	run_extended_tests_inner(threads, true, true, true);
}

pub fn run_unrecoverable_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>, user_addr: bool){
	USER_INVALID_ADDRESS.store(user_addr, Ordering::Relaxed);
	BOTH_INVALID.store(false, Ordering::Relaxed);
	info!("running client invalid address test");
	run_channel_test(unrecoverable_valid_server, unrecoverable_invalid_client, threads, AccessMode::ReadWrite);
	info!("running server invalid address test");
	run_channel_test(unrecoverable_invalid_server, unrecoverable_valid_client, threads, AccessMode::ReadWrite);

	info!("running client invalid reply address test");
	run_channel_test(unrecoverable_reply_valid_server, unrecoverable_reply_invalid_client, threads, AccessMode::ReadWrite);
	info!("running server invalid reply address test");
	run_channel_test(unrecoverable_reply_invalid_server, unrecoverable_reply_valid_client, threads, AccessMode::ReadWrite);

	BOTH_INVALID.store(true, Ordering::Relaxed);
	info!("running client/server invalid address test");
	run_channel_test(unrecoverable_invalid_server, unrecoverable_invalid_client, threads, AccessMode::ReadWrite);
	info!("running client/server invalid reply address test");
	run_channel_test(unrecoverable_reply_invalid_server, unrecoverable_reply_invalid_client, threads, AccessMode::ReadWrite);
	info!("running client/server partial invalid address test");
	run_channel_test_send_size_only(unrecoverable_partial_server, unrecoverable_partial_client, threads, AccessMode::ReadWrite);
}


pub fn run_extended_tests_inner(threads: &mut Vec<Arc<RwLock<SysThread>>>, send_fault_enabled: bool, receive_fault_enabled: bool, vector_fault_enabled: bool){
	SEND_FAULT_ENABLED.store(send_fault_enabled, Ordering::Relaxed);
	RECEIVE_FAULT_ENABLED.store(receive_fault_enabled, Ordering::Relaxed);
	VECTOR_FAULT_ENABLED.store(vector_fault_enabled, Ordering::Relaxed);

	info!("running 10M/10M 0/0 test");
	run_extended_test_simple(threads, 10485760, 0, 10485760, 10485760, 0);
	info!("running 1M/1M 0/0 test");
	run_extended_test_simple(threads, 1048576, 0, 1048576, 1048576, 0);
	info!("running 1M/512k 0/0 test");
	run_extended_test_simple(threads, 1048576, 0, 524288, 524288, 0);
	info!("running 1M/1M 1M/1M test");
	run_extended_test_simple(threads, 1048576, 1048576, 1048576, 1048576, 1048576);
	info!("running 0/0 1M/1M test");
	run_extended_test_simple(threads, 0, 1048576, 0, 0, 1048576);

	//these are equivalent to the above, except repeated twice and using
	//ReplyRecv

	/*info!("running 1M/1M 0/0 combined reply/receive test with client before server");
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 0, 0);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 1048576, 1048576, 0, false);
	tmp_long_ipc_test_exit_wait(threads);

	info!("running 10M/10M 0/0 combined reply/receive test with server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 10485760, 10485760, 10485760, 0, false);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 10485760, 0, 0);
	tmp_long_ipc_test_exit_wait(threads);


	info!("running 1M/512K 0/0 combined reply/receive test with client before server");
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 0, 0);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 524288, 524288, 0, false);
	tmp_long_ipc_test_exit_wait(threads);

	info!("running 1M/512K 0/0 combined reply/receive test with server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 524288, 524288, 0, false);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 0, 0);
	tmp_long_ipc_test_exit_wait(threads);


	info!("running 1M/1M 1M/1M combined reply/receive test with client before server");
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 1048576, 1048576);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 1048576, 1048576, 1048576, false);
	tmp_long_ipc_test_exit_wait(threads);


	info!("running 1M/1M 1M/1M combined reply/receive test with server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 1048576, 1048576, 1048576, false);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 1048576, 1048576);
	tmp_long_ipc_test_exit_wait(threads);

	info!("running 0/0 1M/1M combined reply/receive test with client before server");
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 0, 1048576, 1048576);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 0, 1048576, 0, 1048576, false);
	tmp_long_ipc_test_exit_wait(threads);


	info!("running 0/0 1M/1M combined reply/receive test with server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 0, 1048576, 0, 1048576, false);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 0, 1048576, 1048576);
	tmp_long_ipc_test_exit_wait(threads);


	//these are equivalent to the above ReplyRecv tests, but using a
	//passive server thread

	info!("running 10M/10M 0/0 combined reply/receive test with passive server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 10485760, 10485760, 10485760, 0, true);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 10485760, 0, 0);
	tmp_long_ipc_test_exit_wait(threads);

	info!("running 1M/512K 0/0 combined reply/receive test with passive server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 524288, 524288, 0, true);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 0, 0);
	tmp_long_ipc_test_exit_wait(threads);


	info!("running 1M/1M 1M/1M combined reply/receive test with passive server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 1048576, 1048576, 1048576, 1048576, true);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 1048576, 1048576, 1048576);
	tmp_long_ipc_test_exit_wait(threads);

	info!("running 0/0 1M/1M combined reply/receive test with passive server before client");
	run_tmp_long_ipc_test_simple_replyrecv_server(threads, server_channel_id, server_message_id, 0, 1048576, 0, 1048576, true);
	tmp_long_ipc_test_spin();
	run_tmp_long_ipc_test_simple_replyrecv_client(threads, client_id, 0, 1048576, 1048576);
	tmp_long_ipc_test_exit_wait(threads);*/

	run_unrecoverable_tests(threads, false);
	run_unrecoverable_tests(threads, true);

	run_channel_suspend_test(threads, true, true);
	//run_channel_suspend_test(threads, true, false);
	//run_channel_suspend_test(threads, false, true);

	run_random_tests(threads);
}

/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
