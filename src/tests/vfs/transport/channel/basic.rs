/*
 * Copyright (c) 2022-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes core code for testing the VFS and transport layer.
 */

use alloc::sync::Arc;
use alloc::string::String;
use alloc::collections::BTreeSet;
use core::str;
use core::sync::atomic::{AtomicBool, Ordering};
use core::mem::size_of;

use crate::task::thread::SysThread;

use usync::RwLock;
use sel4_thread::{
	ThreadReturn,
	WrappedThread,
};

use crate::vfs::rpc::get_default_rpc_client;

use uxrt_procfs_client::{
	PORT_OPEN_AFTER_CREATE,
	PORT_CHANNEL_GET_RECV_TOTAL_SIZE,
	PORT_CHANNEL_GET_REPLY_SIZE,
};

use uxrt_procfs_client::port::{
	PortFSClient,
	PortType,
};

use uxrt_transport_layer::{
	AccessMode,
	BufferArray,
	FIXED_BUF_MAX_SIZE,
	FileDescriptor,
	HEADER_SIZE,
	IOError,
	MsgHeader,
	MsgType,
	Offset,
	OffsetType,
	POLLIN,
	RecvLongMsgBuffer,
	SRV_ERR_BADF,
	SRV_ERR_SRCH,
	SRV_ERR_INVAL,
	SendLongMsgBuffer,
	ServerStatus,
};

use crate::vfs::transport::{
	get_fd,
	get_root_fdspace,
};

use crate::{
	dump_heap,
	dump_utspace,
};

use super::{
	channel_test_wait,
	channel_test_wait_secondary,
	run_channel_test,
};

use super::super::SERVER_FIRST;
use super::super::super::allocate_fds;

use alloc::vec::Vec;

fn test_server_basic_read(fd: &dyn FileDescriptor, _: BufferArray, size: usize, offset: usize, whence: OffsetType) -> (usize, usize, ServerStatus) {
	info!("test server: received read of size {} offset {} whence {:?}", size, offset, whence);
	if whence as usize != OffsetType::Start as usize {
		info!("returning failure");
		return (0, 0, -SRV_ERR_INVAL);
	}
	let test_string = "hello world".as_bytes();
	let buf = fd.getbuf_w().expect("could not get buffer for server message descriptor");
	if offset < test_string.len(){
		buf.get_data()[0..test_string.len() - offset].clone_from_slice(&test_string[offset..test_string.len()]);
	}
	/*for i in 0..size {
		info!("write_buf[{}]: {:x}", i, buf.get_data()[i]);
	}*/
	(test_string.len() - offset, offset, 0)
}

fn test_server_basic_write(_: &dyn FileDescriptor, primary: BufferArray, size: usize, offset: usize, whence: OffsetType) -> (usize, usize, ServerStatus) {
	info!("test server: received write of size {} offset {} whence {:?}", size, offset, whence);

	assert!(size <= primary.len(), "test_server_basic_write: specified size {} bigger than buffer size of {}", size, primary.len());

	let primary_string = str::from_utf8(&primary[0..size]).expect("received write with invalid UTF-8 characters");
	info!("size: {}", size);
	info!("buffer contents: {}", primary_string);
	if primary_string == "foobar" {
		info!("returning failure");
		(0, 0, -SRV_ERR_BADF)
	}else{
		(0, 0, size as ServerStatus)
	}
}

fn test_server_basic_inner(fd: &dyn FileDescriptor, buf: BufferArray, cw_size: usize, cr_size: usize, msgtype: MsgType, offset: usize, whence: OffsetType) -> Option<(usize, usize, ServerStatus)> {
	match msgtype {
		MsgType::Read => {
			Some(test_server_basic_read(fd, buf, cr_size, offset, whence))
		},
		MsgType::Write | MsgType::WriteRead => {
			Some(test_server_basic_write(fd, buf, cw_size, offset, whence))
		},
		MsgType::Clunk => {
			info!("clunk received");
			None
		},
	}
}

fn test_server_fixed_common(
		channel_id: i32,
		message_id: i32,
		inner_fn: fn(&dyn FileDescriptor, BufferArray, usize, usize, MsgType, usize, OffsetType) -> Option<(usize, usize, ServerStatus)>,
		allow_read_err: bool){
	info!("test_server_fixed_common");
	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);
	loop {
		info!("test_server_fixed_common: reading message");
		let read_res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id);
		info!("test_server_fixed_common: mreadb result: {:?}", read_res);
		if read_res.is_err() && allow_read_err {
			continue;
		}
		let cw_size = read_res.expect("test server failed to get message");
		//info!("test_server_fixed_common: got write size: {}", cw_size);
		let mut buf = channel_fd.getbuf_r().expect("cannot get fixed buffer for server channel");
		//info!("test_server_fixed_common: got buffer");
		let header = MsgHeader::from_buf(&mut buf);
		//info!("test_server_fixed_common: got header: {:p}", header);
		let msgtype = header.msgtype_enum().expect("invalid message type returned in header");
		let whence = header.whence_enum().expect("invalid message type returned in header");
		//info!("test_server_fixed_common: got message: {} {:?} {} {:?}", cw_size, msgtype, header.offset, whence);
		let client_read_size = header.client_read_size;
		let offset = header.offset as usize;
		info!("test_server_fixed_common: client id: {}", header.client_id);
		if let Some((sw_size, offset, status)) = inner_fn(&message_fd, buf.get_data(), cw_size, client_read_size, msgtype, offset, whence) {
			message_fd.mwpwriteb(sw_size, offset as Offset, OffsetType::End, status).expect("failed to reply to message");
		}else{
			return;
		}
	}
}

fn test_server_basic(channel_id: i32, message_id: i32){
	test_server_fixed_common(channel_id, message_id, test_server_basic_inner, false);
}

pub fn test_server_basic_copy_read(_read_buf: &[u8], size: usize, offset: usize, whence: OffsetType) -> (Vec<u8>, usize, ServerStatus) {
	info!("test server: received read of size {} offset {} whence {:?}", size, offset, whence);
	let mut write_buf = vec![0u8; FIXED_BUF_MAX_SIZE];

	if whence as usize != OffsetType::Start as usize {
		info!("returning failure");
		write_buf.truncate(0);
		return (write_buf, 0, -SRV_ERR_INVAL);
	}
	let test_string = "hello world".as_bytes();
	let test_len = test_string.len() - offset;
	if offset < test_string.len(){
		write_buf[0..test_len].clone_from_slice(&test_string[offset..test_string.len()]);
	}
	write_buf.truncate(test_len);

	(write_buf, offset, 0)
}

pub fn test_server_basic_copy_write(read_buf: &[u8], size: usize, offset: usize, whence: OffsetType) -> (Vec<u8>, usize, ServerStatus) {
	info!("test server: received write of size {} offset {} whence {:?}", size, offset, whence);

	let dummy = vec![0u8; 0];

	let string = str::from_utf8(&read_buf[..size]).expect("received write with invalid UTF-8 characters");
	info!("buffer contents: {}", string);
	if string == "foobar" {
		info!("returning failure");
		(dummy, 0, -SRV_ERR_BADF)
	}else{
		(dummy, 0, size as ServerStatus)
	}
}

pub fn test_server_basic_copy_inner(_fd: &dyn FileDescriptor, data_buf: &[u8], cw_size: usize, cr_size: usize, msgtype: MsgType, offset: i64, whence: OffsetType) -> Option<(Vec<u8>, usize, ServerStatus)> {
	match msgtype {
		MsgType::Read => {
			Some(test_server_basic_copy_read(data_buf, cr_size, offset as usize, whence))
		},
		MsgType::Write | MsgType::WriteRead => {
			Some(test_server_basic_copy_write(data_buf, cw_size, offset as usize, whence))
		},
		MsgType::Clunk => {
			info!("clunk received");
			None
		},
	}
}

static USE_MD_ACCESSORS: AtomicBool = AtomicBool::new(false);

fn test_server_copy_common(
		channel_id: i32,
		message_id: i32,
		inner_fn: fn(&dyn FileDescriptor, &[u8], usize, usize, MsgType, i64, OffsetType) -> Option<(Vec<u8>, usize, ServerStatus)>) {
	info!("test_server_basic_copy");

	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);

	info!("test_server_copy_common got FD");
	loop {
		let use_md_accessors = USE_MD_ACCESSORS.load(Ordering::Relaxed);
		let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
		let mut data_buf: [u8; FIXED_BUF_MAX_SIZE] = [0; FIXED_BUF_MAX_SIZE];

		let mut read_bufs = [RecvLongMsgBuffer::new(&mut header_buf), RecvLongMsgBuffer::new(&mut data_buf)];

		let mut cw_size = channel_fd.mreadv(&mut read_bufs[..], message_id).expect("could not read message from server channel") - size_of::<MsgHeader>();
		let header = MsgHeader::from_long_bufs(&mut read_bufs[..]).expect("could not get message header").clone();
		let msgtype = header.msgtype_enum().expect("invalid message type returned in header");
		let whence = header.whence_enum().expect("invalid message type returned in header");
		let client_read_size = header.client_read_size;
		let offset = header.offset;
		info!("test_server_copy_common: got message: {} {} {} {:?} {} {:?}", cw_size, header.client_write_size, header.client_read_size, msgtype, header.offset, whence);

		if use_md_accessors && msgtype != MsgType::Clunk {
			data_buf.fill(0);
			cw_size = header.client_write_size;
			message_fd.readv(&mut [RecvLongMsgBuffer::new(&mut data_buf)]).expect("could not read from client send buffer");
		}

		if let Some((mut write_buf, offset, status)) = inner_fn(&message_fd, &mut data_buf, cw_size, client_read_size, msgtype, offset, whence) {
			info!("status: {} offset: {} len: {}", status, offset, write_buf.len());
			if use_md_accessors {
				message_fd.wpwrite(&write_buf[..], 0, OffsetType::Start).expect("cannot write to client reply buffer");
				write_buf = Vec::new();
			}

			message_fd.mwpwrite(&write_buf[..], offset as Offset, OffsetType::End, status).expect("failed to reply to message");
		}else{
			return;
		}
	}
}

fn test_server_basic_copy(channel_id: i32, message_id: i32) {
	test_server_copy_common(channel_id, message_id, test_server_basic_copy_inner);
}

fn test_server_fixed_mpwritereadb_common(
		channel_id: i32,
		message_id: i32,
		inner_fn: fn(&dyn FileDescriptor, BufferArray, usize, usize, MsgType, usize, OffsetType) -> Option<(usize, usize, ServerStatus)>,
		allow_badf: bool){
	let channel_fd = get_fd(channel_id);
	let mut message_fd = get_fd(message_id);

	let mut read_res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id);
	loop {
		info!("read_res: {:?}", read_res);
		if read_res.is_err() && allow_badf {
			let err = read_res.unwrap_err();
			match err {
				IOError::InvalidMessage => {},
				_ => panic!("unexpected error {:?} received when trying to read message", err),
			}
			read_res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id);
			continue;
		}
		let cw_size = read_res.expect("test server failed to get message");
		let mut buf = channel_fd.getbuf_r().expect("cannot get fixed buffer for server channel");
		let header = MsgHeader::from_buf(&mut buf);
		let msgtype = header.msgtype_enum().expect("invalid message type returned in header");
		let whence = header.whence_enum().expect("invalid message type returned in header");
		let client_read_size = header.client_read_size;
		let offset = header.offset as usize;

		info!("test_server_fixed_mpwritereadb_common: type: {:?}", header.msgtype_enum());
		if let Some((sw_size, offset, status)) = inner_fn(&mut message_fd, buf.get_data(), cw_size, client_read_size, msgtype, offset, whence) {
			info!("issuing mpwritereadv");
			read_res = channel_fd.mpwritereadb(sw_size, offset as Offset, FIXED_BUF_MAX_SIZE, status, message_id).and_then(|(_, s)| { Ok(s) });
		}else{
			return;
		}
	}
}

fn test_server_basic_mpwritereadb(channel_id: i32, message_id: i32) {
	test_server_fixed_mpwritereadb_common(channel_id, message_id, test_server_basic_inner, false);
}

fn test_server_basic_mpwritereadv(channel_id: i32, message_id: i32) {
	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);

	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut data_buf: [u8; FIXED_BUF_MAX_SIZE] = [0; FIXED_BUF_MAX_SIZE];
	let mut read_bufs = [RecvLongMsgBuffer::new(&mut header_buf), RecvLongMsgBuffer::new(&mut data_buf)];

	let mut cw_size = channel_fd.mreadv(&mut read_bufs, message_id).expect("could not read message from server channel");
	let mut header = MsgHeader::from_long_bufs(&mut read_bufs).expect("could not get message header");

	let mut msgtype = header.msgtype_enum().expect("invalid message type returned in header");
	let mut whence = header.whence_enum().expect("invalid message type returned in header");
	let cr_size = header.client_read_size;
	let mut read_offset = header.offset;

	loop {
		if let Some((write_buf, write_offset, status)) = test_server_basic_copy_inner(&message_fd, &data_buf, cw_size - size_of::<MsgHeader>(), cr_size, msgtype, read_offset, whence) {
			for i in 0..data_buf.len() {
				data_buf[i] = 'a' as u8;
			}
			read_bufs = [RecvLongMsgBuffer::new(&mut header_buf), RecvLongMsgBuffer::new(&mut data_buf)];
			let write_bufs = [SendLongMsgBuffer::new(&write_buf)];
			info!("issuing mpwritereadv");
			(_, cw_size) = channel_fd.mpwritereadv(&write_bufs, write_offset as Offset, &mut read_bufs, status, message_id).expect("test server failed to get message");
			header = MsgHeader::from_long_bufs(&mut read_bufs).expect("could not get message header");
			read_offset = header.offset;
			msgtype = header.msgtype_enum().expect("invalid message type returned in header");
			whence = header.whence_enum().expect("invalid message type returned in header");
		}else{
			return;
		}
	}
}

pub fn test_client_basic_copy(fid: i32) {
	info!("test_client_basic_copy");
	let fd = get_fd(fid);
	info!("test_client_basic_copy got FD");

	let test_string = "foo".as_bytes();
	let (mut size, mut offset) = fd.wpwrite(test_string, 0, OffsetType::Start).expect("failed to write message");
	info!("wrote {} to {}", size, offset);

	let mut buf = vec![0u8; FIXED_BUF_MAX_SIZE];

	let mut c = 'a' as u8;
	for i in 0..buf.len(){
		if c > 'z' as u8 {
			c = 'a' as u8;
		}
		buf[i] = c;
		c += 1;
	}

	(size, offset) = fd.wpwrite(&buf[..], 0, OffsetType::Start).expect("failed to write message");
	info!("wrote {} to {}", size, offset);

	const READ_SIZE: usize = 16;
	(size, offset) = fd.wpread(&mut buf[0..READ_SIZE], 0, OffsetType::Start).expect("failed to read message");
	info!("read {} from {}", size, offset);
	let string = str::from_utf8(&buf[0..size]).expect("read message with invalid UTF-8 characters");
	info!("contents: {}", string);
	assert!(string == "hello world", "invalid response {} received from read", string);
	assert!(offset == 0, "invalid offset {} received from read", offset);

	const READ_OFFSET: Offset = 5;
	(size, offset) = fd.wpread(&mut buf[0..READ_SIZE], READ_OFFSET, OffsetType::Start).expect("failed to read message");
	info!("read {} from {}", size, offset);
	let string = str::from_utf8(&buf[0..size]).expect("read message with invalid UTF-8 characters");
	info!("contents: {}", string);
	assert!(string == " world", "invalid response received from read");
	assert!(offset as Offset == READ_OFFSET, "invalid offset received from read");

	info!("sending 'foobar'; expecting failure");
	let test_string = "foobar".as_bytes();
	buf[..test_string.len()].clone_from_slice(test_string);
	let err = fd.wpwrite(&mut buf[0..test_string.len()], 0, OffsetType::Start).expect_err("server falsely returned success");
	match err {
		IOError::ServerError(SRV_ERR_BADF) => {},
		_ => panic!("unexpected error {:?} received", err),
	}


	let err = fd.wpread(&mut buf[0..test_string.len()], 5, OffsetType::End).expect_err("server falsely returned success");
	match err {
		IOError::ServerError(SRV_ERR_INVAL) => {},
		_ => panic!("unexpected error {:?} received", err),
	}
}


pub fn test_client_basic(fid: i32) {
	info!("test_client_basic");
	let fd = get_fd(fid);
	info!("test_client_basic got FD {}", fid);

	info!("test_client_basic: running test 0 (wpwriteb, short string)");
	let mut buf = fd.getbuf_w().expect("no buffers for client FD");
	let test_string = "foo".as_bytes();
	buf.get_data()[..test_string.len()].clone_from_slice(test_string);
	let (mut size, mut offset) = fd.wpwriteb(test_string.len(), 0, OffsetType::Start).expect("failed to write message");
	info!("wrote {} to {}", size, offset);
	assert!(size == test_string.len(), "invalid size received from server (should be {})", test_string.len());
	assert!(offset == 0, "non-zero offset received from server");

	info!("test_client_basic: running test 1 (wpwriteb, full short buffer length)");
	buf = fd.getbuf_w().expect("no buffers for client FD");
	let data_len = buf.get_data().len();
	for i in 0..buf.get_data().len(){
		buf.get_data()[i] = 'a' as u8;
	}
	(size, offset) = fd.wpwriteb(data_len, 0, OffsetType::Start).expect("failed to write message");
	info!("wrote {} to {}", size, offset);
	assert!(size == data_len, "invalid size {} received from server (should be {})", size, data_len);
	assert!(offset == 0, "non-zero offset received from server");

	info!("test_client_basic: running test 2 (wpreadb, at start)");
	const READ_SIZE: usize = 16;
	(size, offset) = fd.wpreadb(READ_SIZE, 0, OffsetType::Start).expect("failed to read message");
	info!("read {} from {}", size, offset);
	buf = fd.getbuf_r().expect("no buffers for client FD");
	let data = buf.get_data();
	/*for i in 0..size {
		info!("data[{}]: {:x}", i, data[i]);
	}*/
	let string = str::from_utf8(&data[0..size]).expect("read message with invalid UTF-8 characters");
	info!("contents: {}", string);
	assert!(string == "hello world", "invalid response {} received from read", string);
	assert!(offset == 0, "invalid offset received from read");

	info!("test_client_basic: running test 3 (wpreadb, past start)");
	const READ_OFFSET: Offset = 5;
	(size, offset) = fd.wpreadb(16, READ_OFFSET, OffsetType::Start).expect("failed to read message");

	buf = fd.getbuf_r().expect("no buffers for client FD");
	info!("read {} from {}", size, offset);
	let data = buf.get_data();
	let string = str::from_utf8(&data[0..size]).expect("read message with invalid UTF-8 characters");
	info!("contents: {}", string);
	assert!(string == " world", "invalid response received from read");
	assert!(offset as Offset == READ_OFFSET, "invalid offset received from read");

	info!("test_client_basic: running test 4 (wpreadb, failure expected)");
	buf = fd.getbuf_w().expect("no buffers for client FD");
	let test_string = "foobar".as_bytes();
	buf.get_data()[..test_string.len()].clone_from_slice(test_string);
	let err = fd.wpwriteb(test_string.len(), 0, OffsetType::Start).expect_err("server falsely returned success");
	match err {
		IOError::ServerError(SRV_ERR_BADF) => {},
		_ => panic!("unexpected error {:?} received", err),
	}

	info!("test_client_basic: running test 5 (wpreadb, past end, failure expected)");
	let err = fd.wpreadb(test_string.len(), 5, OffsetType::End).expect_err("server falsely returned success");
	match err {
		IOError::ServerError(SRV_ERR_INVAL) => {},
		_ => panic!("unexpected error received"),
	}
	info!("test_client_basic: done");
}


fn test_client_basic_control(fid: i32) {
	info!("test_client_basic_control");
	let fdspace = get_root_fdspace();
	let guard = fdspace.read();

	let orig_fd = guard.get_control_fd(fid).expect("no control FD for channel");
	let fd = orig_fd.get_base_fd();
	info!("test_client_basic_control got FD");

	info!("test_client_basic_control: running test 0 (wpwriteb, short string)");
	let buf = fd.getbuf_w().expect("no buffers for client FD");
	let test_string = "foo".as_bytes();
	buf.get_data()[..test_string.len()].clone_from_slice(test_string);
	let (size, offset) = fd.wpwriteb(test_string.len(), 0, OffsetType::Start).expect("failed to write message");
	info!("wrote {} to {}", size, offset);
	assert!(size == test_string.len(), "invalid size received from server (should be {})", test_string.len());
	assert!(offset == 0, "non-zero offset received from server");
}

fn test_server_combined_inner(fd: &dyn FileDescriptor, mut buf: BufferArray, cw_size: usize, _cr_size: usize, msgtype: MsgType, _offset: usize, _whence: OffsetType) -> Option<(usize, usize, ServerStatus)> {
	match msgtype {
		MsgType::Read => {
			Some((0, 0, -SRV_ERR_BADF))
		},
		MsgType::Write | MsgType::WriteRead => {
			info!("test server: received write of {}", cw_size);
			let string = str::from_utf8_mut(&mut buf[0..cw_size]).expect("received write with invalid UTF-8 characters");
			string.make_ascii_uppercase();
			info!("contents: {}", string);
			let bytes = string.as_bytes();
			let buf = fd.getbuf_w().expect("could not get buffer for server message descriptor");
			buf.get_data()[0..bytes.len()].clone_from_slice(&bytes[0..bytes.len()]);
			for i in 0..bytes.len(){
				info!("bytes[{}]: {:x}", i, bytes[i]);
			}
			Some((bytes.len(), 0, cw_size as ServerStatus))
		},
		MsgType::Clunk => {
			None
		},
	}
}

fn test_server_combined(channel_id: i32, message_id: i32){
	test_server_fixed_common(channel_id, message_id, test_server_combined_inner, true);
}

fn test_server_combined_mpwritereadb(channel_id: i32, message_id: i32) {
	test_server_fixed_mpwritereadb_common(channel_id, message_id, test_server_combined_inner, true);
}

fn test_server_combined_copy_inner(_fd: &dyn FileDescriptor, cw_buf: &[u8], cw_size: usize, _cr_size: usize, msgtype: MsgType, _offset: i64, _whence: OffsetType) -> Option<(Vec<u8>, usize, ServerStatus)> {

	match msgtype {
		MsgType::Read => {
			let mut sw_buf = Vec::new();
			sw_buf.truncate(0);
			Some((sw_buf, 0, -SRV_ERR_BADF))
		},
		MsgType::Write | MsgType::WriteRead => {
			info!("test server: received write of {}", cw_size);
			let mut string = String::from_utf8(cw_buf[..cw_size].to_vec()).expect("received write with invalid UTF-8 characters").clone();
			string.make_ascii_uppercase();
			info!("contents: {}", string);
			let sw_buf = string.into_bytes();
			Some((sw_buf, 0, cw_size as ServerStatus))
		},
		MsgType::Clunk => {
			None
		},
	}
}

fn test_server_combined_copy(channel_id: i32, message_id: i32) {
	test_server_copy_common(channel_id, message_id, test_server_combined_copy_inner);
}

fn test_client_combined(fid: i32) {
	let fd = get_fd(fid);
	info!("attempting to read");
	let err = fd.readb(16).expect_err("server falsely returned success");
	match err {
		IOError::ServerError(SRV_ERR_BADF) => {},
		_ => panic!("unexpected error received"),
	}

	info!("writing test message");
	let mut buf = fd.getbuf_w().expect("failed to get IPC buffers for client");

	let test_string = "foo".as_bytes();
	buf.get_data()[..test_string.len()].clone_from_slice(test_string);
	let (w_size, r_size) = fd.writereadb(test_string.len(), test_string.len()).expect("failed to write message");
	info!("wrote {}, read {}", w_size, r_size);
	buf = fd.getbuf_r().expect("could not get fixed buffers for client");
	let primary = buf.get_data();
	let uc_string = str::from_utf8(&primary[0..test_string.len()]).expect("read message with invalid UTF-8 characters");
	assert!(uc_string == "FOO", "server returned invalid result {}", uc_string);
}

fn test_client_combined_copy(fid: i32) {
	const BUF_SIZE: usize = 16;
	let fd = get_fd(fid);
	info!("attempting plain read");
	let mut buf = vec![0u8; BUF_SIZE];
	let err = fd.read(&mut buf[..]).expect_err("server falsely returned success");
	match err {
		IOError::ServerError(SRV_ERR_BADF) => {},
		_ => panic!("unexpected error received"),
	}

	info!("performing writeread");
	let test_string = "foo".as_bytes();
	let (w_size, r_size) = fd.writeread(&test_string, &mut buf).expect("failed to write message");
	info!("wrote {}, read {}", w_size, r_size);

	let uc_string = str::from_utf8(&buf[..r_size]).expect("read message with invalid UTF-8 characters");
	assert!(uc_string == "FOO", "server returned invalid result {}", uc_string);
}

fn test_client_readonly(fid: i32) {
	let fd = get_fd(fid);
	let err = fd.writeb(16).expect_err("server accepted a write on a read-only FD");
	match err {
		IOError::InvalidOperation => {},
		_ => { panic!("unexpected error {:?} received from invalid write", err); },
	}

	let (size, _) = fd.wpreadb(16, 0, OffsetType::Start).expect("failed to read message");
	info!("read {}", size);
	let buf = fd.getbuf_r().expect("cannot get buffer for client FD");
	let primary = buf.get_data();
	let string = str::from_utf8(&primary[0..size]).expect("read message with invalid UTF-8 characters");
	info!("contents: {}", string);
}

fn test_server_readonly_inner(fd: &dyn FileDescriptor, buf: BufferArray, _cw_size: usize, cr_size: usize, msgtype: MsgType, offset: usize, whence: OffsetType) -> Option<(usize, usize, ServerStatus)> {
	info!("test_server_readonly_inner");
	match msgtype {
		MsgType::Read => {
			info!("got read");
			Some(test_server_basic_read(fd, buf, cr_size, offset, whence))
		},
		MsgType::Write | MsgType::WriteRead => {
			panic!("write received on read-only FD");
		},
		MsgType::Clunk => {
			None
		},
	}
}

fn test_server_readonly(channel_id: i32, message_id: i32) {
	test_server_fixed_common(channel_id, message_id, test_server_readonly_inner, true);
}

fn test_server_readonly_mpwritereadb(channel_id: i32, message_id: i32) {
	test_server_fixed_mpwritereadb_common(channel_id, message_id, test_server_readonly_inner, true);
}

fn test_server_writeonly_inner(fd: &dyn FileDescriptor, buf: BufferArray, cw_size: usize, _cr_size: usize, msgtype: MsgType, offset: usize, whence: OffsetType) -> Option<(usize, usize, ServerStatus)> {
	match msgtype {
		MsgType::Read => {
			panic!("read received on write-only FD");
		},
		MsgType::Write | MsgType::WriteRead => {
			Some(test_server_basic_write(fd, buf, cw_size, offset, whence))
		},
		MsgType::Clunk => {
			None
		},
	}
}

fn test_client_writeonly(fid: i32) {
	let fd = get_fd(fid);
	let err = fd.readb(16).expect_err("server accepted a read on a write-only FD");
	match err {
		IOError::InvalidOperation => {},
		_ => { panic!("unexpected error {:?} received from invalid read", err); },
	}

	let buf = fd.getbuf_w().expect("failed to get IPC buffers for client");
	let test_string = "foo".as_bytes();
	buf.get_data()[..test_string.len()].clone_from_slice(test_string);
	let size = fd.writeb(test_string.len()).expect("failed to write message");
	info!("wrote {}", size);
	assert!(size == test_string.len(), "server returned invalid write size {}", size);
}

fn test_server_writeonly_mpwritereadb(channel_id: i32, message_id: i32) {
	test_server_fixed_mpwritereadb_common(channel_id, message_id, test_server_writeonly_inner, true);
}

fn test_server_writeonly(channel_id: i32, message_id: i32) {
	test_server_fixed_common(channel_id, message_id, test_server_writeonly_inner, true);
}

fn test_server_error_sequence(channel_id: i32, message_id: i32) {
	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);
	info!("attempting write without pending reply");
	let mut err = message_fd.mwpwriteb(FIXED_BUF_MAX_SIZE, 0, OffsetType::End, 0).expect_err("write succeeded without pending reply");
	match err {
		IOError::SyscallError(sys_err) => {
			if sys_err.details() != Some(sel4::ErrorDetails::ReplySequenceError) {
				panic!("unexpected error {:?} received from invalid write", err);
			}
		},
		_ => { panic!("unexpected error {:?} received from invalid write", err); },
	}

	info!("attempting combined write/read without pending reply");
	err = channel_fd.mwritereadb(FIXED_BUF_MAX_SIZE, FIXED_BUF_MAX_SIZE, FIXED_BUF_MAX_SIZE as ServerStatus, message_id).expect_err("write succeeded without pending reply");
	match err {
		IOError::SyscallError(sys_err) => {
			if sys_err.details() != Some(sel4::ErrorDetails::ReplySequenceError) {
				panic!("unexpected error {:?} received from invalid combined write/read",err);
			}
		},
		_ => { panic!("unexpected error {:?} received from invalid combined write/read", err); },
	}

	channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id).expect("failed to read message");
	err = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id).expect_err("reading message into message descriptor with pending message succeeded");
	match err {
		IOError::SyscallError(sys_err) => {
			if sys_err.details() != Some(sel4::ErrorDetails::ReplySequenceError) {
				panic!("unexpected error {:?} received from invalid combined write/read",err);
			}
		},
		_ => { panic!("unexpected error {:?} received from invalid combined write/read", err); },
	}
	message_fd.writeb(FIXED_BUF_MAX_SIZE).expect("failed to write reply");

	info!("done");
}

fn test_server_error_oversized(channel_id: i32, message_id: i32) {
	let channel_fd = get_fd(channel_id);

	info!("attempting to read oversized message");
	let res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE * 2, message_id).expect_err("reading oversized message succeeded");
	match res {
		IOError::InvalidArgument => {},
		_ => { panic!("unexpected error {:?} received from oversize read", res); },
	}
}

fn test_client_error_sequence(fid: i32) {
	let fd = get_fd(fid);
	fd.writeb(FIXED_BUF_MAX_SIZE).expect("writing message failed");
}

fn test_client_error_oversized(fid: i32) {
	let fd = get_fd(fid);
	let size = FIXED_BUF_MAX_SIZE * 2;
	info!("sending oversized message (size: {}, max: {})", size, FIXED_BUF_MAX_SIZE);
	let err = fd.writeb(size).expect_err("server accepted oversized short message");
	match err {
		IOError::InvalidArgument => {},
		_ => { panic!("unexpected error {:?} received from oversized message", err); },
	}
	info!("wrote {}", size);
}

fn test_client_poll(channel_id: i32){
	info!("test_client_poll");
	let fd = get_fd(channel_id);
	let test_string = "foo".as_bytes();
	let (size, offset) = fd.wpwrite(test_string, 0, OffsetType::Start).expect("failed to write message");
	info!("wrote {} to {}", size, offset);
	assert!(size == test_string.len(), "invalid accepted size {}", size);
	assert!(offset == 0, "invalid offset {}", offset);
}

fn test_server_poll(channel_id: i32){
	info!("test_server_poll");
	let channel_fd = get_fd(channel_id);
	channel_fd.wpreadb(0, POLLIN, OffsetType::Poll).expect("could not poll file descriptor");
	info!("test_server_poll done");
}

fn test_server_poll_recv(channel_id: i32, message_id: i32){
	info!("test_server_poll_recv");
	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);
	let mut header_buf: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
	let mut data_buf: [u8; FIXED_BUF_MAX_SIZE] = [0; FIXED_BUF_MAX_SIZE];

	let mut read_bufs = [RecvLongMsgBuffer::new(&mut header_buf), RecvLongMsgBuffer::new(&mut data_buf)];
	let cw_size = channel_fd.mreadv(&mut read_bufs[..], message_id).expect("could not read message from server channel") - size_of::<MsgHeader>();
	let string = str::from_utf8(&data_buf[..cw_size]).expect("received write with invalid UTF-8 characters");
	assert!(string == "foo", "invalid message {} received from client", string);
	message_fd.mwpwriteb(0, 0, OffsetType::End, cw_size as ServerStatus).expect("could not send reply to client");
	info!("test_server_poll_recv done");
}

pub fn run_channel_poll_test_servers(server_channel_id: i32, server_message_id: i32, client: &SysThread, poll_server: &mut SysThread, recv_server: &mut SysThread, server_first: bool, poll_first: bool) {
	let server_wrapper = move || -> Option<ThreadReturn>{
		test_server_poll(server_channel_id);
		None
	};

	let server_recv_wrapper = move || -> Option<ThreadReturn>{
		test_server_poll_recv(server_channel_id, server_message_id);
		None
	};
	if poll_first{
		info!("starting poll server");
		poll_server.run(server_wrapper).expect("could not start server thread");
		channel_test_wait(client, poll_server);
		info!("starting receive server");
		recv_server.run(server_recv_wrapper).expect("could not start server thread");
		if server_first {
			channel_test_wait_secondary(client, poll_server, Some(recv_server));
		}
	}else{
		info!("starting poll server");
		poll_server.run(server_wrapper).expect("could not start server thread");
		channel_test_wait(client, poll_server);
		info!("starting receive server");
		recv_server.run(server_recv_wrapper).expect("could not start server thread");
		if server_first {
			channel_test_wait_secondary(client, poll_server, Some(recv_server));
		}
	}
}

pub fn run_channel_poll_test(threads: &mut Vec<Arc<RwLock<SysThread>>>, poll_first: bool) {
	let (channel_port_id, server_channel_id, client_id, server_message_id, message_port_id) = allocate_fds(PortType::DirectChannel, (PORT_CHANNEL_GET_REPLY_SIZE | PORT_CHANNEL_GET_RECV_TOTAL_SIZE) as u64, AccessMode::ReadWrite);

	info!("getting threads");
	let mut poll_server = threads[0].write();
	let mut client = threads[1].write();
	let mut recv_server = threads[2].write();

	let client_wrapper = move || -> Option<ThreadReturn>{
		test_client_poll(client_id);
		None
	};

	if SERVER_FIRST.load(Ordering::Relaxed) {
		run_channel_poll_test_servers(server_channel_id, server_message_id, &client, &mut poll_server, &mut recv_server, true, poll_first);
		info!("starting client");
		client.run(client_wrapper).expect("could not start client thread");
	}else{
		info!("starting client");
		client.run(client_wrapper).expect("could not start client thread");
		channel_test_wait_secondary(&client, &poll_server, Some(&recv_server));
		run_channel_poll_test_servers(server_channel_id, server_message_id, &client, &mut poll_server, &mut recv_server, true, poll_first);
	}

	client.get_exit_endpoint().unwrap().recv_refuse_reply();
	client.suspend().expect("failed to suspend client");
	info!("client done");

	let rpc_client = get_default_rpc_client();
	let portfs_client = PortFSClient::new(None, true);

	info!("deallocating client FD");
	portfs_client.unlink(channel_port_id).expect("cannot unlink channel port");
	rpc_client.close(client_id).expect("cannot close client FD");

	poll_server.get_exit_endpoint().unwrap().recv_refuse_reply();
	info!("suspending poll server thread");
	poll_server.suspend().expect("cannot suspend poll server thread");

	recv_server.get_exit_endpoint().unwrap().recv_refuse_reply();
	info!("suspending receive server thread");
	recv_server.suspend().expect("cannot suspend receive server thread");

	rpc_client.close(server_channel_id).expect("cannot close server channel FD");
	portfs_client.unlink(message_port_id).expect("cannot unlink message port");
	rpc_client.close(server_message_id).expect("cannot close server message FD");

	info!("Heap status after FD deallocation:");
	dump_heap();
	info!("UTSpace status after FD deallocation:");
	dump_utspace();
}

const EXPECTED_CLIENTS: usize = 2;
fn test_server_shared_double(
		channel_id: i32,
		message_id: i32){
	info!("test_server_shared_double");
	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);
	let mut all_clients = BTreeSet::new();
	let mut active_clients = BTreeSet::new();

	loop {
		info!("test_server_shared_double: reading message");
		let read_res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id);
		info!("test_server_shared_double: mreadb result: {:?}", read_res);
		read_res.expect("cannot read from server channel");
		//info!("test_server_fixed_common: got write size: {}", cw_size);
		let mut buf = channel_fd.getbuf_r().expect("cannot get fixed buffer for server channel");
		//info!("test_server_fixed_common: got buffer");
		let header = MsgHeader::from_buf(&mut buf);
		//info!("test_server_fixed_common: got header: {:p}", header);
		let msgtype = header.msgtype_enum().expect("invalid message type returned in header");
		let client_id = header.client_id;
		//info!("test_server_fixed_common: got message: {} {:?} {} {:?}", cw_size, msgtype, header.offset, whence);
		if msgtype == MsgType::Clunk {
			active_clients.remove(&client_id);
			if active_clients.len() == 0 {
				break;
			}
		}else{
			all_clients.insert(client_id);
			active_clients.insert(client_id);
			message_fd.mwpwriteb(0, 0, OffsetType::End, 1).expect("failed to reply to message");
		}
	}
	assert!(all_clients.len() == EXPECTED_CLIENTS, "{} clients connected instead of the expected {}", all_clients.len(), EXPECTED_CLIENTS);
}

/*
fn test_client_shared_double(fid: i32){
	let fd = get_fd(fid);
	fd.writeb(1).expect("failed to write test message");
}

pub fn run_channel_shared_double_test(threads: &mut Vec<Arc<RwLock<SysThread>>>) {
	let (server_channel_id, client1_id, server_message_id, client2_id) = allocate_fds_base(FileDescriptionType::IPCChannel(ServerChannelOptions::new().get_reply_size().get_recv_total_size()), AccessMode::ReadWrite, true, true);

	info!("getting threads");
	let mut server = threads[0].write();
	let mut client1 = threads[1].write();
	let mut client2 = threads[2].write();


	if SERVER_FIRST.load(Ordering::Relaxed) {
		info!("starting server");
		server.run(move || -> Option<ThreadReturn>{
			test_server_shared_double(server_channel_id, server_message_id);
			None
		}).expect("could not start server thread");
		info!("starting client 1");
		client1.run(move || -> Option<ThreadReturn>{
			test_client_shared_double(client1_id);
			None
		}).expect("could not start client thread");
		client2.run(move || -> Option<ThreadReturn>{
			test_client_shared_double(client2_id);
			None
		}).expect("could not start client thread");
	}else{
		info!("starting client 1");
		client1.run(move || -> Option<ThreadReturn>{
			test_client_shared_double(client1_id);
			None
		}).expect("could not start client thread");
		client2.run(move || -> Option<ThreadReturn>{
			test_client_shared_double(client2_id);
			None
		}).expect("could not start client thread");

		info!("starting server");
		server.run(move || -> Option<ThreadReturn>{
			test_server_shared_double(server_channel_id, server_message_id);
			None
		}).expect("could not start server thread");
	}

	client1.get_exit_endpoint().unwrap().recv_refuse_reply();
	client1.suspend().expect("failed to suspend client 1");
	info!("client 1 done");

	client2.get_exit_endpoint().unwrap().recv_refuse_reply();
	client2.suspend().expect("failed to suspend client 2");
	info!("client 2 done");


	let tmp = get_root_fdspace();
	let mut fdspace = tmp.write();

	info!("deallocating client 1 FD");
	fdspace.remove(client1_id).expect("could not deallocate client 1 FD");
	info!("deallocating client 2 FD");
	fdspace.remove(client2_id).expect("could not deallocate client 2 FD");

	server.get_exit_endpoint().unwrap().recv_refuse_reply();
	info!("suspending server thread");
	server.suspend().expect("cannot suspend server thread");

	info!("deallocating server channel FD");
	fdspace.remove(server_channel_id).expect("could not deallocate server channel FD");
	info!("deallocating server message FD");
	fdspace.remove(server_message_id).expect("could not deallocate server message FD");

	info!("Heap status after FD deallocation:");
	dump_heap();
	info!("UTSpace status after FD deallocation:");
	dump_utspace();
}
*/
fn test_server_clunk(
		channel_id: i32,
		message_id_0: i32,
		message_id_1: i32){
	info!("test_server_clunk");
	let channel_fd = get_fd(channel_id);
	let message_fd_1 = get_fd(message_id_1);


	info!("test_server_clunk: reading message");
	let mut res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id_0).expect("failed to read client message");
	info!("server: result: {:?}", res);

	info!("test_server_clunk: reading message");
	res = channel_fd.mreadb(FIXED_BUF_MAX_SIZE, message_id_1).expect("failed to read clunk message");
	info!("server: result: {:?}", res);
}

fn test_client_clunk(fid: i32){
	info!("test_client_clunk");
	let fd = get_fd(fid);
	let res = fd.writeb(1).expect_err("read falsely suceeded when server clunk was expected");
	if let IOError::ServerError(err) = res {
		assert!(err == SRV_ERR_SRCH, "invalid server error {} received", err);
	}else{
		panic!("invalid error {:?} received instead of server clunk", res);
	}
	info!("client done");
}

pub fn run_channel_server_clunk_test(threads: &mut Vec<Arc<RwLock<SysThread>>>) {
	let (channel_port_id, server_channel_id, client_id, server_message_id_0, message_port_id_0) = allocate_fds(PortType::DirectChannel, (PORT_CHANNEL_GET_REPLY_SIZE | PORT_CHANNEL_GET_RECV_TOTAL_SIZE) as u64, AccessMode::ReadWrite);

	let portfs_client = PortFSClient::new(None, true);
	let (message_port_id_1, server_message_id_1) = portfs_client.create(PortType::MessageDescriptor, PORT_OPEN_AFTER_CREATE as u64).expect("cannot create extra server message descriptor");

	info!("getting threads");
	let mut server = threads[0].write();
	let mut client = threads[1].write();

	if SERVER_FIRST.load(Ordering::Relaxed) {
		info!("starting server");
		server.run(move || -> Option<ThreadReturn>{
			test_server_clunk(server_channel_id, server_message_id_0, server_message_id_1);
			None
		}).expect("could not start server thread");
		info!("starting client");
		client.run(move || -> Option<ThreadReturn>{
			test_client_clunk(client_id);
			None
		}).expect("could not start client thread");
	}else{
		info!("starting client");
		client.run(move || -> Option<ThreadReturn>{
			test_client_clunk(client_id);
			None
		}).expect("could not start client thread");

		info!("starting server");
		server.run(move || -> Option<ThreadReturn>{
			test_server_clunk(server_channel_id, server_message_id_0, server_message_id_1);
			None
		}).expect("could not start server thread");
	}

	info!("pausing");
	for _ in 0..1000 {
		sel4::yield_now();
	}

	let rpc_client = get_default_rpc_client();
	info!("removing server channel FD");
	portfs_client.unlink(channel_port_id).expect("cannot unlink channel port");
	rpc_client.close(server_channel_id).expect("cannot close server FD");


	info!("removing server message FD 0");
	portfs_client.unlink(message_port_id_0).expect("cannot unlink message port");
	rpc_client.close(server_message_id_0).expect("cannot close server message FD");

	info!("removing server message FD 1");
	portfs_client.unlink(message_port_id_1).expect("cannot unlink message port");
	rpc_client.close(server_message_id_1).expect("cannot close server message FD");

	info!("waiting for client to exit");
	client.get_exit_endpoint().unwrap().recv_refuse_reply();
	info!("suspending client thread");
	client.suspend().expect("failed to suspend client");
	info!("client done");

	info!("waiting for server to exit");
	server.get_exit_endpoint().unwrap().recv_refuse_reply();
	info!("suspending server thread");
	server.suspend().expect("cannot suspend server thread");

	info!("deallocating client FD");
	rpc_client.close(client_id).expect("could not deallocate client FD");

	info!("Heap status after FD deallocation:");
	dump_heap();
	info!("UTSpace status after FD deallocation:");
	dump_utspace();
}

pub fn run_basic_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("running basic test");
	run_channel_test(test_server_basic, test_client_basic, threads, AccessMode::ReadWrite);
	info!("running basic test with control FD");
	run_channel_test(test_server_basic, test_client_basic_control, threads, AccessMode::ReadWrite);
	info!("running basic test with copy on both sides");
	run_channel_test(test_server_basic_copy, test_client_basic_copy, threads, AccessMode::ReadWrite);
	info!("running basic test with copy on server only");
	run_channel_test(test_server_basic_copy, test_client_basic, threads, AccessMode::ReadWrite);
	info!("running basic test with copy on client only");
	run_channel_test(test_server_basic, test_client_basic_copy, threads, AccessMode::ReadWrite);
	info!("running basic test with combined write/read");
	run_channel_test(test_server_basic_mpwritereadb, test_client_basic, threads, AccessMode::ReadWrite);
	info!("running basic test with combined write/read and copy on both sides");
	run_channel_test(test_server_basic_mpwritereadv, test_client_basic_copy, threads, AccessMode::ReadWrite);
	info!("running basic test with combined write/read and copy on server only");
	run_channel_test(test_server_basic_mpwritereadv, test_client_basic, threads, AccessMode::ReadWrite);
	info!("running basic test with combined write/read and copy on client only");
	run_channel_test(test_server_basic_mpwritereadb, test_client_basic_copy, threads, AccessMode::ReadWrite);

	info!("running combined reply test");
	run_channel_test(test_server_combined, test_client_combined, threads, AccessMode::ReadWrite);
	info!("running combined reply test with combined write/read");
	run_channel_test(test_server_combined_mpwritereadb, test_client_combined, threads, AccessMode::ReadWrite);
	info!("running combined reply test with copy on both sides");
	run_channel_test(test_server_combined_copy, test_client_combined_copy, threads, AccessMode::ReadWrite);
	info!("running combined reply test with copy on server only");
	run_channel_test(test_server_combined_copy, test_client_combined, threads, AccessMode::ReadWrite);
	info!("running combined reply test with copy on client only");
	run_channel_test(test_server_combined, test_client_combined_copy, threads, AccessMode::ReadWrite);
	info!("running read-only FD test");
	run_channel_test(test_server_readonly, test_client_readonly, threads, AccessMode::ReadOnly);
	info!("running read-only FD test with combined write/read");
	run_channel_test(test_server_readonly_mpwritereadb, test_client_readonly, threads, AccessMode::ReadOnly);
	info!("running write-only FD test");
	run_channel_test(test_server_writeonly, test_client_writeonly, threads, AccessMode::WriteOnly);
	info!("running write-only FD test with combined write/read");
	run_channel_test(test_server_writeonly_mpwritereadb, test_client_writeonly, threads, AccessMode::WriteOnly);

	info!("running protocol sequence error FD test");
	run_channel_test(test_server_error_sequence, test_client_error_sequence, threads, AccessMode::ReadWrite);

	info!("running oversized message FD test");
	run_channel_test(test_server_error_oversized, test_client_error_oversized, threads, AccessMode::ReadWrite);

	info!("running FD poll test with poll first");
	run_channel_poll_test(threads, true);

	info!("running FD poll test with receive first");
	run_channel_poll_test(threads, false);

	/*info!("running shared channel FD test with two clients");
	run_channel_shared_double_test(threads);*/

	info!("running server-initiated clunk test");
	run_channel_server_clunk_test(threads);
}
/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
