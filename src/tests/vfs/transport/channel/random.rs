/*
 * Copyright (c) 2022-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes core code for testing the VFS and transport layer.
 */

use alloc::sync::Arc;
use core::mem::size_of;
use core::sync::atomic::Ordering;
use core::slice;

use rand::{Rng, SeedableRng};
use rand_pcg::Pcg64Mcg;

use sel4::{
	PAGE_SIZE,
	page_align,
};

use usync::RwLock;

use zerocopy::AsBytes;

use uxrt_transport_layer::{
	AccessMode,
	BufferArray,
	FileDescriptor,
	HEADER_SIZE,
	MsgHeader,
	Offset,
	OffsetType,
	RecvLongMsgBuffer,
	SendLongMsgBuffer,
	ServerChannelOptions,
	ServerStatus,
};
use crate::task::thread::SysThread;

use super::extended::{
	add_fault_range,
	allocate_extended_receive_buf,
	allocate_extended_send_buf,
	allocate_extended_receive_vec,
	allocate_extended_send_vec,
};
use crate::vfs::transport::get_fd;

use alloc::vec::Vec;

use super::super::USE_MD_ACCESSORS;
use super::run_channel_test_closure;

const MAX_BUFFERS_PER_ITERATION: usize = 20;
const MAX_BUFFER_SIZE: usize = 10485760;

#[derive(Copy, Clone)]
struct SendVector {
	addr: usize,
	count: usize,
	size: usize,
}
impl SendVector {
	fn new(addr: usize, count: usize, size: usize) -> Self {
		Self {
			addr,
			count,
			size,
		}
	}
	fn as_slice(&self) -> &[SendLongMsgBuffer<'_>] {
		unsafe { slice::from_raw_parts(self.addr as *mut SendLongMsgBuffer, self.count) }
	}
	fn bytes(&self) -> usize {
		self.size
	}
	fn as_raw(&self) -> Vec<(usize, usize)> {
		let mut res = Vec::new();
		for buf in self.as_slice() {
			res.push((buf.base as usize, buf.len))
		}
		res
	}
}

#[derive(Copy, Clone)]
struct RecvVector {
	addr: usize,
	count: usize,
	size: usize,
}
impl RecvVector {
	fn new(addr: usize, count: usize, size: usize) -> Self {
		Self {
			addr,
			count,
			size,
		}
	}
	fn as_slice(&self) -> &mut [RecvLongMsgBuffer<'_>] {
		unsafe { slice::from_raw_parts_mut(self.addr as *mut RecvLongMsgBuffer, self.count) }
	}
	fn bytes(&self) -> usize {
		self.size
	}
	fn as_raw(&self) -> Vec<(usize, usize)> {
		let mut res = Vec::new();
		for buf in self.as_slice() {
			res.push((buf.base as usize, buf.len))
		}
		res
	}
}

fn check_random_bufs(bufs0: &[(usize, usize)], bufs1: &[(usize, usize)]) -> Result<(), ()> {
	info!("check_random_bufs: {} {}", bufs0.len(), bufs1.len());
	let mut index0 = 0;
	let mut index1 = 0;
	let mut offset0 = 0;
	let mut offset1 = 0;
	while index0 < bufs0.len() && index1 < bufs1.len(){
		let array0 = unsafe { BufferArray::new(bufs0[index0].0 as *mut u8, bufs0[index0].1) };
		let array1 = unsafe { BufferArray::new(bufs1[index1].0 as *mut u8, bufs1[index1].1) };
		info!("len0: {} len1: {}", array0.len(), array1.len());
		while offset0 < array0.len() && offset1 < array1.len() {
			let value0 = array0[offset0];
			let value1 = array1[offset1];
			if value0 != value1 {
				error!("mismatch between bufs0[{}][{}] and bufs1[{}][{}]: {} {}", index0, offset0, index1, offset1, value0, value1);
				return Err(());
			}
			offset0 += 1;
			offset1 += 1;
		}
		if offset0 >= array0.len(){
			index0 += 1;
			offset0 = 0;
		}
		if offset1 >= array1.len(){
			index1 += 1;
			offset1 = 0;
		}
	}
	Ok(())
}

fn allocate_random_buf_ranges(rng: &mut Pcg64Mcg, addr: usize, size: usize){
	let res_end = page_align(addr + size);
	let mut alloc_start = addr;
	while alloc_start < res_end {
		let alloc_end = page_align(rng.gen_range(alloc_start..res_end));
		add_fault_range(alloc_start, alloc_end);
		alloc_start += alloc_end - alloc_start;
	}
}

static mut HEADER: MsgHeader = MsgHeader::new();

fn allocate_random_buf(rng: &mut Pcg64Mcg, send: bool) -> (usize, usize) {
	let buf_size = rng.gen_range(1..MAX_BUFFER_SIZE);
	let offset = if rng.gen_range(0..2) > 0 {
		rng.gen_range(0..PAGE_SIZE)
	}else{
		0
	};
	let buf_addr = if send {
		allocate_extended_send_buf(buf_size + offset)
	}else{
		allocate_extended_receive_buf(buf_size + offset)
	};
	allocate_random_buf_ranges(rng, buf_addr, buf_size + offset);
	info!("buffer: {:x} {}", buf_addr + offset, buf_size);

	(buf_addr + offset, buf_size)
}

fn allocate_random_send_buf(rng: &mut Pcg64Mcg) -> (usize, usize) {
	allocate_random_buf(rng, true)
}

fn allocate_random_recv_buf(rng: &mut Pcg64Mcg) -> (usize, usize) {
	allocate_random_buf(rng, false)
}

fn get_vec_offset(rng: &mut Pcg64Mcg) -> usize {
	let max_offset = if rng.gen_range(0..1) > 0 {
		PAGE_SIZE
	}else{
		size_of::<usize>() * 8
	};
	let ret = PAGE_SIZE - (rng.gen_range(0..max_offset) / size_of::<usize>()) * size_of::<usize>();
	ret
}

fn allocate_random_recv_bufs(rng: &mut Pcg64Mcg, add_header: bool) -> RecvVector {
	info!("allocating receive buffers");
	let mut total = 0;
	let mut recv_buf_count = rng.gen_range(1..MAX_BUFFERS_PER_ITERATION);
	let mut addrs = Vec::new();
	if add_header {
		unsafe { addrs.push((HEADER.as_bytes_mut().as_mut_ptr() as usize, HEADER_SIZE)) }
	}
	for _ in 0..recv_buf_count {
		let (buf_addr, buf_size) = allocate_random_recv_buf(rng);
		addrs.push((buf_addr, buf_size));
		total += buf_size;
	}
	if add_header {
		recv_buf_count += 1;
	}
	let vec_addr = allocate_extended_receive_vec(&mut addrs, get_vec_offset(rng));
	RecvVector::new(vec_addr, recv_buf_count, total)
}

fn allocate_random_send_bufs(rng: &mut Pcg64Mcg) -> SendVector {
	info!("allocating send buffers");
	let mut total = 0;
	let send_buf_count = rng.gen_range(1..MAX_BUFFERS_PER_ITERATION);
	let mut addrs = Vec::new();
	for _ in 0..send_buf_count {
		let (buf_addr, buf_size) = allocate_random_send_buf(rng);
		addrs.push((buf_addr, buf_size));
		total += buf_size;
	}
	let vec_addr = allocate_extended_send_vec(&mut addrs, get_vec_offset(rng));
	info!("allocate_random_send_bufs: {:x}", vec_addr);
	SendVector::new(vec_addr, send_buf_count, total)
}

pub fn run_random_test_simple(seed: u64, threads: &mut Vec<Arc<RwLock<SysThread>>>){
	let mut rng = Pcg64Mcg::seed_from_u64(seed);
	info!("running simple random test with seed {:x}", seed);
	let client_send_info = allocate_random_send_bufs(&mut rng);
	let client_recv_info = allocate_random_recv_bufs(&mut rng, false);
	let server_send_info = allocate_random_send_bufs(&mut rng);
	let server_recv_info = allocate_random_recv_bufs(&mut rng, true);

	info!("allocating full receive buffers");
	let mut server_full_recv_bufs = Vec::new();
	let mut server_full_recv_size = 0;
	let mut server_full_recv_count = 0;
	while server_full_recv_size < client_send_info.bytes() {
		let (buf_addr, buf_size) = allocate_random_recv_buf(&mut rng);
		server_full_recv_bufs.push((buf_addr, buf_size));
		server_full_recv_size += buf_size;
		server_full_recv_count += 1;
	}

	let server_full_recv_addr = allocate_extended_receive_vec(&mut server_full_recv_bufs, get_vec_offset(&mut rng));

	let server_full_recv_info = RecvVector::new(server_full_recv_addr, server_full_recv_count, server_full_recv_size);

	info!("client send size: {}", client_send_info.bytes());
	info!("client receive size: {}", client_recv_info.bytes());
	info!("server send size: {}", server_send_info.bytes());
	info!("server receive size: {}", server_recv_info.bytes());
	info!("server full receive size: {}", server_full_recv_info.bytes());

	run_channel_test_closure(move |channel_id: i32, message_id: i32|{
		random_test_simple_server(channel_id, message_id, client_send_info.bytes(), server_recv_info, server_full_recv_info, server_send_info);
	},
	move |channel_id: i32| {
		random_test_simple_client(channel_id, client_send_info, client_recv_info, server_recv_info, server_full_recv_info, server_send_info);
	},
	threads, AccessMode::ReadWrite,
	ServerChannelOptions::new().get_reply_size().get_recv_total_size());
}

//TODO: perform random message descriptor reads and writes (testing writes will require performing the same writes to a list of reference buffers and comparing them to the ones actually transferred)

fn random_test_simple_server(channel_id: i32, message_id: i32,
		client_send_size: usize, recv_buf_info: RecvVector,
		full_recv_buf_info: RecvVector, reply_buf_info: SendVector) ->
			Option<usize> {
	info!("random_test_simple_server: starting");
	let channel_fd = get_fd(channel_id);
	let message_fd = get_fd(message_id);

	info!("random_test_simple_server: waiting for message: {} {:p} {:p}", reply_buf_info.as_slice().len(), recv_buf_info.as_slice().as_mut_ptr(), reply_buf_info.as_slice().as_ptr());

	let cw_size = channel_fd.mreadv(recv_buf_info.as_slice(), message_id).expect("could not receive message from client") - HEADER_SIZE;
	let header = MsgHeader::from_long_bufs(recv_buf_info.as_slice()).expect("could not get header from buffer");

	info!("random_test_simple_server: receive done");

	let expected_receive_size = usize::min(client_send_size, recv_buf_info.bytes());
	assert!(cw_size == expected_receive_size, "random_test_simple_server: invalid receive size: {} instead of {}", cw_size, expected_receive_size);
	assert!(header.client_write_size == client_send_size, "random_test_simple_server: invalid total size: {} instead of {}", header.client_write_size, client_send_size);


	info!("random_test_simple_server: reading from message descriptor");
	for buf in full_recv_buf_info.as_slice(){
		info!("buf: {:p} {}", buf.base, buf.len);
	}

	let cw_full_size = message_fd.readv(full_recv_buf_info.as_slice()).expect("reading from client write buffer failed");
	assert!(cw_full_size == client_send_size, "message descriptor read returned {} instead of {}", cw_full_size, client_send_size);

	info!("random_test_simple_server: sending reply: {}", cw_size);
	if USE_MD_ACCESSORS.load(Ordering::Relaxed){
		message_fd.wpwritev(reply_buf_info.as_slice(), 0, OffsetType::Start).expect("cannot write reply to client");
		message_fd.mwpwritev(&[], 0, OffsetType::End, cw_full_size as ServerStatus).expect("cannot write reply to client");

	}else{
		message_fd.mwpwritev(reply_buf_info.as_slice(), 0, OffsetType::End, cw_full_size as ServerStatus).expect("cannot write reply to client");
	}
	info!("random_test_simple_server: reply done");
	None
}

fn random_test_simple_client(client_id: i32,
		send_buf_info: SendVector,
		recv_buf_info: RecvVector,

		server_recv_buf_info: RecvVector,
		server_full_recv_buf_info: RecvVector,
		server_reply_buf_info: SendVector)
			-> Option<usize>{
	info!("random_test_simple_client: starting");
	let fd = get_fd(client_id);

	info!("random_test_simple_client: sending message: {:p} {:p}", send_buf_info.as_slice().as_ptr(), recv_buf_info.as_slice().as_ptr());

	let (send_reply_size, received_reply_size) = fd.writereadv(send_buf_info.as_slice(), recv_buf_info.as_slice()).expect("cannot write/read client FD");

	assert!(send_reply_size == send_buf_info.bytes(), "server replied with invalid size {} instead of {}", send_reply_size, send_buf_info.bytes());
	let reply_size = usize::min(recv_buf_info.bytes(), server_reply_buf_info.bytes());
	assert!(received_reply_size == reply_size, "invalid reply size received: {} instead of {}", received_reply_size, reply_size);

	info!("random_test_simple_client: checking buffers");

	check_random_bufs(&send_buf_info.as_raw(), &server_recv_buf_info.as_raw()[1..]).expect("mismatch between client send buffers and server receive buffers");
	check_random_bufs(&send_buf_info.as_raw(), &server_full_recv_buf_info.as_raw()).expect("mismatch between client send buffers and server message copy buffers");
	check_random_bufs(&recv_buf_info.as_raw(), &server_reply_buf_info.as_raw()).expect("mismatch between client receive buffers and server send buffers");
	info!("random_test_simple_client: done");
	None
}

const RANDOM_TEST_ITERATIONS: usize = 20;
pub fn run_random_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	let mut rng = Pcg64Mcg::seed_from_u64(0x1234567890abcdef);
	for _ in 0..RANDOM_TEST_ITERATIONS {
		let seed = rng.gen_range(0..u64::MAX);
		run_random_test_simple(seed, threads);
	}
}

/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
