/*
 * Copyright (c) 2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///This is the filesystem namespace manager

use core::str;

use alloc::boxed::Box;
use alloc::sync::Arc;
use alloc::vec::Vec;

use super::rpc::{
	FSContext,
	get_fs_context,
};

use super::transport::{
	FileDescription,
	FileDescriptorType,
};

use crate::{
	add_arc_list_slab,
	add_arc_slab,
};

use crate::vfs::rpc::ConnectResult;

use crate::utils::{
	LockedArcList,
	ArcListItem,
	add_slab,
};

use typed_path::{
	UnixPath,
	UnixPathBuf,
};

use usync::{
	RwLock,
	RwLockReadGuard,
	RwLockWriteGuard,
};

use usync_once_cell::OnceCell;

use intrusive_collections::{
	LinkedList,
	LinkedListAtomicLink,
	UnsafeRef,
};

use uxrt_transport_layer::{
	MsgHeader,
	SRV_ERR_INVAL,
	SRV_ERR_NOENT,
	ServerStatus,
};

use uxrt_vfs_rpc::{
	VFSServerOpenResult,
	dev_t,
	gid_t,
	mode_t,
	open_how,
	stat,
	timespec,
	uid_t,
};

use super::VFSError;

///Trait for mounts in a `MountTable`. Colocated filesystems should generally
///be written as standard resource managers rather than adding custom types here.
///
///The only reason to write a filesystem as a mount handler instead of a
///resource manager is if it manipulates file descriptions directly instead of
///having them pre-allocated, and basically everything except for PortFS can
///be implemented with pre-allocated file descriptions.
pub trait Mount {
	///Adds a path to the list of allowed path prefixes for this mount. Any
	///path without a prefix on this list will return ENOENT. If no paths
	///have been added, all paths will be accepted.
	fn allow_path(&self, path: &UnixPath) -> Result<(), ()>{
		Err(())
	}
	///Handler for openat2()
	fn handle_openat2(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, _path: &UnixPath, _how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<OpenResult, ServerStatus> {
		Err(SRV_ERR_NOENT)
	}
	///Handler for open request failures after the open request itself
	///succeeded (e.g. adding the file descriptor)
	fn handle_open_fail(&self, _desc: &Arc<FileDescription>, _fd_type: FileDescriptorType, _badge: u32) {
	}
	///Handler for openfd()
	fn handle_openfd(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, fd: i32, flags: i32) -> Result<i32, ServerStatus> {
		info!("TODO: openfd: {} {:x}", fd, flags);
		Ok(0xabcd)
	}
	///Handler for dup3()
	fn handle_dup3(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, oldfd: i32, newfd: i32, flags: i32) -> Result<i32, ServerStatus> {
		info!("TODO: dup3: {} {} {:x}", oldfd, newfd, flags);
		Ok(0xabcd)
	}
	///Handler for fcntl()
	fn handle_fcntl(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, fd: i32, cmd: i32, _arg: &[u8], ret: &mut [u8]) -> Result<usize, ServerStatus> {
		info!("TODO: fcntl: {} {}", fd, cmd);
		ret[0] = 0xab;
		Ok(1)
	}
	///Handler for linkat()
	fn handle_linkat(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, olddirfd: i32, oldpath: &UnixPath, newdirfd: i32, newpath: &UnixPath, flags: i32) -> Result<(), ServerStatus> {
		info!("TODO: linkat: {} {} {} {} {:x}", olddirfd, oldpath, newdirfd, newpath, flags);
		Ok(())
	}
	///Handler for symlinkat()
	fn handle_symlinkat(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, target: &UnixPath, newdirfd: i32, linkpath: &UnixPath) -> Result<(), ServerStatus> {

		info!("TODO: symlinkat: {} {} {}", target, newdirfd, linkpath);
		Ok(())
	}
	///Handler for mknodat()
	fn handle_mknodat(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, dirfd: i32, path: &UnixPath, mode: mode_t, dev: dev_t) -> Result<(), ServerStatus> {
		info!("TODO: mknodat: {} {} {} {}", dirfd, path, mode, dev);
		Ok(())
	}
	///Handler for renameat2()
	fn handle_renameat2(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, olddirfd: i32, oldpath: &UnixPath, newdirfd: i32, newpath: &UnixPath, flags: i32) -> Result<(), ServerStatus> {
		info!("TODO: renameat2: {} {} {} {} {:x}", olddirfd, oldpath, newdirfd, newpath, flags);
		Ok(())
	}
	///Handler for unlinkat()
	fn handle_unlinkat(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, _pathname: &UnixPath, _flags: i32) -> Result<ConnectResult, ServerStatus> {
		Err(SRV_ERR_NOENT)
	}
	///Handler for getpages()
	fn handle_getpages(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, fd: i32, start: usize, size: usize) -> Result<usize, ServerStatus> {
		info!("TODO: getpages: {} {} {}", fd, start, size);
		Ok(0xabcd)
	}
	///Handler for mount()
	fn handle_mount(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, mount_id: i32, dev: &UnixPath, fstype: &[u8], opts: &[u8]) -> Result<(), ServerStatus> {
		let fstype_utf8 = core::str::from_utf8(fstype).expect("non-UTF-8 sequence in new path");
		let opts_utf8 = core::str::from_utf8(opts).expect("non-UTF-8 sequence in new path");

		info!("TODO: mount: {} {} {} {}", mount_id, dev, fstype_utf8, opts_utf8);
		Ok(())
	}
}

///A binding of an arbitrary path, either one within this FSSpace or a global
///path
pub struct PathBindingMount {
}

impl Mount for PathBindingMount {
}

///A binding of another mount table into this FSSpace
pub struct SubTableBindingMount {
	fsspace: Arc<MountTableContainer>
}

impl Mount for SubTableBindingMount {
}

///Hides everything from mounts located below it by stopping path resolution
///if encountered. May be used to approximate traditional Unix mount semantics.
///
///If this is encountered when reading a directory, entries from mounts above
///it will be returned to the client, but not ones below it. If this is
///encountered during any other path resolution, it will cause it to fail with
///ENOENT.
///
///All attempts to access files on this will fail with ENOENT.
pub struct HideMount {
}

impl Mount for HideMount {
	///Handler for openat2()
	fn handle_openat2(&self, _transport_header: &MsgHeader, _fs_context: &FSContext, _path: &UnixPath, _how: &open_how, _secondary_bufs: &mut [Vec<u8>], _secondary_len: &mut [usize]) -> Result<OpenResult, ServerStatus> {
		Ok(OpenResult::Abort)
	}
}

///Returned when opening a file
///
///Restart restarts name resolution with a different path
///Finish returns a file description to the RPC handler
pub enum OpenResult {
	Restart(VFSServerOpenResult, Vec<u8>, Arc<FSContext>),
	Finish(Option<Arc<FileDescription>>, FileDescriptorType, u32, u64, u64, Option<usize>),
	Abort,
}

///Returned from the closure passed to MountTable::walk()
///
///Returning Continue lets resolution continue to match entries
///Returning Finish stops resolution immediately
pub enum WalkResult {
	Continue,
	Finish,
}

///Specifies how to match the path when unmounting
///
///First matches the first mount with the path
///Last matches the last mount with the path
///All matches all mounts with the path
///Unspecified doesn't specify a path (in which case the index must be
///specified)
pub enum UnmountPath<'a> {
	First(&'a str),
	Last(&'a str),
	All(&'a str),
	Unspecified,
}

///A mount table entry
pub struct MountEntry {
	path: UnixPathBuf,
	handler: Arc<dyn Mount + Send + Sync>,
	link: LinkedListAtomicLink,
}

impl MountEntry {
	///Creates a new `MountEntry`
	fn new(path: UnixPathBuf, handler: Arc<dyn Mount + Send + Sync>) -> UnsafeRef<Self> {
		UnsafeRef::from_box(Box::new(Self {
			path,
			handler,
			link: Default::default(),
		}))
	}
}

intrusive_adapter!(MountAdapter = UnsafeRef<MountEntry>: MountEntry { link: LinkedListAtomicLink });

///A filesystem mount table
pub struct MountTable {
	mounts: LinkedList<MountAdapter>,
}

impl MountTable {
	///Constructs a new `MountTable`
	pub fn new() -> Self {
		Self {
			mounts: Default::default()
		}
	}
	///Adds a mount to this table at the specified path prefix. The index
	///specifies where it should be inserted in the list.
	///
	///The source path may be a global path or a regular path. If the root
	///of a global path is specified, the corresponding object will be
	///mounted directly. Otherwise, a subdirectory binding will be created.
	pub fn mount(&mut self, index: isize, src: &UnixPath, dest: &UnixPath) -> Result<Arc<dyn Mount>, VFSError> {
		//info!("MountTable::mount: {} {} {}", index, src, dest);
		if src.is_relative() || dest.is_relative() {
			return Err(VFSError::InvalidArgument);
		}
		let mount = match lookup_global(src) {
			Ok((m, r)) => {
				if r.is_none() {
					m
				}else{
					warn!("TODO: implement subdirectory bindings for global paths");
					return Err(VFSError::FileNotFound);
				}
			},
			Err(e) => {
				if let Some(err) = e {
					return Err(err);
				}else{
					warn!("TODO: implement subdirectory bindings");
					return Err(VFSError::FileNotFound);
				}
			},
		};
		let start = dest.iter().take(1).next();
		if dest.is_relative() ||
			start.is_none() ||
			start.unwrap().starts_with(":".as_bytes()) {
			return Err(VFSError::InvalidArgument);
		}
		if index < 0 {
			let mut cursor = self.mounts.back_mut();
			let mut i = -1;
			while i > index && !cursor.is_null() {
				cursor.move_prev();
				i -= 1;
			}
			cursor.insert_before(MountEntry::new(UnixPathBuf::from(dest), mount.clone()));
		}else{
			let mut cursor = self.mounts.front_mut();
			let mut i = 0;
			while i < index && !cursor.is_null() {
				cursor.move_next();
				i += 1;
			}
			cursor.insert_after(MountEntry::new(UnixPathBuf::from(dest), mount.clone()));
		};
		Ok(mount)
	}
	///Unmounts a prefix.
	///
	///Either the index, path, or both may be specified. If only the index
	///is specified, the mount at that index will be removed regardless
	///of the path. The path is specified with the UnmountPath enum to
	///control where the path is matched. If only the path is specified,
	///the path will be matched according to the enum regardless of index.
	///If both are specified, the path must match and also must be at the
	///specified index.
	///
	///If neither is specified, this will fail.
	pub fn unmount(&self, index: Option<isize>, path: UnmountPath) -> Result<(), VFSError>{
		//TODO: implement this (make sure to drop the entry explicitly)
		Err(VFSError::InternalError)
	}
	///Runs the closure for all mounts that match the path. Each invocation
	///is passed the mount object and the remaining part of the path after
	///the prefix.
	pub fn walk<F>(&self, fallthrough: Result<(), ServerStatus>, path: &UnixPath, mut f: F) -> Result<(), ServerStatus> where F: FnMut(&UnixPath, &Arc<dyn Mount + Send + Sync>) -> Result<WalkResult, ServerStatus> {
		//info!("MountTable::walk");
		//TODO: it would be a good idea to somehow split this up into two phases - one where paths are matched and just pushed into some kind of list and one where the closure is run on each item in the list, so that the lock on the table can be released before running the closure (any closure that tries to modify the table somehow will probably deadlock as things stand now)
		match lookup_global(path) {
			Ok((handler, s)) => {
				let suffix = s.unwrap_or_else(|| { UnixPath::new("") });
				let res = f(suffix, &handler);
				if let Err(err) = res {
					return Err(err);
				}else{
					return Ok(());
				}
			},
			Err(e) => {
				if let Some(err) = e {
					return Err(err.to_errno());
				}
			},
		}
		let mut cursor = self.mounts.front();
		while let Some(mount) = cursor.get(){
			if let Ok(suffix) = path.strip_prefix(&mount.path) {
				//info!("MountTable::walk: {:?}", str::from_utf8(mount.path.as_bytes()));
				let res = f(suffix, &mount.handler);
				if let Err(err) = res {
					return Err(err);
				}
				match res.unwrap() {
					WalkResult::Continue => {},
					WalkResult::Finish => {
						return Ok(());
					},
				}
			}
			cursor.move_next();
		}
		fallthrough
	}
}

///Wrapper for `MountTable` that holds the ID and an RwLock
pub struct MountTableContainer {
	id: i32,
	contents: RwLock<MountTable>,
}

impl MountTableContainer {
	///Creates a new MountTableContainer
	fn new(contents: MountTable) -> Self {
		MountTableContainer {
			id: 0,
			contents: RwLock::new(contents),
		}
	}
	///Creates a new MountTableContainer with an empty MountTable
	fn new_empty() -> Self {
		Self::new(MountTable::new())
	}
	///Gets the ID of this MountTable
	pub fn get_id(&self) -> i32 {
		self.id
	}
	///Gets a read-only guard for this MountTable
	pub fn read(&self) -> RwLockReadGuard<MountTable> {
		self.contents.read()
	}
	///Get a read-write guard for this MountTable
	pub fn write(&self) -> RwLockWriteGuard<MountTable> {
		self.contents.write()
	}
}

impl ArcListItem for MountTableContainer {
	fn get_id(&self) -> i32 {
		self.id
	}
	fn set_id(&mut self, id: i32) {
		self.id = id;
	}
}

///Looks up a global (`/:<type>:<parameters>/`) path. These provide a way to
///unambiguously reference files across FDSpaces for internal/transient use.
///These are exposed to user space, but there is no guarantee that they won't
///change across reboots, or even across process starts/ends. The only
///guarantee of stability is that holding some kind of file descriptor to the
///underlying object will prevent the corresponding global prefix being
///re-used until the FD has been closed.
///
///Currently recognized global prefixes:
///
///	/:mount:<context>:<port>/	Accesses the root of an individual mount
///					exported by a resource manager
///	/:table:<id>/			Accesses the root of a mount table
///
///	/:hide/				Hides everything below it in the mount
///					table by stopping path resolution
///					below it if encountered
///All other /:* paths are illegal and will return ENOENT on all accesses.
///File/directory names starting with `:` are legal in directories other than
///the root (although seldom used).
pub fn lookup_global<'a>(path: &'a UnixPath) -> Result<(Arc<dyn Mount + Send + Sync>, Option<&'a UnixPath>), Option<VFSError>>{
	if path.is_relative(){
		return Err(None);
	}
	let mut iter = path.iter().take(3);
	let _ = iter.next();
	let start_raw = iter.next().ok_or(VFSError::FileNotFound)?;
	let start_path = UnixPath::new("/").join(start_raw);
	let start_utf8 = str::from_utf8(start_raw).or(Err(None))?;
	//info!("lookup_global: {} {}", path, start_utf8);
	if let Ok((context_id, port_id)) = scan_fmt!(&start_utf8, ":mount:{d}:{d}", i32, i32) {
		//info!("lookup_global: mount: {} {}", context_id, port_id);
		let context = get_fs_context(context_id).ok_or(VFSError::FileNotFound)?;
		let mount = context.get_mount(port_id)?;
		let suffix = if iter.next().is_some(){
			let suffix = path.strip_prefix(start_path).or(Err(VFSError::InvalidArgument))?;
			Some(suffix)
		}else{
			None
		};
		Ok((mount, suffix))
	}else if let Ok(fsspace) = scan_fmt!(&start_utf8, ":table:{d}", i32) {
		//TODO: this should look up a mount table by its ID
		warn!("TODO: implement /:table: paths");
		Err(Some(VFSError::InternalError))
	}else if start_utf8 == ":hide" {
		if iter.next().is_none(){
			Ok((HIDE_MOUNT.get_or_init(||{
				Arc::new(HideMount {})
			}).clone(), None))
		}else{
			Err(Some(VFSError::FileNotFound))
		}
	}else if start_utf8.starts_with(":"){
		//absolute paths with a `:` as the first character of the first
		//element are reserved
		Err(Some(VFSError::FileNotFound))
	}else{
		Err(None)
	}
}


static mut ROOT_DEFAULT_FSSPACE: Option<Arc<MountTableContainer>> = None;
static mut FSSPACE_LIST: Option<LockedArcList<MountTableContainer>> = None;
static HIDE_MOUNT: OnceCell<Arc<HideMount>> = OnceCell::new();

///Initializes FSSpace management
pub fn init_fsspaces() {
	unsafe { FSSPACE_LIST = Some(LockedArcList::new()) };
	let fsspace_list = get_fsspace_list();
	let (_, root_fsspace) = fsspace_list.add(MountTableContainer::new_empty()).expect("could not add root FSSpace to list");
	unsafe { ROOT_DEFAULT_FSSPACE = Some(root_fsspace) };
}

///Gets the FSSpace list
pub fn get_fsspace_list() -> &'static LockedArcList<MountTableContainer> {
	unsafe { FSSPACE_LIST.as_ref().expect("FSSpace list unset") }
}

///Gets the default root FSSpace
pub fn get_root_fsspace() -> Arc<MountTableContainer> {
	unsafe { ROOT_DEFAULT_FSSPACE.as_ref().expect("root FSSpace unset").clone() }
}

const ROOT_FSSPACE_ID: i32 = 0;

///Adds FSSpace-related custom slabs
pub fn add_custom_slabs() {
	add_arc_list_slab!(MountTableContainer, 512, 4, 4, 2, "MountTableContainer");
	add_arc_slab!(MountTableContainer, 512, 4, 4, 2, "MountTableContainer");
	add_arc_slab!(PathBindingMount, 512, 4, 4, 2, "PathBindingMount");
	add_arc_slab!(SubTableBindingMount, 512, 4, 4, 2, "SubTableBindingMount");
	add_slab::<MountEntry>(512, 4, 4, 2, "MountEntry");
}
