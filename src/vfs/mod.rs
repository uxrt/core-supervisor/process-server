/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */
///This module includes the VFS along with glue code for the IPC transport
///layer.

pub mod transport;
pub mod rpc;
pub mod fsspace;
pub mod dir;
pub mod vnode;

use alloc::sync::Arc;

use sel4_alloc::{cspace, vspace};

pub use self::dir::{
	init_cwd,
	init_dir_handler,
};

pub use self::fsspace::{
	get_root_fsspace,
	init_fsspaces,
};

pub use self::rpc::{
	get_rpc_handler,
	init_rpc_handler,
	start_rpc_handler,
};

use self::rpc::get_root_default_fs_context;

pub use transport::{
	get_root_fd_arrays,
	get_root_fdspace,
	init_fdspace_list,
};

use transport::{
	FileDescription,
	FileDescriptionType,
	FileDescriptorType,
	fdspace_root_thread_init,
};

use uxrt_transport_layer::{
	AccessMode,
	FDArray,
	FileDescriptorRef,
	IOError,
	SRV_ERR_INVAL,
	SRV_ERR_IO,
	SRV_ERR_MFILE,
	SRV_ERR_NOENT,
	SRV_ERR_PERM,
	ServerChannelOptions,
	ServerStatus,
	get_fd,
};

use uxrt_vfs_resmgr::{
	MessageFDAllocator,
	OpenControlBlock,
	ResMgrContextExtra,
	ResMgrMsgHandler,
	ResMgrPort,
};

use intrusive_collections::UnsafeRef;
use typed_path::UnixPath;

///Enum for VFS errors
#[derive(Clone, Copy, Debug, Fail)]
pub enum VFSError {
	#[fail(display = "I/O error")]
	IOError(uxrt_transport_layer::IOError),
	#[fail(display = "CSpace error")]
	CSpaceError(cspace::CSpaceError),
	#[fail(display = "VSpace error")]
	VSpaceError(vspace::VSpaceError),
	#[fail(display = "Too many open files")]
	TooManyFiles,
	#[fail(display = "Permission denied")]
	PermissionDenied,
	#[fail(display = "ID already in use")]
	IDInUse,
	#[fail(display = "Invalid argument")]
	InvalidArgument,
	#[fail(display = "File not found")]
	FileNotFound,
	#[fail(display = "Internal error")]
	InternalError,
}

impl VFSError {
	///Gets the errno for this `VFSError`
	pub fn to_errno(&self) -> ServerStatus {
		match self {
			Self::IOError(err) => err.to_errno(),
			Self::CSpaceError(err) => {
				match err {
					cspace::CSpaceError::CSpaceExhausted => SRV_ERR_MFILE,
					_ => SRV_ERR_IO,
				}
			},
			Self::PermissionDenied => SRV_ERR_PERM,
			Self::TooManyFiles => SRV_ERR_MFILE,
			Self::InvalidArgument => SRV_ERR_INVAL,
			Self::FileNotFound => SRV_ERR_NOENT,
			_ => SRV_ERR_IO,
		}
	}
}

impl From<VFSError> for ServerStatus {
	fn from(value: VFSError) -> ServerStatus {
		value.to_errno()
	}
}

impl From<ServerStatus> for VFSError {
	fn from(value: ServerStatus) -> VFSError {
		VFSError::IOError(IOError::ServerError(value))
	}
}

impl From<IOError> for VFSError {
	fn from(value: IOError) -> VFSError {
		VFSError::IOError(value)
	}
}

impl From<VFSError> for IOError {
	fn from(value: VFSError) -> IOError {
		IOError::ServerError(value.to_errno())
	}
}


///Wraps a CSpaceError in a VFSError
pub fn vfs_cspace_result<T>(res: Result<T, cspace::CSpaceError>) -> Result<T, VFSError> {
	res.or_else(|err| { Err(VFSError::CSpaceError(err)) })
}

///Creates a new server channel for VFS use (regular drivers implemented as
///resource managers should create their server channels through PortFS instead)
pub fn new_vfs_server_channel() -> Result<(Arc<FileDescription>, FileDescriptorRef), VFSError>{
	let desc = FileDescription::new(
		FileDescriptionType::IPCChannel(
			ServerChannelOptions::new().get_recv_total_size()
			.get_reply_size()),
		AccessMode::ReadWrite, 0, true, false, false, None, None, None)?;
	let fdspace = get_root_fdspace();
	let mut guard = fdspace.write();
	let channel_id = guard.insert(&desc, FileDescriptorType::Server, AccessMode::ReadWrite, 0, None)?;
	Ok((desc, get_fd(channel_id)))
}

///Directly allocates a message descriptor into the root FDSpace (bypassing
///PortFS)
pub fn new_md() -> Result<FileDescriptorRef, VFSError> {
	let desc = FileDescription::new(FileDescriptionType::ServerMessage, AccessMode::ReadWrite, 0, false, true, true, None, None, None)?;
	let fdspace = get_root_fdspace();
	let id = fdspace.write().insert(&desc, FileDescriptorType::Server, AccessMode::ReadWrite, 0, None)?;
	//info!("MessageDescriptorPool::allocate_item: {}", id);
	Ok(get_fd(id))
}

///Creates a new resource manager port and mounts it in the initial FSSpace
pub fn new_resmgr_port<O: OpenControlBlock, H: ResMgrMsgHandler<O, E>, E: ResMgrContextExtra>(handler: Arc<H>, flags: u64, path: &str, allowed_paths: &[&UnixPath]) -> Result<Arc<ResMgrPort<O, H, E>>, IOError> {
	let port = ResMgrPort::new(handler, flags)?;
	let fs_context = get_root_default_fs_context();
	let src = format!("/:mount:{}:{}", fs_context.get_id(), port.get_port_id());
	let mount = fs_context.mount(-1, UnixPath::new(&src), UnixPath::new(path))?;
	for ref path in allowed_paths {
		mount.allow_path(path).or(Err(IOError::ServerError(SRV_ERR_IO)))?;
	}
	info!("created new resource manager port: {}", port.get_port_id());
	Ok(port)
}

///Message FD allocator that manipulates the main process server FDSpace
///directly rather than using PortFS
pub struct BasicMessageFDAllocator {}

impl BasicMessageFDAllocator {
	///Creates a new `BasicMessageFDAllocator`
	pub fn new() -> Arc<Self> {
		Arc::new(Self {})
	}
}

impl MessageFDAllocator for BasicMessageFDAllocator {
	fn new_message_fd(&self) -> Result<(FileDescriptorRef, i32), IOError> {
		Ok((new_md()?, -1))
	}
}

///Adds custom slabs for a device resource manager OCB
#[macro_export]
macro_rules! add_device_ocb_slabs {
	($slab_type: ty, $slab_size: expr, $min_free: expr, $max_dealloc_slabs: expr, $max_drop_rounds: expr, $description: expr) => {
		{
			use crate::global_heap_alloc::get_kobj_alloc;
			let kobj_alloc = get_kobj_alloc();
			uxrt_vfs_resmgr::add_device_ocb_slabs!(&kobj_alloc, $slab_type, $slab_size, $min_free, $max_dealloc_slabs, $max_drop_rounds, $description);
		}
	}
}

///Adds custom slabs for a tree resource manager OCB
#[macro_export]
macro_rules! add_tree_ocb_slabs {
	($ocb_type: ty, $node_type: ty, $slab_size: expr, $min_free: expr, $max_dealloc_slabs: expr, $max_drop_rounds: expr, $description: expr) => {
		{
			use crate::global_heap_alloc::get_kobj_alloc;
			let kobj_alloc = get_kobj_alloc();
			uxrt_vfs_resmgr::add_tree_ocb_slabs!(&kobj_alloc, $ocb_type, $node_type, $slab_size, $min_free, $max_dealloc_slabs, $max_drop_rounds, $description);
		}
	}
}


///Initialize VFS-related thread state for a root server thread
pub fn vfs_root_thread_init(fdspace: (UnsafeRef<FDArray>, UnsafeRef<FDArray>)) {
	fdspace_root_thread_init(fdspace.0, fdspace.1);
}

///Sets up mounts (this function may be removed at some point)
pub fn tmp_setup_mounts(){
	get_root_fsspace().write().mount(0, &UnixPath::new("/:mount:0:-1"), &UnixPath::new("/proc/fscontext/self/ports")).expect("cannot mount root server port FS");
}

///Adds VFS-related custom slabs
pub fn add_custom_slabs() {
	rpc::add_custom_slabs();
	transport::add_custom_slabs();
	fsspace::add_custom_slabs();
	dir::add_custom_slabs();
	vnode::add_custom_slabs();
}
