/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///File descriptions and related state

use core::cell::Cell;
use core::sync::atomic::{
	AtomicBool,
	AtomicU64,
	AtomicUsize,
	Ordering,
};

use alloc::sync::{
	Arc,
	Weak,
};

use typed_path::{
	UnixPath,
	UnixPathBuf,
};

use sel4::{
	Badge,
	CapRights,
	Endpoint,
	FromSlot,
	Notification,
	Reply,
	SlotRef,
	ToCap,
	null_slot,
};

use sel4_alloc::AllocatorBundle;
use sel4_alloc::cspace::{
	AllocatableSlotRef,
	CSpaceManager,
};

use intrusive_collections::{
	Bound,
	KeyAdapter,
	RBTree,
	RBTreeAtomicLink,
};

use crate::global_heap_alloc::get_kobj_alloc;

use crate::add_arc_slab;

use crate::drivers::portfs::GlobalPortID;

use crate::task::thread_pool::run_io_thread;

use super::vfs_cspace_result;

use usync::{
	Mutex,
	MutexGuard,
	RwLock,
};

use usync_once_cell::OnceCell;

use uxrt_transport_layer::{
	AccessMode,
	CLIENT_ENDPOINT_COPIES,
	ClientBadge,
	IOError,
	SRV_ERR_IO,
	ServerChannelOptions,
	ServerStatus,
	UnifiedFileDescriptor,
	shared_client_clunk,
};

use uxrt_vfs_rpc::{
	O_DIRECTORY,
	O_PATH,
};

use super::super::VFSError;

use super::allocate_id;

use super::fdspace::{
	FDInternal,
	FDCSpace,
};

use super::super::vnode::{
	Vnode,
	get_vnode_list,
};

///The type of a file description
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum FileDescriptionType {
	IPCChannel(ServerChannelOptions),
	ServerMessage,
	Notification,
}

///Container for lists and pointers to file descriptors/descriptions
///associated with a file description (dependent on the sharing mode)
enum FDInfo {
	Private {
		num_servers: AtomicUsize,
		servers: Mutex<RBTree<FileDescriptionFDAdapter>>,
	},
	SharedClient {
		server: Arc<FileDescription>,
	},
	SharedServer {
		num_servers: AtomicUsize,
		servers: Mutex<RBTree<FileDescriptionFDAdapter>>,
		search_start: AtomicUsize,
	},
}

impl FDInfo {
	///Gets the sharing mode as a string for debugging
	fn get_type_string(&self) -> &'static str {
		match self {
			Self::Private { .. } => "Private",
			Self::SharedClient { .. } => "SharedClient",
			Self::SharedServer { .. } => "SharedServer",
		}

	}
}

///Holds the kernel capbilities and related state associated with a
///`FileDescription`.
pub(super) struct CapInfo {
	pub(super) base_type: FileDescriptionType,
	alloc_endpoint: SlotRef,
	alloc_reply: SlotRef,
	alloc_notification: SlotRef,
	base_endpoint: SlotRef,
	base_reply: SlotRef,
	base_notification: SlotRef,
	control_caps: [Option<Endpoint>; CLIENT_ENDPOINT_COPIES],
	//TODO: put this inside an RwLock and include a connection ID (which may be different than that of the file description itself) so a control FD to the not-yet-implemented PortFS resource manager can be allocated on demand for ports
	pub(super) control_fd: Option<UnifiedFileDescriptor>,
	pub(super) server_options: ServerChannelOptions,
	caps_deallocated: Mutex<Cell<bool>>,
}

impl Clone for CapInfo {
	fn clone(&self) -> Self {
		Self {
			base_type: self.base_type,
			alloc_endpoint: self.alloc_endpoint,
			alloc_reply: self.alloc_reply,
			alloc_notification: self.alloc_notification,
			base_endpoint: self.base_endpoint,
			base_reply: self.base_reply,
			base_notification: self.base_notification,
			control_caps: self.control_caps,
			control_fd: self.control_fd.clone(),
			server_options: self.server_options,
			caps_deallocated: Mutex::new(Cell::new(false)),
		}
	}
}

impl CapInfo {
	///Create a new `CapInfo`
	fn new(base_type: FileDescriptionType, shared: bool) ->Result<Self, VFSError> {
		match base_type {
			FileDescriptionType::IPCChannel(_) => {},
			_ => {
				if shared {
					return Err(VFSError::InternalError);
				}
			},
		}
		let kobj_alloc = get_kobj_alloc();
		let (alloc_endpoint, alloc_reply, alloc_notification, base_endpoint, base_reply, base_notification, server_options, has_control_connection) = match base_type {
			FileDescriptionType::IPCChannel(options) => {
				let alloc = vfs_cspace_result(
					kobj_alloc.cspace()
					.allocate_slot_with_object_ref_fixed::<Endpoint, _>(&kobj_alloc))?;
				let base = vfs_cspace_result(
					alloc.copy_to_new(kobj_alloc.cspace(), CapRights::all(), &kobj_alloc))?;

				(alloc, null_slot(), null_slot(), base, null_slot(), null_slot(), options, true)
			},
			FileDescriptionType::ServerMessage => {
				let alloc = vfs_cspace_result(
					kobj_alloc.cspace()
					.allocate_slot_with_object_ref_fixed::<Reply, _>(&kobj_alloc))?;
				let base = vfs_cspace_result(
					alloc.copy_to_new(kobj_alloc.cspace(), CapRights::all(), &kobj_alloc))?;

				(null_slot(), alloc, null_slot(), null_slot(), base, null_slot(), ServerChannelOptions::new(), false)
			},
			FileDescriptionType::Notification => {
				let alloc = vfs_cspace_result(
					kobj_alloc.cspace()
					.allocate_slot_with_object_ref_fixed::<Notification, _>(&kobj_alloc))?;
				let base = vfs_cspace_result(
					alloc.copy_to_new(kobj_alloc.cspace(), CapRights::all(), &kobj_alloc))?;

				(null_slot(), null_slot(), alloc, null_slot(), null_slot(), base, ServerChannelOptions::new(), false)
			},
		};
		let mut info = Self {
			base_type,
			alloc_endpoint,
			alloc_reply,
			alloc_notification,
			base_endpoint,
			base_reply,
			base_notification,
			control_caps: [None; CLIENT_ENDPOINT_COPIES],
			control_fd: None,
			server_options,
			caps_deallocated: Mutex::new(Cell::new(false)),
		};
		if has_control_connection {
			let slots = info.mint_caps(&FDCSpace::Root, FileDescriptorType::Client, AccessMode::ReadWrite, 0, 0)?;
			for i in 0..slots.len() {
				if let Some(slot) = slots[i] {
					info.control_caps[i] = Some(Endpoint::from_slot(slot))
				}
			}
			info.control_fd = Some(UnifiedFileDescriptor::new_client(info.control_caps.clone(), AccessMode::ReadWrite));
		}

		Ok(info)
	}
	///Mint capabilities for a new file descriptor
	pub(super) fn mint_caps(&self, cspace: &FDCSpace, fd_type: FileDescriptorType, access: AccessMode, badge: u32, connection_id: i32) -> Result<[Option<SlotRef>; CLIENT_ENDPOINT_COPIES], VFSError> {
		let base_slot = match self.base_type {
			FileDescriptionType::IPCChannel(_) => self.base_endpoint,
			FileDescriptionType::ServerMessage => self.base_reply,
			FileDescriptionType::Notification => self.base_notification,
		};

		let kobj_alloc = get_kobj_alloc();
		let cspace_guard = cspace.lock();
		let mut user_slots = [None; CLIENT_ENDPOINT_COPIES];
		let badges = if fd_type.is_client() && let FileDescriptionType::IPCChannel(_) = self.base_type {
			ClientBadge::pack(access, connection_id, badge)
		}else if let FDCSpace::Root = cspace {
			user_slots = [Some(base_slot), None, None];
			[None, None, None]
		}else{
			[Some(0), None, None]
		};

		let mut alloc_err = None;
		for i in 0..badges.len() {
			let badge = if let Some(b) = badges[i] {
				b
			}else{
				continue;
			};
			let slot = match base_slot.mint_to_new(&cspace_guard, CapRights::all(), Badge::new(badge), &kobj_alloc) {
				Ok(user_slot) => {
					user_slots[i] = Some(user_slot);
					user_slot
				}
				Err(err) => {
					warn!("FDInternal::added: could not copy endpoint: {:?}", err);
					alloc_err = Some(VFSError::CSpaceError(err));
					break;
				},
			};
			user_slots[i] = Some(slot);
		}
		if let Some(err) = alloc_err {
			for i in 0..badges.len() {
				if badges[i].is_none() {
					continue;
				}
				if let Some(slot) = user_slots[i] {
					if let Err(err) = cspace_guard.free_and_delete_slot(slot, &kobj_alloc) {
						warn!("FDInternal::added: could not free endpoint slot after error: {:?}", err);
					}
				}
			}
			return Err(err);
		}
		Ok(user_slots)
	}
	///Gets the control file descriptor for this file description
	pub fn get_control_fd(&self) -> Option<UnifiedFileDescriptor> {
		self.control_fd.clone()
	}
	///Gets the file descriptor to use for I/O RPC requests for this file
	///description
	pub fn get_rpc_io_fd(&self) -> Result<UnifiedFileDescriptor, IOError> {
		let res = self.get_control_fd().expect("TODO: implement RPC I/O FD override (for PortFS)");
		Ok(res)
	}
	///Deallocate the slots used for allocating file descriptors from this
	///file description
	fn deallocate_caps(&self) -> Result<(), VFSError>{
		let caps_deallocated = self.caps_deallocated.lock();
		if caps_deallocated.get() {
			return Ok(())
		}
		caps_deallocated.set(true);
		let mut res = Ok(());
		let kobj_alloc = get_kobj_alloc();
		let cspace = kobj_alloc.cspace();
		for opt in self.control_caps {
			if let Some(endpoint) = opt {
				let control_res = cspace.free_and_delete_slot_raw(endpoint.to_cap(), &kobj_alloc);
				if control_res.is_err() {
					res = control_res;
				}
			}
		}

		let base_res = match self.base_type {
			FileDescriptionType::IPCChannel(_) => cspace.free_and_delete_slot_with_object_ref_fixed::<Endpoint, _>(self.alloc_endpoint, &kobj_alloc),
			FileDescriptionType::ServerMessage => cspace.free_and_delete_slot_with_object_ref_fixed::<Reply, _>(self.alloc_reply, &kobj_alloc),
			FileDescriptionType::Notification => cspace.free_and_delete_slot_with_object_ref_fixed::<Notification, _>(self.alloc_notification, &kobj_alloc),
		};
		if base_res.is_err() {
			res = base_res;
		}
		if let Err(err) = res {
			warn!("could not deallocate capability for file description: {:?}", res);
			Err(VFSError::CSpaceError(err))
		}else{
			Ok(())
		}
	}
	///Deallocate the base objects associated with this file description
	fn deallocate_base_objects(&mut self) {
		let kobj_alloc = get_kobj_alloc();
		if self.base_endpoint.cptr != 0 {
			kobj_alloc.cspace().free_slot(self.base_endpoint, &kobj_alloc).expect("failed to free endpoint for FD");
			self.base_endpoint.cptr = 0;
		}
		if self.base_reply.cptr != 0 {
			kobj_alloc.cspace().free_slot(self.base_reply, &kobj_alloc).expect("failed to free endpoint for FD");
			self.base_reply.cptr = 0;
		}
		if self.base_notification.cptr != 0 {
			kobj_alloc.cspace().free_slot(self.base_notification, &kobj_alloc).expect("failed to free notification for FD");
			self.base_notification.cptr = 0;
		}
	}
}

///Holds the connection-related state associated with a `FileDescription`.
pub(super) struct ConnectionInfo {
	num_clients: AtomicUsize,
	clients: Mutex<RBTree<FileDescriptionFDAdapter>>,
	fd_info: FDInfo,
	client_opened: Mutex<Cell<bool>>,
	server_opened: Mutex<Cell<bool>>,
	clunk_sent: Mutex<Cell<bool>>,
	final_clunk_sent: Mutex<Cell<bool>>,
	clunk_on_no_clients: AtomicBool,
	clunk_on_no_servers: AtomicBool,
	connection_id: i32,
}



impl ConnectionInfo {
	///Creates a new `ConnectionInfo`
	fn new(fd_info: FDInfo, connection_id: i32, clunk_on_no_clients: bool, clunk_on_no_servers: bool) -> Self {
		Self {
			num_clients: AtomicUsize::new(0),
			clients: Default::default(),
			fd_info,
			client_opened: Mutex::new(Cell::new(false)),
			server_opened: Mutex::new(Cell::new(false)),
			clunk_sent: Mutex::new(Cell::new(false)),
			final_clunk_sent: Mutex::new(Cell::new(false)),
			clunk_on_no_clients: AtomicBool::new(clunk_on_no_clients),
			clunk_on_no_servers: AtomicBool::new(clunk_on_no_servers),
			connection_id,
		}
	}
	///Creates a new `ConnectionInfo` for a shared client
	fn new_shared_client(&self, desc: Arc<FileDescription>, connection_id: i32) -> Self {
		let fd_info = FDInfo::SharedClient {
			server: desc.clone(),
		};
		Self::new(fd_info, connection_id, self.clunk_on_no_clients.load(Ordering::Relaxed), self.clunk_on_no_servers.load(Ordering::Relaxed))
	}
	///Returns true iff this is a shared server file description
	pub fn is_shared_server(&self) -> bool {
		match self.fd_info {
			FDInfo::SharedServer { .. } => true,
			_ => false,
		}
	}
	///Gets the connection ID of this file description
	pub fn get_connection_id(&self) -> i32 {
		self.connection_id
	}
	///Removes a connection from this file description
	fn remove(&self, desc: &FileDescription, fd_type: FileDescriptorType, id_opt: Option<u64>) -> Result<(), VFSError> {
		//info!("FileDescription::remove: {:p} {:?} {:?}", self, id_opt, fd_type);
		let (src, target, src_remaining, shared_server, clunk_on_no_fds) = match (fd_type, &self.fd_info) {
			(FileDescriptorType::Client, FDInfo::Private { ref servers, .. }) => {
				(&self.clients, servers, &self.num_clients, None, self.clunk_on_no_clients.load(Ordering::Relaxed))
			},
			(FileDescriptorType::Server, FDInfo::Private { ref servers, ref num_servers, .. }) => {
				(servers, &self.clients, num_servers, None, self.clunk_on_no_servers.load(Ordering::Relaxed))
			},
			(FileDescriptorType::Client, FDInfo::SharedClient { ref server }) => {
				let servers = if let FDInfo::SharedServer { ref servers, .. } = server.conn_info.fd_info {
					servers
				}else{
					warn!("FileDescription::remove: shared client has server file description that is not a shared server (this should never happen!)");
					return Err(VFSError::InternalError);
				};
				(&self.clients, servers, &self.num_clients, Some(server), true)
			},
			(FileDescriptorType::Server, FDInfo::SharedServer { ref servers, ref num_servers, .. }) => {
				(servers, &self.clients, num_servers, None, self.clunk_on_no_servers.load(Ordering::Relaxed))
			},
			(FileDescriptorType::Client, FDInfo::SharedServer { ref servers, .. }) => {
				(&self.clients, servers, &self.num_clients, None, false)
			},
			_ => {
				warn!("FileDescription::remove: invalid FD type combination {:?} {}", fd_type, self.fd_info.get_type_string());
				return Err(VFSError::InternalError)
			},
		};
		let mut res = Ok(());
		let remaining = if let Some(id) = id_opt {
			let mut src_guard = src.lock();
			let mut src_cursor = src_guard.find_mut(&id);
			let fd = if let Some(f) = src_cursor.remove() {
				f
			}else{
				return Ok(());
			};
			drop(src_cursor);
			drop(src_guard);
			res = fd.close_local();
			src_remaining.fetch_sub(1, Ordering::Relaxed) - 1
		}else{
			src_remaining.load(Ordering::Relaxed)
		};
		if remaining != 0 {
			//info!("FDs left; not sending clunk");
			return res;
		}
		self.clunk(desc, clunk_on_no_fds, target, shared_server)?;
		res
	}
	///Gets the number of server file descriptors pointing to this file
	///description
	fn get_num_servers(&self) -> usize {
		match self.fd_info {
			FDInfo::Private { ref num_servers, .. } => num_servers.load(Ordering::Relaxed),
			FDInfo::SharedServer { ref num_servers, .. } => num_servers.load(Ordering::Relaxed),
			_ => 0,
		}
	}
	///Internal method to add a client file descriptor
	fn add_client(&self, fd: Arc<FDInternal>) -> Result<(), VFSError>{
		//info!("add_client: {:?}", fd.fd_id);
		self.clients.lock().insert(ConnectionInternal::new_fd(fd));
		self.num_clients.fetch_add(1, Ordering::Relaxed);
		self.client_opened.lock().set(true);
		Ok(())
	}
	///Internal method to add a server file descriptor
	fn add_server(&self, fd: Arc<FDInternal>) -> Result<(), VFSError>{
		//info!("add_server: {:?}", fd.fd_id);
		let (servers, num_servers) = match self.fd_info {
			FDInfo::Private { ref servers, ref num_servers, .. } => {
				(servers, num_servers)
			},
			FDInfo::SharedClient { .. } => { return Err(VFSError::IOError(IOError::InvalidOperation)) },
			FDInfo::SharedServer { ref servers, ref num_servers, .. } => {
				(servers, num_servers)
			},
		};
		servers.lock().insert(ConnectionInternal::new_fd(fd));
		num_servers.fetch_add(1, Ordering::Relaxed);
		self.server_opened.lock().set(true);
		Ok(())
	}
	///Adds a file descriptor
	pub(super) fn add(&self, fd: Arc<FDInternal>) -> Result<(), VFSError>{
		match fd.get_fd_type() {
			FileDescriptorType::Client => {
				self.add_client(fd)
			},
			FileDescriptorType::Server => {
				self.add_server(fd)
			},
		}
	}
	///Clunks this file description if it is a shared client
	fn clunk_shared_client(&self, desc: &FileDescription) -> Result<(), VFSError> {
		//info!("clunk_shared_client");
		let server = match self.fd_info {
			FDInfo::SharedClient { ref server, .. } => {
				server
			},
			_ => {
				warn!("attempt to send a shared client clunk for file description that is not a shared client (type: {:?})", self.fd_info.get_type_string());
				return Err(VFSError::InternalError)
			},
		};
		self.clunk(desc, true, &self.clients, Some(&server))
	}
	///Clunks this file description if required
	fn clunk(&self, desc: &FileDescription, clunk_on_no_fds: bool, target: &Mutex<RBTree<FileDescriptionFDAdapter>>, shared_server: Option<&Arc<FileDescription>>) -> Result<(), VFSError> {
		//info!("FileDescription::clunk {:p} {}", self, self.fd_info.get_type_string());
		let num_clients = self.num_clients.load(Ordering::Relaxed);
		let num_servers = self.get_num_servers();
		if let Some(final_clunk_fn) = desc.final_clunk_fn {
			//there is the possibility of a deadlock here if a
			//final clunk hook is set for a shared client, but
			//there is no way to create a shared client with one,
			//so that particular scenario shouldn't happen
			let final_clunk_sent = self.final_clunk_sent.lock();
			if !final_clunk_sent.get()
					&& num_clients == 0
					&& num_servers == 0 {
				final_clunk_fn(desc.global_id.load(Ordering::Relaxed));
				final_clunk_sent.set(true);
			}
		}

		if num_servers == 0 && num_clients == 0 {
			desc.list_unlink()
		}

		let clunk_sent = self.clunk_sent.lock();
		if clunk_sent.get() {
			return Ok(())
		}
		if clunk_on_no_fds || shared_server.is_some() {
			clunk_sent.set(true);
		}
		drop(clunk_sent);

		//info!("FileDescription::clunk");
		let mut res = Ok(());
		if clunk_on_no_fds {
			//info!("sending remote clunk");
			let target_guard = target.lock();
			let mut target_cursor = target_guard.front();
			while let Some(fd) = target_cursor.get(){
				let remote_res = fd.close_remote();
				if remote_res.is_err() {
					res = remote_res;
				}
				target_cursor.move_next();
			}
			if let Err(err) = desc.deallocate_caps(){
				res = Err(err);
			}
			//info!("remote clunk done");
		}
		if let Some(server) = shared_server {
			//info!("sending shared clunk");
			let connection_id = self.get_connection_id();
			if self.client_opened.lock().get(){
				let endpoint = Endpoint::from_slot(desc.cap_info.base_endpoint);
				let server_clone = server.clone();
				let thread_res = run_io_thread(move || {
					//info!("FileDescription::clunk: sending shared clunk");
					shared_client_clunk(endpoint, connection_id);
					let _ = server_clone.remove(FileDescriptorType::Client, connection_id as u64);
					//info!("FileDescription::clunk: shared clunk done");
				}, "shared_client_clunk");
				if let Err(err) = thread_res {
					//TODO: return something better here
					warn!("could not create clunk thread for shared FD: {:?}", err);
					res = Err(VFSError::InternalError);
				}
			}else{
				if let Err(err) = server.remove(FileDescriptorType::Client, connection_id as u64) {
					res = Err(err);
				}
			}
			//info!("shared clunk done");
		}
		res
	}
	///Internal implementation of clunk_on_no_clients; also called by
	///open_aborted() for shared clients
	fn clunk_on_no_clients_base(&self, desc: &FileDescription, enable: bool, force: bool) -> Result<(), VFSError> {
		//info!("clunk_on_no_clients");
		self.clunk_on_no_clients.store(enable, Ordering::Relaxed);
		let mut opened = self.client_opened.lock().get();
		if enable && force {
			opened = true;
		}
		if enable && opened {
			self.remove(desc, FileDescriptorType::Client, None)
		}else{
			Ok(())
		}
	}
	///Sets whether this file description sends a clunk to all clients when
	///no servers are present. If no servers are present when called and
	///this file description has been opened at least once, one will be
	///sent immediately
	pub fn clunk_on_no_servers(&self, desc: &FileDescription, enable: bool) -> Result<(), VFSError> {
		//info!("clunk_on_no_servers");
		self.clunk_on_no_servers.store(enable, Ordering::Relaxed);
		let opened = self.server_opened.lock();
		if enable && opened.get() {
			self.remove(desc, FileDescriptorType::Server, None)
		}else{
			Ok(())
		}
	}
}

///State shared between a group of interconnected file descriptors. May contain
///any number of clients and servers.
pub struct FileDescription {
	pub(super) cap_info: CapInfo,
	pub(super) conn_info: ConnectionInfo,
	pub(super) access: AccessMode,
	global_id: AtomicU64,
	inode: AtomicU64,
	final_clunk_fn: Option<fn(u64)>,
	vnode: OnceCell<Weak<Vnode>>,
	vnode_unlinked: AtomicBool,
	open_flags: u64,
	orig_path: Mutex<Option<UnixPathBuf>>,
	real_path: Mutex<Option<UnixPathBuf>>,
	path_node: Mutex<Option<Arc<FileDescriptionPathNode>>>,
	alt_rpc_desc_client: RwLock<Option<(Arc<FileDescription>, i32)>>,
	alt_rpc_desc_server: RwLock<Option<(Arc<FileDescription>, i32)>>,
	path_list_link: RBTreeAtomicLink,
	shared_client_link: RBTreeAtomicLink,
	pub vnode_link: RBTreeAtomicLink,
}

impl FileDescription {
	///Creates a new file description
	pub fn new(base_type: FileDescriptionType,
		   access: AccessMode,
		   open_flags: u64,
		   shared: bool,
		   clunk_on_no_clients: bool,
		   clunk_on_no_servers: bool,
		   alt_rpc_desc_client: Option<(Arc<FileDescription>, i32)>,
		   alt_rpc_desc_server: Option<(Arc<FileDescription>, i32)>,
		   final_clunk_fn: Option<fn(u64)>) -> Result<Arc<FileDescription>, VFSError> {
		//info!("FileDescription::new {:?} {:?} {} {}", base_type, access, shared, clunk_on_no_clients);
		let (fd_info, connection_id) = if shared {
			let info = FDInfo::SharedServer {
				num_servers: AtomicUsize::new(0),
				servers: Default::default(),
				search_start: AtomicUsize::new(0),
			};
			(info, 0)
		}else{
			let info = FDInfo::Private {
				num_servers: AtomicUsize::new(0),
				servers: Default::default(),
			};
			(info, 1)
		};

		let desc = FileDescription {
			cap_info: CapInfo::new(base_type, shared)?,
			conn_info: ConnectionInfo::new(fd_info, connection_id, clunk_on_no_clients, clunk_on_no_servers),
			access,
			global_id: AtomicU64::new(GlobalPortID::invalid().value()),
			inode: AtomicU64::new(0),
			final_clunk_fn,
			vnode: Default::default(),
			vnode_unlinked: AtomicBool::new(false),
			open_flags,
			orig_path: Mutex::new(None),
			real_path: Mutex::new(None),
			path_node: Default::default(),
			alt_rpc_desc_client: RwLock::new(alt_rpc_desc_client),
			alt_rpc_desc_server: RwLock::new(alt_rpc_desc_server),
			path_list_link: Default::default(),
			shared_client_link: Default::default(),
			vnode_link: Default::default(),
		};
		Ok(Arc::new(desc))
	}
	///Creates a new client file description linked to a shared server file
	///description
	pub fn new_shared_client(self: &Arc<FileDescription>, open_flags: u64, id: Option<i32>, alt_rpc_desc: Option<(Arc<FileDescription>, i32)>) -> Result<Arc<FileDescription>, VFSError> {
		let search_start = if let FDInfo::SharedServer { ref search_start, .. } = self.conn_info.fd_info {
			search_start
		}else{
			warn!("attempt to add shared client to file description that is not a shared server (this should never happen!) (type: {})", self.conn_info.fd_info.get_type_string());
			return Err(VFSError::InternalError);
		};

		let mut clients = self.conn_info.clients.lock();
		let connection_id = if let Some(id) = id {
			if !clients.find(&(id as u64)).is_null() {
				return Err(VFSError::IDInUse);
			}
			id
		}else{
			if let Some(id) = allocate_id!(clients, search_start.load(Ordering::Relaxed) as u64) {
				id
			}else{
				return Err(VFSError::TooManyFiles);
			}
		};
		let client = Arc::new(FileDescription {
			cap_info: self.cap_info.clone(),
			conn_info: self.conn_info.new_shared_client(self.clone(), connection_id),
			access: self.access,
			global_id: AtomicU64::new(self.get_global_id()),
			inode: AtomicU64::new(0),
			final_clunk_fn: None,
			vnode: Default::default(),
			vnode_unlinked: AtomicBool::new(false),
			open_flags,
			orig_path: Mutex::new(None),
			real_path: Mutex::new(None),
			path_node: Default::default(),
			alt_rpc_desc_client: RwLock::new(alt_rpc_desc),
			alt_rpc_desc_server: RwLock::new(None),
			path_list_link: Default::default(),
			shared_client_link: Default::default(),
			vnode_link: Default::default(),
		});
		self.conn_info.num_clients.fetch_add(1, Ordering::Relaxed);
		//info!("FileDescription::new_shared_client: adding client {} to list", client.connection_id);
		clients.insert(ConnectionInternal::new_shared_client(client.clone()));
		self.conn_info.client_opened.lock().set(true);
		Ok(client)
	}
	///Returns true iff this is a shared server file description
	pub fn is_shared_server(&self) -> bool {
		self.conn_info.is_shared_server()
	}
	///Gets the connection ID of this file description
	pub fn get_connection_id(&self) -> i32 {
		self.conn_info.get_connection_id()
	}
	///Internal method to set whether to send a clunk when no clients are
	///left and possibly send a clunk message immediately if none are
	///present already
	fn clunk_on_no_clients_base(&self, enable: bool, force: bool) -> Result<(), VFSError> {
		self.conn_info.clunk_on_no_clients_base(self, enable, force)
	}
	///Sets whether this file description sends a clunk to all servers when
	///no clients are present. If no clients are present when called and
	///this file description has been opened at least once, one will be
	///sent immediately
	pub fn clunk_on_no_clients(&self, enable: bool) -> Result<(), VFSError> {
		self.clunk_on_no_clients_base(enable, true)
	}
	///Sets whether this file description sends a clunk to all clients when
	///no servers are present. If no servers are present when called and
	///this file description has been opened at least once, one will be
	///sent immediately
	pub fn clunk_on_no_servers(&self, enable: bool) -> Result<(), VFSError> {
		self.conn_info.clunk_on_no_servers(self, enable)
	}
	///This should be called on a newly-created file description that hasn't
	///been added to an FDSpace when something unrelated to the description
	///itself has happened that prevents it from being used; this will clean
	///up any references to it to allow it to be dropped.
	pub fn open_aborted(&self, _fd_type: FileDescriptorType) -> Result<(), VFSError> {
		match self.conn_info.fd_info {
			FDInfo::SharedClient{..} => {
				self.clunk_on_no_clients_base(true, true)
			},
			_ => { Ok(()) },
		}
	}
	///Gets the control file descriptor for this file description
	pub fn get_control_fd(&self) -> Option<UnifiedFileDescriptor> {
		self.cap_info.get_control_fd()
	}
	///Inner method to get the alternate RPC file description for a given
	///FD type
	fn get_rpc_io_desc_base(&self, fd_type: FileDescriptorType) -> &RwLock<Option<(Arc<FileDescription>, i32)>> {
		match fd_type {
			FileDescriptorType::Client => &self.alt_rpc_desc_client,
			FileDescriptorType::Server => &self.alt_rpc_desc_server,
		}
	}
	///Gets the file descriptor to use for I/O RPC requests for this file
	///description
	pub fn get_rpc_io_fd(&self, fd_type: FileDescriptorType) -> Result<(UnifiedFileDescriptor, i32), VFSError> {
		if let Some((desc, connection_id)) = self.get_rpc_io_desc_base(fd_type).read().as_ref() {
			let fd = desc.get_control_fd().ok_or(VFSError::InternalError)?;
			Ok((fd, *connection_id))
		}else{
			let fd = self.cap_info.get_rpc_io_fd()?;
			Ok((fd, self.get_connection_id()))
		}
	}
	///Sets an alternate file description to use for I/O RPC requests for
	///this file description
	pub fn set_rpc_io_desc(&self, fd_type: FileDescriptorType, info: Option<(Arc<FileDescription>, i32)>){
		let alt = self.get_rpc_io_desc_base(fd_type);
		if let Some(i) = info {
			alt.write().replace(i);
		}else{
			alt.write().take();
		}
	}
	///Returns true iff this file description has an alternate I/O file
	///description
	pub fn has_alt_rpc_io_desc(&self, fd_type: FileDescriptorType) -> bool {
		self.get_rpc_io_desc_base(fd_type).read().is_some()
	}
	///Sets the global ID of this file description (for use by the final
	///clunk hook)
	pub fn set_global_id(&self, id: u64){
		self.global_id.store(id, Ordering::Relaxed)
	}
	///Gets the global ID of this file description
	pub fn get_global_id(&self) -> u64 {
		self.global_id.load(Ordering::Relaxed)
	}
	///Sets the inode associated with this file description
	pub fn set_inode(&self, id: u64){
		self.inode.store(id, Ordering::Relaxed);
	}
	///Gets the global ID of this file description
	pub fn get_inode(&self) -> u64 {
		self.inode.load(Ordering::Relaxed)
	}
	///Associates this file descriptor with a Vnode
	pub fn set_vnode(self: &Arc<Self>, vnode: &Arc<Vnode>) -> Result<(), ServerStatus> {
		if self.get_global_id() == 0 {
			self.set_global_id(vnode.get_port_id());
		}
		self.set_inode(vnode.get_inode_id());
		//info!("set_vnode: {} {} {}", vnode.get_port_id(), vnode.get_inode_id(), self.get_connection_id());
		vnode.add_desc(self.clone())?;
		self.vnode.set(Arc::downgrade(vnode)).or(Err(SRV_ERR_IO))?;
		//info!("vnode added");
		Ok(())
	}
	//Gets the `Vnode` associated with this file description if one is
	//present
	pub fn get_vnode(&self) -> Option<Arc<Vnode>> {
		if let Some(vnode) = self.vnode.get() {
			vnode.upgrade()
		} else if let Some((desc, _)) = self.get_rpc_io_desc_base(FileDescriptorType::Client).read().as_ref() {
			desc.get_vnode()
		}else{
			None
		}
	}
	///Gets the flags used to open this file description
	pub fn get_open_flags(&self) -> u64 {
		self.open_flags
	}
	///Returns true iff this file description can be used as a base for
	///relative paths
	pub fn is_base_path(&self) -> bool {
		self.open_flags & O_DIRECTORY as u64 != 0 || self.open_flags & O_PATH as u64 != 0
	}
	///Removes a connection from this file description
	pub(super) fn remove(&self, fd_type: FileDescriptorType, id: u64) -> Result<(), VFSError> {
		self.conn_info.remove(self, fd_type, Some(id))
	}
	///Deallocate the slots used for allocating file descriptors from this
	///file description
	fn deallocate_caps(&self) -> Result<(), VFSError>{
		match self.conn_info.fd_info {
			FDInfo::SharedClient { .. } => { return Ok(()) },
			_ => {},
		};
		self.cap_info.deallocate_caps()
	}
	///Gets the number of server file descriptors pointing to this file
	///description
	fn get_num_servers(&self) -> usize {
		self.conn_info.get_num_servers()
	}
	///Unlinks this file description from the vnode and path lists
	fn list_unlink(&self) {
		let inode = self.get_inode();
		//info!("FileDescription::list_unlink: {}", inode);
		if inode != 0 && !self.vnode_unlinked.load(Ordering::Relaxed){
			if let Some(vnode_opt) = self.vnode.get() {
				if let Some(vnode) = vnode_opt.upgrade(){
					self.vnode_unlinked.store(true, Ordering::Relaxed);
					vnode.remove_desc(self.get_global_id(), self.get_connection_id());
				}else{
					//info!("FileDescription::clunk: vnode already deallocated for {:x} {:x}", self.get_global_id(), inode);
				}
			}else{
				//info!("FileDescription::clunk: vnode not present for {:x} {:x}", self.get_global_id(), inode);
			}
		}

		let mut path_opt = self.path_node.lock();
		if let Some(path_node) = path_opt.as_ref() {
			path_node.remove(self.get_connection_id());
		}
		path_opt.take();
	}
	///Associates this file description with a path list node
	fn set_path_node(&self, node: &Arc<FileDescriptionPathNode>) {
		self.path_node.lock().replace(node.clone());
	}
	///Sets the full paths associated with this file description
	pub fn set_paths(&self, orig_path: &UnixPath, real_path: &UnixPath){
		self.set_orig_path(orig_path);
		self.set_real_path(real_path);
	}
	///Sets the normalized original path (before any link resolution) under
	///which this file description was opened
	pub fn set_orig_path(&self, path: &UnixPath){
		let mut guard = self.orig_path.lock();
		guard.replace(UnixPathBuf::from(path));
	}
	///Sets the normalized real path (after all link resolution) under
	///which this file description was opened
	pub fn set_real_path(&self, path: &UnixPath){
		let mut guard = self.real_path.lock();
		guard.replace(UnixPathBuf::from(path));
	}
	///Gets the original path this file description was opened under
	pub fn get_orig_path(&self) -> MutexGuard<Option<UnixPathBuf>> {
		self.orig_path.lock()
	}
	///Gets the real path this file description was opened under (after
	///following symlinks and any renames that occur)
	pub fn get_real_path(&self) -> MutexGuard<Option<UnixPathBuf>> {
		self.orig_path.lock()
	}
}

impl Drop for FileDescription {
	fn drop(&mut self) {
		//info!("FileDescription::drop: {:p} {:?} {} {:?} {:?} {:?} {}", self, self.base_type, self.fd_info.get_type_string(), self.base_endpoint, self.base_reply, self.base_notification, self.get_inode());
		match self.conn_info.fd_info {
			FDInfo::SharedClient { .. } => { return },
			_ => {},
		}
		let _ = self.deallocate_caps();
		self.cap_info.deallocate_base_objects();
	}
}

intrusive_adapter!(FileDescriptionSharedAdapter = Arc<FileDescription>: FileDescription { shared_client_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FileDescriptionSharedAdapter {
	type Key = i32;
	fn get_key(&self, desc: &'a FileDescription) -> i32 {
		desc.get_connection_id()
	}
}

intrusive_adapter!(FileDescriptionVnodeAdapter = Arc<FileDescription>: FileDescription { vnode_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FileDescriptionVnodeAdapter {
	type Key = u64;
	fn get_key(&self, desc: &'a FileDescription) -> u64 {
		desc.get_global_id()
	}
}

intrusive_adapter!(FileDescriptionPathAdapter = Arc<FileDescription>: FileDescription { path_list_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FileDescriptionPathAdapter {
	type Key = i32;
	fn get_key(&self, desc: &'a FileDescription) -> i32 {
		desc.get_connection_id()
	}
}

///Holds client/server-specific FD state/parameters
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum FileDescriptorType {
	Client,
	Server,
}

impl FileDescriptorType {
	///Returns true if this file descriptor is a client FD
	pub fn is_client(&self) -> bool {
		match self {
			Self::Client => true,
			_ => false,
		}
	}
}

///Enum for contents of `ConnectionInternal`
enum ConnectionInternalContents {
	FileDescriptor(Arc<FDInternal>),
	SharedClient(SharedClientInternal),
}

///The internal state of a file descriptor
pub(super) struct ConnectionInternal {
	contents: ConnectionInternalContents,
	file_description_link: RBTreeAtomicLink,
}

impl ConnectionInternal {
	///Create a new `ConnectionInternal` for a file descriptor
	fn new_fd(fd: Arc<FDInternal>) -> Arc<Self> {
		let res = Arc::new(ConnectionInternal {
			contents: ConnectionInternalContents::FileDescriptor(fd),
			file_description_link: Default::default(),
		});
		//info!("ConnectionInternal::new_fd: {}", res.get_global_id());
		res
	}
	///Create a new `ConnectionInternal` for a shared client file
	///description
	fn new_shared_client(file_description: Arc<FileDescription>) -> Arc<Self> {
		let client = SharedClientInternal {
			file_description,
		};
		let res = Arc::new(ConnectionInternal {
			contents: ConnectionInternalContents::SharedClient(client),
			file_description_link: Default::default(),
		});
		//info!("ConnectionInternal::new_shared_client: {}", res.get_global_id());
		res
	}
	///Gets the local ID of this connection
	fn get_id(&self) -> i32 {
		match self.contents {
			ConnectionInternalContents::FileDescriptor(ref fd) => fd.get_id(),
			ConnectionInternalContents::SharedClient(ref client) => client.get_id(),
		}
	}
	///Gets the global ID of this connection
	fn get_global_id(&self) -> u64 {
		match self.contents {
			ConnectionInternalContents::FileDescriptor(ref fd) => fd.get_global_id(),
			ConnectionInternalContents::SharedClient(ref client) => client.get_global_id(),
		}
	}
	///Close this connection from a local context
	fn close_local(&self) -> Result<(), VFSError> {
		match self.contents {
			ConnectionInternalContents::FileDescriptor(ref fd) => fd.close_local(),
			ConnectionInternalContents::SharedClient(ref client) => client.close_local(),
		}
	}
	///Close this connection from a remote context
	fn close_remote(&self) -> Result<(), VFSError> {
		match self.contents {
			ConnectionInternalContents::FileDescriptor(ref fd) => fd.close_remote(),
			ConnectionInternalContents::SharedClient(ref client) => client.close_remote(),
		}
	}
}

/*impl Drop for ConnectionInternal {
	fn drop(&mut self) {
		info!("ConnectionInternal::drop: {}", self.get_global_id());
	}
}*/

intrusive_adapter!(FileDescriptionFDAdapter = Arc<ConnectionInternal>: ConnectionInternal { file_description_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FileDescriptionFDAdapter {
	type Key = u64;
	fn get_key(&self, conn: &'a ConnectionInternal) -> u64 {
		conn.get_global_id()
	}
}

///Internal state of a shared client file description
struct SharedClientInternal {
	file_description: Arc<FileDescription>,
}

impl SharedClientInternal {
	///Gets the local ID of this connection
	fn get_id(&self) -> i32 {
		self.file_description.get_connection_id()
	}
	///Gets the global ID of this connection
	fn get_global_id(&self) -> u64 {
		self.get_id() as u64
	}
	///Clunks this connection from a local context
	fn close_local(&self) -> Result<(), VFSError> {
		//info!("SharedClientInternal::close_local");
		self.file_description.conn_info.clunk_shared_client(&self.file_description)
	}
	///Clunks this connection from a remote context
	fn close_remote(&self) -> Result<(), VFSError> {
		//info!("SharedClientInternal::close_remote");
		//self.file_description.clunk_shared_client(&self)
		Ok(())
	}
}

///A list of file descriptions by path on the underlying server
pub struct FileDescriptionPathList {
	contents: RBTree<PathNodeAdapter>,
}

impl FileDescriptionPathList {
	///Creates a new `FileDescriptionPathList`
	fn new() -> Self {
		Self {
			contents: Default::default(),
		}
	}
	///Adds a new file description to the list
	pub fn add_desc(&mut self, desc: &Arc<FileDescription>, path: &UnixPath) {
		//info!("FileDescriptionPathList::add_desc: {}", path);
		if desc.path_list_link.is_linked() {
			//info!("already linked");
			return;
		}
		let port = desc.get_global_id();
		let cursor = self.contents.find(&(port, path));
		let node = if let Some(n) = cursor.clone_pointer() {
			n
		}else{
			let n = FileDescriptionPathNode::new(port, path);
			drop(cursor);
			self.contents.insert(n.clone());
			n
		};
		node.insert(desc);
		return;
	}
	///Gets a node from the list
	pub fn get_node(&self, port: u64, path: &UnixPathBuf) -> Option<Arc<FileDescriptionPathNode>> {
		let cursor = self.contents.find(&(port, path));
		cursor.clone_pointer()
	}
	///Removes a node from the list
	fn remove_node(&mut self, port: u64, path: &UnixPathBuf) {
		let mut cursor = self.contents.find_mut(&(port, path));
		cursor.remove();
	}
}

///A node in the FileDescriptionPathList. Holds references to all file
///descriptions associated with a particular path on a particular mount.
pub struct FileDescriptionPathNode {
	contents: RwLock<RBTree<FileDescriptionPathAdapter>>,
	port: u64,
	path: UnixPathBuf,
	link: RBTreeAtomicLink,
}

impl FileDescriptionPathNode {
	///Creates a new `FileDescriptionPathNode`
	fn new(port: u64, path: &UnixPath) -> Arc<Self> {
		let path_buf = UnixPathBuf::from(path);
		Arc::new(Self {
			contents: Default::default(),
			port,
			path: path_buf,
			link: Default::default(),
		})
	}
	///Adds a file description to this node
	fn insert(self: &Arc<Self>, desc: &Arc<FileDescription>) {
		//info!("FileDescriptionPathNode::insert: {} {}", self.port, desc.get_connection_id());
		let mut contents = self.contents.write();
		desc.set_path_node(&self);
		contents.insert(desc.clone());
	}
	///Removes a file description from this node
	fn remove(&self, conn: i32) {
		let mut contents = self.contents.write();
		let mut cursor = contents.find_mut(&conn);
		if !cursor.is_null() {
			cursor.remove();
		}
		let is_null = contents.front().is_null();
		drop(contents);
		if is_null {
			let mut path_list = get_path_list().write();
			path_list.remove_node(self.port, &self.path);
		}
	}
}

intrusive_adapter!(PathNodeAdapter = Arc<FileDescriptionPathNode>: FileDescriptionPathNode { link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for PathNodeAdapter {
	type Key = (u64, &'a UnixPath);
	fn get_key(&self, node: &'a FileDescriptionPathNode) -> (u64, &'a UnixPath) {
		(node.port, &node.path)
	}
}

static DESC_PATH_LIST: OnceCell<RwLock<FileDescriptionPathList>> = OnceCell::new();

///Gets the path list
pub fn get_path_list() -> &'static RwLock<FileDescriptionPathList> {
	DESC_PATH_LIST.get_or_init(|| {
		RwLock::new(FileDescriptionPathList::new())
	})
}

///Adds FDSpace-related custom slabs
pub fn add_custom_slabs() {
	add_arc_slab!(FileDescription, 512, 4, 4, 2, "file description");
	add_arc_slab!(FileDescriptionPathNode, 512, 4, 4, 2, "file description path list node");
	add_arc_slab!(ConnectionInternal, 512, 4, 4, 2, "connection internal state");
}
