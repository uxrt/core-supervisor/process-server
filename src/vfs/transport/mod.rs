/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///FDSpace support and glue code for the transport layer

use super::vfs_cspace_result;

mod description;
mod fdspace;

pub use description::{
	FileDescription,
	FileDescriptionType,
	FileDescriptorType,
	get_path_list,
};

pub use fdspace::{
	ContextFDSpace,
	FDSpace,
	FDSpaceContainer,
	fdspace_root_thread_init,
	get_fd,
	get_initial_reserved_fdspace,
	get_root_fdspace,
	get_root_fd_arrays,
	init_fdspace_list,
	set_root_fd_arrays,
};

///Allocates a file descriptor ID
macro_rules! allocate_id {
	($fd_tree: expr, $search_start: expr) => {
		{
			let mut c = $fd_tree.upper_bound_mut(Bound::Included(&$search_start));
			//info!("search_start: {} null: {}", self.search_start, c.is_null());
			let mut wrapped = false;
			let mut id = 0;
			while let Some(current) = c.get(){
				let next = c.peek_next();
				let current_id = current.get_id();
				if current_id > i32::MAX {
					wrapped = true;
				}
				id = current_id + 1;
				if next.is_null() || next.get().unwrap().get_id() - id > 0 {
					break;
				}
				c.move_next();
			}
			if c.is_null() {
				c = $fd_tree.back_mut();
				if let Some(fd) = c.get(){
					let next_id = fd.get_id();
					wrapped = next_id > i32::MAX;
					id = next_id + 1;
				}
			}
			if wrapped {
				None
			}else{
				Some(id)
			}
		}
	};
}

pub(crate) use allocate_id;

///Adds FDSpace-related custom slabs
pub fn add_custom_slabs() {
	description::add_custom_slabs();
	fdspace::add_custom_slabs();
}
