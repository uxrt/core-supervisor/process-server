/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */
///This is the directory read handler

use core::mem::size_of;

use alloc::vec::Vec;
use alloc::sync::Arc;

use intrusive_collections::{
	KeyAdapter,
	RBTree,
	RBTreeAtomicLink,
};

use typed_path::UnixPathBuf;

use zerocopy::AsBytes;

use super::fsspace::OpenResult;
use super::rpc::ConnectResult;
use super::vnode::get_vnode_list;


use super::transport::{
	FileDescription,
	FileDescriptorType,
	get_root_fdspace,
};

use usync::{
	Mutex,
	RwLock,
};
use usync_once_cell::OnceCell;

use dir_map::DirReader;

use uxrt_transport_layer::{
	AccessMode,
	FileDescriptor,
	FileDescriptorRef,
	IOError,
	MsgHeader,
	MsgType,
	Offset,
	OffsetType,
	SRV_ERR_BADF,
	SRV_ERR_IO,
	SRV_ERR_NOENT,
	ServerStatus,
};

use uxrt_vfs_rpc::{
	CWD_FD,
	AT_FDCWD,
	AT_FDINVALID,
	O_DIRECTORY,
	O_RDONLY,
	O_TRANSIENT,
	O_USEDEST,
	dirent,
	open_how,
};

use uxrt_vfs_rpc_client::get_default_rpc_client;

use uxrt_vfs_resmgr::base::RESMGR_NO_REPLY;
use uxrt_vfs_resmgr::base::{
	DispatchContext,
	DispatchContextFactory,
	handle_dir_message,
};

use uxrt_vfs_rpc::server::ServerMessageHandler;

use crate::add_arc_slab;

use crate::global_heap_alloc::get_kobj_alloc;

use crate::task::thread_pool::{
	RootThreadPool,
	new_thread_pool,
};

use crate::vfs::{
	new_md,
	new_vfs_server_channel,
};

use super::rpc::FSContext;

use super::VFSError;

///OCB for directory file descriptors
pub struct DirOCB {
	desc: Arc<FileDescription>,
	reader: Mutex<DirReader>,
	path: Mutex<Vec<u8>>,
	dirfd: i32,
	how: open_how,
	context: Arc<FSContext>,
	link: RBTreeAtomicLink,
}

impl DirOCB {
	///Creates a new `DirOCB`
	fn new(desc: Arc<FileDescription>, context: &Arc<FSContext>, path: &[u8], dirfd: i32, mut how: open_how) -> Arc<Self> {
		let mut path_vec = Vec::new();
		path_vec.extend_from_slice(path);
		//info!("DirOCB::new: {:?}", core::str::from_utf8(path));
		how.flags |= O_DIRECTORY as u64;
		Arc::new(Self {
			desc,
			reader: Mutex::new(DirReader::new()),
			path: Mutex::new(path_vec),
			dirfd,
			how,
			context: context.clone(),
			link: Default::default(),
		})
	}
	///Opens a directory, acquiring the lock for the reader
	pub fn open(&self, transport_header: &MsgHeader, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<(UnixPathBuf, UnixPathBuf, UnixPathBuf, u64, u64), ServerStatus> {
		self.open_locked(&mut self.reader.lock(), transport_header, secondary_bufs, secondary_len)
	}
	///Opens a directory, using an existing lock guard for the reader
	fn open_locked(&self, reader: &mut DirReader, transport_header: &MsgHeader, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<(UnixPathBuf, UnixPathBuf, UnixPathBuf, u64, u64), ServerStatus> {
		self.clear(reader);
		let mut inode = 0;
		let mut port = 0;
		let path = self.path.lock();
		//info!("DirOCB::open_locked: {:?}", core::str::from_utf8(path.as_bytes()));
		let fdspace = self.context.get_fdspace();
		let mut connect_res = self.context.connect_path(self.dirfd, &path, self.how.flags, self.how.resolve, 0, &mut |suffix, binding, open_flags, resolve_flags| {
			//info!("DirOCB::open_locked: opening {:?}", core::str::from_utf8(path.as_bytes()).unwrap());
			if open_flags != self.how.flags || resolve_flags != self.how.resolve {
				warn!("DirOCB: unexpected flags {} {} received from connect_path", open_flags, resolve_flags);
			}

			let (sb, sl) = if inode == 0 {
				(&mut secondary_bufs[..], &mut secondary_len[..])
			}else{
				(&mut [][..], &mut [][..])
			};

			match binding.handle_openat2(transport_header, &self.context, suffix, &self.how, sb, sl)? {
				OpenResult::Restart(res, new_path, context) => Ok(ConnectResult::Restart(res, new_path, context)),
				OpenResult::Finish(mount_desc_opt, fd_type, badge, cur_port, cur_inode, secondary_ret) => {
					//info!("DirOCB::open_locked: got result: {:?} {} {}", fd_type, cur_port, cur_inode);
					if cur_inode == 0 {
						return Err(SRV_ERR_IO);
					}
					if fd_type != FileDescriptorType::Client {
						return Err(SRV_ERR_IO);
					}
					if self.how.flags & O_TRANSIENT as u64 != 0 {
						port = cur_port;
						inode = cur_inode;
						return Ok(ConnectResult::Finish);
					}
					let mount_desc = if let Some(d) = mount_desc_opt {
						d
					}else{
						return Err(SRV_ERR_IO);
					};
					let fd = fdspace.write().insert(&mount_desc, fd_type, AccessMode::ReadWrite, badge, None)?;
					//info!("DirOCB::open_locked: FD added");
					reader.add_fd(fd);
					if inode == 0 {
						//info!("DirOCB::open_locked: setting primary port/inode");
						self.desc.set_rpc_io_desc(FileDescriptorType::Client, Some((mount_desc.clone(), mount_desc.get_connection_id())));
						port = cur_port;
						inode = cur_inode;
					}
					let mut vnode_list = get_vnode_list().write();
					vnode_list.add_desc(&mount_desc, cur_port, cur_inode)?;
					Ok(ConnectResult::Continue)
				},
				OpenResult::Abort => {
					Ok(ConnectResult::Finish)
				},
			}
		});
		if inode == 0 && connect_res.is_ok() {
			connect_res = Err(SRV_ERR_NOENT);
		}
		//info!("DirOCB::open_locked: done: {}", reader.get_fds().len());
		match connect_res {
			Ok((orig_path, real_path, mount_path)) => {
				Ok((orig_path, real_path, mount_path, port, inode))
			},
			Err(err) => {
				//info!("DirOCB::open_locked: got error: {:?}", err);
				self.clear(reader);
				Err(err)
			}
		}
	}
	pub fn get_file_description(&self) -> Arc<FileDescription> {
		self.desc.clone()
	}
	///Clears the state of this OCB; the underlying directories will be
	///fully re-read on the next read
	fn clear(&self, reader: &mut DirReader) {
		let fds = reader.get_fds();
		let fdspace = self.context.get_fdspace();
		let mut f = fdspace.write();
		for i in 0..fds.len(){
			let _ = f.remove(fds[i]);
		}
		drop(f);
		reader.clear_fds();
	}
}

impl Drop for DirOCB {
	fn drop(&mut self) {
		let f = get_root_fdspace();
		let mut fdspace = f.write();
		let reader = self.reader.lock();
		let fds = reader.get_fds();
		for i in 0..fds.len(){
			if let Err(err) = fdspace.remove(fds[i]) {
				warn!("could not close directory client FD {} for directory reader {:p}: {:?}", self.dirfd, self, err);
			}
		}
		if self.dirfd != AT_FDINVALID && self.dirfd != AT_FDCWD {
			if let Err(err) = fdspace.remove(self.dirfd) {
				warn!("could not close dirfd {} for directory reader {:p}: {:?}", self.dirfd, self, err);
			}
		}
	}
}

intrusive_adapter!(DirOCBAdapter = Arc<DirOCB>: DirOCB { link: RBTreeAtomicLink } );

impl<'a> KeyAdapter<'a> for DirOCBAdapter {
	type Key = i32;
	fn get_key(&self, node: &'a DirOCB) -> i32 {
		node.desc.get_connection_id()
	}
}

///Dispatch context for the directory handler
struct DirDispatchContext {
	factory: Arc<DirHandler>,
	message_fd: Option<FileDescriptorRef>,
	transport_header: MsgHeader,
}

impl DirDispatchContext {
	///Internal implementation of handle_message()
	fn handle_message_inner(&mut self, msg_fd: FileDescriptorRef, ocb: &DirOCB) -> Result<Offset, IOError> {
		//info!("DirDispatchContext::handle_message_inner");
		let mut reader = ocb.reader.lock();
		let header = self.get_transport_header();
		handle_dir_message(header, msg_fd, |data_offset, _request| {
			if data_offset < 0 {
				//TODO: track renames of the original top-level directory
				if let Err(err) = ocb.open_locked(&mut reader, &header, &mut [], &mut []) {
					return Err(IOError::ServerError(err))
				}else{
					return Ok(0);
				}
			}

			if let Some(entry) = reader.preaddir(header.offset)? {
				let total = usize::min(entry.d_reclen as usize, size_of::<dirent>()) as usize;
				msg_fd.wpwrite(&entry.as_bytes()[..total], data_offset, OffsetType::Start)?;
				Ok(1)
			}else{
				Ok(0)
			}
		})
	}
}

impl ServerMessageHandler<()> for DirDispatchContext {
}

impl DispatchContext for DirDispatchContext {
	fn get_transport_header(&self) -> &MsgHeader {
		&self.transport_header
	}
	fn get_transport_header_mut(&mut self) -> &mut MsgHeader {
		&mut self.transport_header
	}
	fn get_channel_fd(&self) -> FileDescriptorRef {
		self.factory.channel_fd
	}
	///Gets the current message FD of this context.
	fn get_message_fd(&self) -> Option<FileDescriptorRef> {
		self.message_fd
	}
	fn new_message_fd(&mut self) -> Result<FileDescriptorRef, IOError> {
		let md = new_md()?;
		self.message_fd = Some(md);
		Ok(md)
	}
	fn handle_message(&mut self) -> Result<(), IOError> {
		let header = self.get_transport_header();
		let msg_fd = self.get_message_fd().expect("CreatePortContext: message FD unset (this should never happen)");
		let status = if header.msgtype_enum() == Some(MsgType::Clunk) {
			self.factory.delete_client(header.client_id);
			0
		}else{
			let ocbs = self.factory.ocbs.read();
			let cursor = ocbs.find(&header.client_id);
			let opt = cursor.clone_pointer();
			drop(ocbs);

			let res = if let Some(ocb) = opt {
				self.handle_message_inner(msg_fd, &ocb)
			}else{
				Err(IOError::ServerError(SRV_ERR_IO))
			};
			match res {
				Ok(size) => size,
				Err(err) => -err.to_errno(),
			}
		};
		if status != RESMGR_NO_REPLY as ServerStatus {
			let _ = msg_fd.mwpwrite(&[], 0, OffsetType::End, status);
		}
		Ok(())
	}
}

impl Drop for DirDispatchContext {
	fn drop(&mut self) {
		self.drop_common();
	}
}

///Handles directory reads
pub struct DirHandler {
	channel_fd: FileDescriptorRef,
	channel_desc: Arc<FileDescription>,
	ocbs: RwLock<RBTree<DirOCBAdapter>>,
}

impl DirHandler {
	///Constructs a new `DirHandler`
	fn new() -> Result<Arc<Self>, VFSError> {
		let (channel_desc, channel_fd) = new_vfs_server_channel()?;
		Ok(Arc::new(Self {
			channel_fd,
			channel_desc,
			ocbs: Default::default(),
		}))
	}
	///Gets the file description for this message handler
	pub fn get_file_description(&self) -> &Arc<FileDescription> {
		&self.channel_desc
	}
	///Creates a new directory client connection
	pub fn new_client(&self, context: &Arc<FSContext>, path: &[u8], mut dirfd: i32, how: &open_how) -> Result<Arc<DirOCB>, VFSError> {
		let f = context.get_fdspace();
		let mut context_fdspace = f.write();
		dirfd = if f.get_id() == get_root_fdspace().get_id() {
			match context_fdspace.dup(dirfd, None, None, None) {
				Ok(f) => f,
				Err(err) => {
					if err.to_errno() == SRV_ERR_BADF {
						AT_FDINVALID
					}else{
						return Err(err);
					}
				},
			}
		}else{
			panic!("TODO: implement directory creation for non-root FDSpaces");
		};
		drop(context_fdspace);
		let mut how_clone = how.clone();
		how_clone.flags |= O_DIRECTORY as u64;
		let desc = self.channel_desc.new_shared_client(how_clone.flags, None, None)?;
		let ocb = DirOCB::new(desc.clone(), context, path, dirfd, how_clone);
		self.ocbs.write().insert(ocb.clone());
		Ok(ocb)
	}
	///Deletes a directory client connection
	pub fn delete_client(&self, id: i32) {
		let mut ocbs = self.ocbs.write();
		let mut cursor = ocbs.find_mut(&id);
		let ocb = cursor.remove();
		drop(ocbs);
		drop(ocb);
	}
}

impl DispatchContextFactory<DirDispatchContext> for Arc<DirHandler> {
	///Creates a new `ResMgrContext` attached to this port.
	fn new_context(&self) -> Result<DirDispatchContext, IOError> {
		Ok(DirDispatchContext {
			factory: self.clone(),
			message_fd: None,
			transport_header: MsgHeader::new(),
		})
	}
}

static DIR_THREAD_POOL: OnceCell<Arc<RootThreadPool<Arc<DirHandler>, DirDispatchContext>>> = OnceCell::new();

///Initializes the directory handler, starting the server
pub fn init_dir_handler(){
	//info!("initializing directory handler");
	let orig_handler = DirHandler::new().expect("cannot create VFS RPC handler");
	let thread_pool = DIR_THREAD_POOL.get_or_init(|| {
		new_thread_pool::<Arc<DirHandler>, DirDispatchContext>(orig_handler, 2, 10, i32::MAX as usize, 2, "dir_read_handler").expect("cannot create PortFS thread pool")
	});
	thread_pool.start().expect("failed to start directory handler");
}

pub fn init_cwd(){
	let how = open_how {
		flags: (O_RDONLY | O_DIRECTORY | O_USEDEST) as u64,
		mode: (CWD_FD as u64) << 32,
		resolve: 0,
	};
	get_default_rpc_client().openat2(AT_FDINVALID, "/dev".as_bytes(), &how).expect("could not open root directory as current directory");
}

///Gets the running directory handler
pub fn get_dir_handler() -> Arc<DirHandler> {
	let pool = DIR_THREAD_POOL.get().expect("directory handler unset");
	pool.get_factory().clone()
}

///Adds directory-related custom slabs
pub fn add_custom_slabs() {
	add_arc_slab!(DirOCB, 512, 4, 4, 2, "directory OCB");
	let kobj_alloc = get_kobj_alloc();
	dir_map::add_custom_slabs(&kobj_alloc);
}
