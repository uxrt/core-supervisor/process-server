/*
 * Copyright (c) 2018-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 */

///This module contains all of the filesystems that are built into the process
///server.

pub mod dmesg;
pub mod null;
pub mod portfs;

///Starts drivers that are implemented as standard resource managers
pub fn start_resmgr_drivers() {
	dmesg::start_dmesg();
	null::start_null();
}

pub fn add_custom_slabs() {
	portfs::add_custom_slabs();
}
