/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///Full/null/zero devices

use alloc::sync::Arc;

use typed_path::UnixPath;

use sel4::{
	PAGE_SIZE,
	SendLongMsgBuffer,
};

use crate::add_tree_ocb_slabs;

use crate::task::thread_pool::new_resmgr_thread_pool;

use crate::vfs::new_resmgr_port;

use uxrt_vfs_resmgr::{
	CommonOCB,
	DirectoryIOHandler,
	DispatchContext,
	OpenControlBlock,
	PrimitiveIOHandler,
	ResMgrContextExtra,
	RPCConnectHandler,
	RPCIOHandler,
	tree_rpc_connect_handler,
	tree_rpc_io_handler,
	tree_primitive_io_handler,
};

use uxrt_vfs_resmgr::inner::{
	TreeContextExtra,
	TreeInnerMsgHandler,
	TreeMessageHandler,
	TreeNodeNonLinkExtra,
	TreeOCB,
	TreeResMgrContext,
};

use uxrt_vfs_rpc::{
	O_TRANSIENT,
	S_IFDIR,
	S_IFCHR,
	VFSServerOpenResult,
	gid_t,
	mode_t,
	open_how_server,
	stat,
	uid_t,
};

use uxrt_transport_layer::{
	FileDescriptor,
	IOError,
	Offset,
	OffsetType,
	SRV_ERR_IO,
	SRV_ERR_NOSPC,
	ServerStatus,
};

const ZERO_BUF_COPIES: usize = 512;
const ZERO_BUF_SIZE: usize = PAGE_SIZE * 4;
const ZERO_BUF_TOTAL: usize = ZERO_BUF_COPIES * ZERO_BUF_SIZE;
static ZERO_BUF: [u8; ZERO_BUF_SIZE] = [0; ZERO_BUF_SIZE];
static ZERO_BUF_INFO: [SendLongMsgBuffer; ZERO_BUF_COPIES] = [SendLongMsgBuffer::new(&ZERO_BUF); ZERO_BUF_COPIES];

///OCB for null device handler
struct NullOCB {
	common_ocb: CommonOCB,
}

impl NullOCB {
	///Creates a new `NullOCB`.
	pub fn new() -> Self {
		Self {
			common_ocb: CommonOCB::new(),
		}
	}
}

impl OpenControlBlock for NullOCB {
	fn get_common<'a>(&'a self) -> &'a CommonOCB {
		&self.common_ocb
	}
	fn get_common_mut<'a>(&'a mut self) -> &'a mut CommonOCB {
				&mut self.common_ocb
	}
}

///Context state for null device handler
struct NullContextExtra {
}

impl ResMgrContextExtra for NullContextExtra {
	fn new() -> Self {
		Self {
		}
	}
}

///Type enum for devices implemented by the null driver
#[derive(Debug)]
enum NullDeviceType {
	Dir,
	Full,
	Null,
	Zero,
}

impl TreeNodeNonLinkExtra for NullDeviceType {
}

///Central handler for log messages
pub struct NullResMgr {
}

impl NullResMgr {
	///Creates a new `NullResMgr`.
	fn new() -> Arc<TreeMessageHandler<NullOCB, Self, NullContextExtra, NullDeviceType>> {
		let handler = TreeMessageHandler::new(Self {}, NullDeviceType::Dir).expect("cannot create null device message handler");
		handler.add_node(UnixPath::new("dev"), (S_IFDIR | 0o755) as mode_t, 0, 0, NullDeviceType::Dir).expect("cannot create /dev entry for device message handler");
		handler.add_node(UnixPath::new("dev/full"), (S_IFCHR | 0o666) as mode_t, 0, 0, NullDeviceType::Full).expect("cannot create /dev/full entry for device message handler");
		handler.add_node(UnixPath::new("dev/null"), (S_IFCHR | 0o666) as mode_t, 0, 0, NullDeviceType::Null).expect("cannot create /dev/null entry for device message handler");
		handler.add_node(UnixPath::new("dev/zero"), (S_IFCHR | 0o666) as mode_t, 0, 0, NullDeviceType::Zero).expect("cannot create /dev/zero entry for device message handler");
		handler
	}
	///Writes zeroes to the client buffer.
	fn write_zeroes<H: NullInnerMsgHandler>(&self, context: &NullResMgrContext<H>, size: usize) -> Result<(usize, Offset), IOError> {
		let mut remaining = size;
		let message_fd = context.get_message_fd().ok_or(IOError::ServerError(SRV_ERR_IO))?;
		while remaining != 0 {
			let dest_offset = size - remaining;
			if remaining < ZERO_BUF_TOTAL {
				remaining = 0;
			}else{
				remaining -= ZERO_BUF_TOTAL;
			}

			message_fd.wpwritev(&ZERO_BUF_INFO, dest_offset as Offset, OffsetType::Start)?;
		}
		Ok((size, 0))
	}
}

trait NullInnerMsgHandler = TreeInnerMsgHandler<NullOCB, NullContextExtra, NullDeviceType>;
type NullResMgrContext<H> = TreeResMgrContext<NullOCB, H, NullContextExtra, NullDeviceType>;
type NullOuterOCB = TreeOCB<NullOCB, NullDeviceType>;

tree_rpc_connect_handler!(NullResMgr, NullOCB, NullContextExtra, NullDeviceType, {
	fn handle_openat2<H: NullInnerMsgHandler>(&self, context: &NullResMgrContext<H>, _path: &mut [u8], _path_size: usize, _how: &open_how_server, _res_offset: usize) -> Result<(VFSServerOpenResult, usize), ServerStatus>{
		let mut extra = context.get_extra().borrow_mut();
		let ocb = extra.new_ocb(NullOCB::new()).or(Err(SRV_ERR_IO))?;
		context.bind_ocb(ocb).or_else(|_| { Err(SRV_ERR_IO) })?;
		//info!("NullResMgr::handle_openat2()");
		Ok(VFSServerOpenResult::inode(0))
	}
});

tree_rpc_io_handler!(NullResMgr, NullOCB, NullContextExtra, NullDeviceType, {
	fn handle_fstat<H: NullInnerMsgHandler>(&self, _context: &NullResMgrContext<H>, _ocb: &mut NullOuterOCB, _statbuf: &mut stat) -> Result<(), ServerStatus> {
		Ok(())
	}
	fn handle_fchmod<H: NullInnerMsgHandler>(&self, _context: &NullResMgrContext<H>, _ocb: &mut NullOuterOCB, _mode: mode_t) -> Result<(), ServerStatus> {
		Ok(())
	}
	fn handle_fchown<H: NullInnerMsgHandler>(&self, _context: &NullResMgrContext<H>, _ocb: &mut NullOuterOCB, _uid: uid_t, _gid: gid_t) -> Result<(), ServerStatus> {
		Ok(())
	}
});

tree_primitive_io_handler!(NullResMgr, NullOCB, NullContextExtra, NullDeviceType, {
	fn handle_wpreadv_outer<H: NullInnerMsgHandler>(&self, context: &NullResMgrContext<H>, ocb: &mut NullOuterOCB, size: usize, _offset: i64, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		//info!("NullResMgr::handle_wpreadv_outer");
		let dev_type = ocb.get_node().get_extra().non_link().ok_or(IOError::ServerError(SRV_ERR_IO))?;
		match dev_type {
			NullDeviceType::Dir => Err(IOError::ServerError(SRV_ERR_IO)),
			NullDeviceType::Full | NullDeviceType::Zero => {
				self.write_zeroes(context, size)
			},
			NullDeviceType::Null => Ok((0, 0)),
		}
	}
	fn handle_wpwritev_outer<H: NullInnerMsgHandler>(&self, _context: &NullResMgrContext<H>, ocb: &mut NullOuterOCB, size: usize, _offset: i64, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		let dev_type = ocb.get_node().get_extra().non_link().ok_or(IOError::ServerError(SRV_ERR_IO))?;
		//info!("NullResMgr::handle_wpwritev_outer: {:?}", dev_type);
		match dev_type {
			NullDeviceType::Dir => Err(IOError::ServerError(SRV_ERR_IO)),
			NullDeviceType::Full => Err(IOError::ServerError(SRV_ERR_NOSPC)),
			NullDeviceType::Null | NullDeviceType::Zero => Ok((size, 0)),
		}
	}
	fn handle_writereadv_outer<H: NullInnerMsgHandler>(&self, context: &NullResMgrContext<H>, ocb: &mut NullOuterOCB, w_size: usize, r_size: usize, _offset: i64, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		//info!("NullResMgr::handle_writereadv_outer");
		let dev_type = ocb.get_node().get_extra().non_link().ok_or(IOError::ServerError(SRV_ERR_IO))?;
		match dev_type {
			NullDeviceType::Dir => Err(IOError::ServerError(SRV_ERR_IO)),
			NullDeviceType::Full => Err(IOError::ServerError(SRV_ERR_NOSPC)),
			NullDeviceType::Null => Ok((w_size, 0)),
			NullDeviceType::Zero => {
				self.write_zeroes(context, r_size)?;
				Ok((w_size, 0))
			},
		}
	}
	fn handle_clunk<H: NullInnerMsgHandler>(&self, _context: &NullResMgrContext<H>, _ocb: &mut NullOuterOCB) {
	}
});

pub fn add_null_slabs(){
	add_tree_ocb_slabs!(NullOCB, NullDeviceType, 512, 4, 4, 2, "null device OCB");
}

///Starts the null resource manager.
pub fn start_null(){
	info!("starting null resource manager");
	add_null_slabs();
	let port = new_resmgr_port(NullResMgr::new(), 0, "/", &[&UnixPath::new("dev")]).expect("could not create null resource manager port");
	let pool = new_resmgr_thread_pool(&port, 3, 5, 10, 3, "dev_null").expect("could not create null resource manager thread pool");
	pool.start().expect("could not start null resource manager");
}
