/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 */

///This is the port filesystem driver. It provides basically anonymous file
///descriptions that are more or less the client-server equivalent of pipes,
///allowing access to server-side file descriptors without having to
///implement a full resource manager, and also provides channels that allow
///resource managers to serve filesystems.

use core::fmt;
use core::sync::atomic::{
	AtomicBool,
	Ordering,
};

use core::convert::TryFrom;
use core::cell::RefCell;
use core::mem::size_of;
use core::str;
use alloc::sync::{
	Arc,
	Weak,
};
use alloc::vec::Vec;

use num_traits::FromPrimitive;
use bilge::prelude::*;

use zerocopy::AsBytes;

use usync::RwLock;

use usync_once_cell::OnceCell;

use uxrt_procfs_client::{
	PORT_OPEN_AFTER_CREATE,
	PORT_UNLINK_ON_NO_SERVERS,
	PORT_CHANNEL_GET_RECV_TOTAL_SIZE,
	PORT_CHANNEL_GET_REPLY_SIZE,
	PORT_MOUNT_MULTIPLE,
	PORT_MOUNT_PRIVATE_CHANNEL,
	PORT_DIRECT_CHANNEL_VALID_FLAGS,
	PORT_MOUNT_CHANNEL_VALID_FLAGS,
	PORT_NOTIFICATION_VALID_FLAGS,
	PORT_MESSAGE_VALID_FLAGS,
	create_port_request,
	create_port_response,
};
use uxrt_procfs_client::port::PortType;

use uxrt_thread_pool::ResMgrThreadPool;

use uxrt_transport_layer::{
	AccessMode,
	FileDescriptor,
	FileDescriptorRef,
	IOError,
	MsgHeader,
	Offset,
	OffsetType,
	SRV_ERR_INVAL,
	SRV_ERR_IO,
	SRV_ERR_ISDIR,
	SRV_ERR_NOENT,
	SRV_ERR_NOTDIR,
	SRV_ERR_PROTO,
	ServerStatus,
	ServerChannelOptions,
	UnifiedFileDescriptor,
};

use uxrt_vfs_rpc_client::VFSRPCClient;

use uxrt_vfs_rpc::{
	AT_FDINVALID,
	O_DIRECTORY,
	O_TRANSIENT,
	PATH_MAX,
	S_IFCHR,
	S_IFDIR,
	S_IFMT,
	S_IFMSG,
	VFS_RPC_SERVER_OPEN_INODE,
	VFS_RPC_SERVER_OPEN_SYMLINK,
	VFS_RPC_SERVER_OPEN_FIRMLINK,
	VFSCommonHeader,
	VFSServerOpenResult,
	dev_t,
	gid_t,
	ino_t,
	mode_t,
	off_t,
	open_how,
	open_how_server,
	rpc_end_bufs,
	rpc_end_open_server,
	rpc_init_open_server,
	rpc_new_bufs_open_server,
	stat,
	uid_t,
};

use uxrt_vfs_rpc::client::ClientMessage;
use uxrt_vfs_rpc::server::ServerMessageHandler;

use uxrt_vfs_resmgr::{
	DispatchContext,
	DispatchContextFactory,
	DirectoryIOHandler,
	CommonOCB,
	OpenControlBlock,
	PrimitiveIOHandler,
	RPCConnectHandler,
	RPCIOHandler,
	ResMgrContext,
	ResMgrContextExtra,
	ResMgrMsgHandler,
	ResMgrPort,
};

use crate::task::thread_pool::{
	RootPoolThread,
	RootThreadPool,
	new_resmgr_thread_pool,
};

use crate::vfs::{
	BasicMessageFDAllocator,
	new_md,
	new_vfs_server_channel,
};

use crate::{
	add_arc_slab,
	add_arc_list_slab,
};

use crate::vfs::fsspace::{
	Mount,
	OpenResult,
};
use crate::vfs::rpc::{
	ConnectResult,
	FSContext,
	get_root_default_fs_context,
	get_fs_context,
};
use crate::vfs::transport::{
	FileDescription,
	FileDescriptionType,
	FileDescriptorType,
	get_root_fdspace,
};

use crate::utils::{
	LockedArcList,
	ArcListItem,
};

use crate::vfs::VFSError;

use typed_path::{
	UnixPath,
	UnixPathBuf,
};

use zerocopy::FromBytes;

///Holds state specific to each port type
#[repr(C)]
pub enum InternalPortType {
	DirectChannel(ServerChannelOptions),
	MountChannel(ServerChannelOptions, bool, bool),
	Notification,
	MessageDescriptor,
	SecondaryMountChannel(Arc<Port>),
	VFSClientChannel,
}

impl InternalPortType {
	///Returns true if this port has a client side
	fn has_client(&self) -> bool {
		match self {
			Self::DirectChannel(..) | Self::Notification | Self::VFSClientChannel => true,
			_ => false,
		}
	}
	///Returns true if this port has a server side
	fn has_server(&self) -> bool {
		match self {
			Self::VFSClientChannel => false,
			_ => true,
		}
	}
}

impl fmt::Debug for InternalPortType {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			InternalPortType::DirectChannel(..) => write!(f, "direct channel"),
			InternalPortType::MountChannel(..) => write!(f, "mount channel"),

			InternalPortType::Notification => write!(f, "notification"),
			InternalPortType::MessageDescriptor => write!(f, "message descriptor"),
			InternalPortType::SecondaryMountChannel(orig_port) => write!(f, "secondary mount channel, original: {:?}", orig_port),
			InternalPortType::VFSClientChannel => write!(f, "VFS client channel"),
		}
	}
}

//Bitfield struct for global port IDs
#[bitsize(64)]
#[derive(FromBits)]
pub struct GlobalPortID {
	port_id: u32,
	context_id: u32,
}

impl GlobalPortID {
	///Returns an ID that is guaranteed to be invalid
	pub fn invalid() -> Self {
		Self::new(i32::MIN as u32, i32::MAX as u32)
	}
	///Gets the ID as an integer
	pub fn value(&self) -> dev_t {
		self.value
	}
}

//Bitfield struct for PortFS inode IDs
#[bitsize(64)]
#[derive(FromBits)]
pub struct PortFSInode {
	context_id: u32,
	port_id: u32,
}

impl PortFSInode {
	///Gets the ID as an integer
	pub fn value(&self) -> ino_t {
		self.value
	}
}

///Hook function to remove an unlinked port from the list once the last file
///descriptor to it has been closed
fn global_clunk(id: u64){
	let unpacked = GlobalPortID::from(id);
	//info!("global_clunk: {} {}", unpacked.context_id(), unpacked.port_id());
	if let Some(context) = get_fs_context(unpacked.context_id() as i32) {
		if context.get_ports().ports.remove(unpacked.port_id() as i32).is_err(){
			warn!("global port clunk received for invalid port: {}, {}", unpacked.context_id(), unpacked.port_id());
		}
	}else{
		warn!("global port clunk received for invalid context {} (port: {})", unpacked.context_id(), unpacked.port_id());
	}
}

///Mutable stat fields for ports
struct PortStatInfo {
	mode: mode_t,
	uid: uid_t,
	gid: gid_t,
}

impl PortStatInfo {
	///Creates a new `PortStatInfo` for a directory
	fn new_dir(uid: uid_t, gid: gid_t) -> Arc<RwLock<Self>> {
		Self::new(S_IFDIR as mode_t | 0o700, uid, gid)
	}
	///Creates a new `PortStatInfo` for a directory
	fn new_port(uid: uid_t, gid: gid_t) -> Arc<RwLock<Self>> {
		Self::new(S_IFMSG as mode_t | 0o600, uid, gid)
	}
	///Base constructor for `PortStatInfo`
	fn new(mode: mode_t, uid: uid_t, gid: gid_t) -> Arc<RwLock<Self>> {
		Arc::new(RwLock::new(Self {
			mode,
			uid,
			gid,
		}))
	}
}

///An individual port
pub struct Port {
	id: i32,
	context_id: i32,
	context: OnceCell<Weak<FSContext>>,
	port_type: InternalPortType,
	base_desc: Arc<FileDescription>,
	control_desc: Option<Arc<FileDescription>>,
	unlinked: AtomicBool,
	client_stat_info: Arc<RwLock<PortStatInfo>>,
	server_stat_info: Arc<RwLock<PortStatInfo>>,
}

impl Port {
	///Creates a new `Port` wwithout associating an `FSContext` with it
	///(this is used to create the initial port for PortFS itself since the
	///root FSContext hasn't yet been created)
	fn new_base(port_type: InternalPortType, context_id: i32, flags: u64) -> Result<Self, VFSError> {
		let (base_desc, control_desc) = match port_type {
			InternalPortType::DirectChannel(opts) => (Self::new_desc(FileDescriptionType::IPCChannel(opts), false)?, None),
			InternalPortType::MountChannel(opts, _, private) => {
				let base = Self::new_desc(FileDescriptionType::IPCChannel(opts), !private)?;
				let control = Some(base.new_shared_client(0, None, None)?);
				(base, control)
			},
			InternalPortType::Notification => (Self::new_desc(FileDescriptionType::Notification, false)?, None),
			InternalPortType::MessageDescriptor => (Self::new_desc(FileDescriptionType::ServerMessage, false)?, None),
			InternalPortType::SecondaryMountChannel(ref orig_port) => {
				let control_desc = orig_port.base_desc.new_shared_client(0, None, None)?;
				(orig_port.base_desc.clone(), Some(control_desc))
			},
			InternalPortType::VFSClientChannel => panic!("TODO: implement handling of VFS RPC channels as ports"),

		};

		if flags & PORT_UNLINK_ON_NO_SERVERS as u64 != 0 {
			base_desc.clunk_on_no_servers(true)?;
		}

		//TODO: set both of these from the uid, gid, and umask of the FSContext
		Ok(Self {
			id: 0,
			context_id,
			context: Default::default(),
			port_type,
			base_desc,
			control_desc,
			unlinked: AtomicBool::new(false),
			client_stat_info: PortStatInfo::new_port(0, 0),
			server_stat_info: PortStatInfo::new_port(0, 0),
		})
	}
	///Creates a new `Port` and associates it with an `FSContext`
	pub fn new(port_type: InternalPortType, context: &Arc<FSContext>, flags: u64) -> Result<Self, VFSError> {
		let port = Self::new_base(port_type, context.get_id(), flags)?;
		let _ = port.context.set(Arc::downgrade(context));
		Ok(port)
	}
	///Sets the `FSContext` for this port
	fn init_context(&self) -> Result<Weak<FSContext>, ServerStatus> {
		let context = get_fs_context(self.context_id).ok_or(SRV_ERR_IO)?;
		Ok(Arc::downgrade(&context))
	}
	///Gets the `FSContext` for this port
	fn get_context(&self) -> Result<Arc<FSContext>, ServerStatus> {
		let weak = self.context.get_or_try_init(|| {
			self.init_context()
		})?;
		weak.upgrade().ok_or(SRV_ERR_IO)
	}
	///Internal method to create a new file description for a port
	fn new_desc(base_type: FileDescriptionType, shared: bool) -> Result<Arc<FileDescription>, VFSError> {
		FileDescription::new(base_type, AccessMode::ReadWrite, 0, shared, false, false, None, None, Some(global_clunk))
	}
	///Gets the file description and file descriptor type for the client
	///side of a port
	pub fn get_client_desc(&self) -> Option<(Arc<FileDescription>, FileDescriptorType)> {
		Some((self.base_desc.clone(), FileDescriptorType::Client))
	}
	///Gets the file description and file descriptor type for the server
	///side of a port
	pub fn get_server_desc(&self) -> Option<(Arc<FileDescription>, FileDescriptorType)> {
		Some((self.base_desc.clone(), FileDescriptorType::Server))
	}
}

impl ArcListItem for Port {
	fn get_id(&self) -> i32 {
		self.id
	}
	fn set_id(&mut self, id: i32) {
		self.base_desc.set_global_id(GlobalPortID::new(id as u32, self.context_id as u32).value());
		self.id = id;
	}
}

impl fmt::Debug for Port {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		writeln!(f, "[Port: id {}, unlinked: {}]", self.id, self.unlinked.load(Ordering::Relaxed))?;
		Ok(())
	}
}

///A mount of the root directory of a filesystem exported by a resource
///manager
pub struct ResMgrMount {
	port: Arc<Port>,
	allow_paths: RwLock<Vec<UnixPathBuf>>,
}

impl ResMgrMount {
	///Creates a new `ResMgrMount`
	pub fn new(port: Arc<Port>) -> Arc<Self> {
		Arc::new(Self {
			port,
			allow_paths: RwLock::new(Vec::new()),
		})
	}
	///Gets the channel file descriptor for VFS RPC messages (this FD is not
	///actually present in the process server FDSpace)
	fn get_rpc_channel(&self) -> Result<VFSRPCClient<UnifiedFileDescriptor>, VFSError> {
		let control_desc = self.port.control_desc.as_ref().ok_or(VFSError::InternalError)?;
		let fd = control_desc.get_control_fd().ok_or(VFSError::InternalError)?;
		Ok(VFSRPCClient::new(fd))
	}
	///Checks if the path is allowed by the prefix filter
	fn path_is_allowed(&self, path: &UnixPath) -> Result<(), ServerStatus> {
		let allowed = self.allow_paths.read();
		if allowed.len() > 0 {
			for i in 0..allowed.len() {
				if path.starts_with(&allowed[i]) {
					return Ok(());
				}
			}
			Err(SRV_ERR_NOENT)
		}else{
			Ok(())
		}
	}
	///Sends an open message, possibly including a secondary I/O request,
	///to a resource manager
	fn send_openat2_msg(&self, pathname: &[u8], how: &open_how_server, ret_pathname: &mut [u8], secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<(VFSServerOpenResult, usize), IOError> {
		if secondary_bufs.len() > 1 {
			return Err(IOError::InvalidArgument);
		}
		let rpc = self.get_rpc_channel()?;
		let mut open_res = VFSServerOpenResult::default();

		let mut bufs = rpc_new_bufs_open_server!();
		rpc_init_open_server!(bufs, pathname.as_bytes(), how.as_bytes(), open_res.as_bytes_mut(), ret_pathname);

		let secondary_reqs = secondary_bufs.len();
		if secondary_reqs > 0 {
			let (secondary_request, secondary_response) = secondary_bufs[0].split_at_mut(secondary_len[0]);
			bufs.push_write_buf_complete(secondary_request)?;
			bufs.push_read_buf_complete(secondary_response)?;
		}

		//info!("ResMgrMount::send_openat2_msg: sending message");
		rpc.send_msg(&mut bufs)?;
		//info!("ResMgrMount::send_openat2_msg: checking path");
		let path_len = rpc_end_open_server!(bufs, IOError::InvalidMessage)?;
		//responses to secondary requests aren't processed by the
		//client side here so just consume any remaining buffers;
		//responses that need to be processed before sending them to
		//the actual client (e.g. fstat) will be handled by
		//VFSRPCHandler callbacks instead
		while bufs.next_read_buf() {
		}
		//info!("ResMgrMount::send_openat2_msg: read size {}, path size {}", read_len, path_len);
		//info!("ResMgrMount: dropping buffers");
		rpc_end_bufs!(bufs)?;
		//info!("ResMgrMount::send_openat2_msg: path: {:?}", core::str::from_utf8(&ret_pathname[..path_len]));

		//info!("ResMgrMount: done");
		Ok((open_res, path_len))
	}
	///Internal implementation of openat2()
	fn handle_openat2_inner(&self, _transport_header: &MsgHeader, fs_context: &FSContext, _base_port: &Arc<Port>, private: bool, path: &UnixPath, how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<OpenResult, ServerStatus> {
		//info!("ResMgrMount::handle_openat2_inner: port id: {}", self.port.id);
		self.path_is_allowed(path)?;
		//info!("ResMgrMount::handle_openat2_inner: path allowed");
		if private {
			//TODO: for resource managers with private channels, allocate a connection ID for each private channel in the same way as connections on shared channels, in order to use in places like the path list in place of shared connection IDs
			warn!("TODO: implement support for resource managers with private channels");
			return Err(SRV_ERR_IO);
		}
		let (desc, connection_id) = if how.flags & O_TRANSIENT as u64 == 0 {
			let d = self.port.base_desc.new_shared_client(how.flags, None, None)?;
			let id = d.get_connection_id();
			(Some(d), id)
		}else{
			(None, AT_FDINVALID)
		};
		let how_server = open_how_server {
			flags: how.flags,
			mode: how.mode,
			conn: connection_id,
			mount: self.port.id,
			context: fs_context.get_id(),
			_pad: 0,
		};

		let mut ret_buf = Vec::with_capacity(PATH_MAX as usize);
		unsafe { ret_buf.set_len(PATH_MAX as usize) };

		//info!("ResMgrMount::handle_openat2_inner: connection id: {}", how_server.conn);
		let (res, path_len) = match self.send_openat2_msg(path.as_bytes(), &how_server, &mut ret_buf, secondary_bufs, secondary_len){
			Ok(r) => r,
			Err(err) => {
				//info!("ResMgrMount::handle_openat2: failure: {}", err.to_errno());
				if let Some(d) = desc {
					//info!("calling desc.open_aborted");
					let _ = d.open_aborted(FileDescriptorType::Client);
				}
				//info!("returning error");
				return Err(err.to_errno());
			},
		};
		//info!("ResMgrMount::handle_openat2: result: {} {} {}", res.res_type, res.res_value, path_len);

		match res.res_type as u32 {
			VFS_RPC_SERVER_OPEN_INODE => {
				//TODO: accept a badge from the resource manager and return it here instead of 0
				Ok(OpenResult::Finish(desc, FileDescriptorType::Client, 0, self.port.base_desc.get_global_id(), res.res_value, None))
			},
			VFS_RPC_SERVER_OPEN_FIRMLINK | VFS_RPC_SERVER_OPEN_SYMLINK => {
				//info!("ResMgrMount::handle_openat2: found link");
				if let Some(d) = desc && let Err(err) = d.open_aborted(FileDescriptorType::Client) {
					Err(err.to_errno())
				}else{
					let context = self.port.get_context()?;
					unsafe { ret_buf.set_len(path_len as usize) };
					Ok(OpenResult::Restart(res, ret_buf, context))
				}
			},
			_ => {
				Err(SRV_ERR_PROTO)
			},
		}
	}
}

impl Mount for ResMgrMount {
	fn allow_path(&self, path: &UnixPath) -> Result<(), ()>{
		self.allow_paths.write().push(UnixPathBuf::from(path));
		Ok(())
	}
	fn handle_openat2(&self, transport_header: &MsgHeader, fs_context: &FSContext, path: &UnixPath, how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<OpenResult, ServerStatus> {
		//info!("ResMgrMount::handle_openat2: {}", path);
		let (base_port, private) = match self.port.port_type {
			InternalPortType::MountChannel(_, _, p) => {
				(&self.port, p)
			},
			InternalPortType::SecondaryMountChannel(ref port) => {
				if let InternalPortType::MountChannel(_, _, p) = port.port_type {
					(port, p)
				}else{
					return Err(SRV_ERR_IO);
				}
			},
			_ => return Err(SRV_ERR_IO),
		};
		self.handle_openat2_inner(transport_header, fs_context, base_port, private, path, how, secondary_bufs, secondary_len)
	}
}

///Filter function for client ports
fn handle_client_port(port: &Arc<Port>) -> Option<(Arc<FileDescription>, FileDescriptorType)> {
	if port.port_type.has_client() && !port.unlinked.load(Ordering::Relaxed) {
		port.get_client_desc()
	}else{
		None
	}
}

///Filter function for server ports
fn handle_server_port(port: &Arc<Port>) -> Option<(Arc<FileDescription>, FileDescriptorType)> {
	if port.port_type.has_server() && !port.unlinked.load(Ordering::Relaxed) {
		port.get_server_desc()
	}else{
		None
	}
}

type PortFilter = (&'static str, fn(&Arc<Port>) -> Option<(Arc<FileDescription>, FileDescriptorType)>);

static MAIN_FILTERS: [PortFilter; 2] = [("client", handle_client_port), ("server", handle_server_port)];

///A port filesystem mount
pub struct PortFSMount {
	ports: LockedArcList<Port>,
	context_id: i32,
	context: Weak<FSContext>,
	filter_fns: &'static [PortFilter],
	inner: Arc<ResMgrMount>,
	root_dir_stat: Arc<RwLock<PortStatInfo>>,
	client_dir_stat: Arc<RwLock<PortStatInfo>>,
	server_dir_stat: Arc<RwLock<PortStatInfo>>,
}

impl PortFSMount {
	///Creates a new `PortFSMount`
	///
	///The contents of individual subdirectories are generated by calling
	///filter functions from a list on individual ports in a list
	fn new_base(filter_fns: &'static [(&'static str, fn(&Arc<Port>) -> Option<(Arc<FileDescription>, FileDescriptorType)>)], context: &Arc<FSContext>) -> Result<Arc<Self>, VFSError> {
		let base_resmgr_port = PORTFS_RESMGR_PORT.get().expect("PortFS resource manager port unset");
		let resmgr_port = Arc::new(Port::new(InternalPortType::SecondaryMountChannel(base_resmgr_port.clone()), context, 0)?);

		let inner = ResMgrMount::new(resmgr_port);

		Ok(Arc::new(Self {
			ports: LockedArcList::new(),
			context_id: context.get_id(),
			context: Arc::downgrade(&context),
			filter_fns,
			inner,
			root_dir_stat: PortStatInfo::new_dir(0, 0),
			client_dir_stat: PortStatInfo::new_dir(0, 0),
			server_dir_stat: PortStatInfo::new_dir(0, 0),
		}))
	}
	///Creates a new main PortFS for an FSContext
	pub fn new_main(context: &Arc<FSContext>) -> Result<Arc<Self>, VFSError> {
		Self::new_base(&MAIN_FILTERS, context)
	}
	///Gets a `Port` from this PortFS instance
	fn get_port(&self, id: i32) -> Result<Arc<Port>, ServerStatus> {
		Ok(self.ports.get(id).ok_or(SRV_ERR_NOENT)?)
	}
	///Internal method to handle paths and run method-specific callbacks
	///
	///The first closure is called if the access is to the `create` file
	///The second is called for accesses to the port files themselves
	///The third is for accesses to the port directories
	fn match_path<P, D, O>(&self, path: &UnixPath, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize], mut port_fn: P, mut dir_fn: D, mut other_fn: O) -> Result<(), ServerStatus> where
		P: FnMut(&UnixPath, &mut [Vec<u8>], &mut [usize], &Arc<Port>, Arc<FileDescription>, FileDescriptorType) -> Result<(), ServerStatus>,
		D: FnMut(&UnixPath, &mut [Vec<u8>], &mut [usize]) -> Result<(), ServerStatus>,
		O: FnMut(&UnixPath, &mut [Vec<u8>], &mut [usize]) -> Result<(), ServerStatus>,{
		if let Some(dir_name) = path.parent() {
			//info!("match_path: matching against filter");
			let mut filter_opt = None;
			for (n, f) in self.filter_fns {
				if n.as_bytes() == dir_name {
					filter_opt = Some(f);
					break;
				}
			}
			let filter_fn = if let Some(f) = filter_opt {
				f
			}else{
				//info!("match_path: matching as other");
				return other_fn(&path, secondary_bufs, secondary_len);
			};
			if let Some(file_name) = path.file_name() {
				let utf8_name = str::from_utf8(file_name).or(Err(SRV_ERR_NOENT))?;
				let id = i32::from_str_radix(utf8_name, 10).or(Err(SRV_ERR_NOENT))?;

				let port = self.get_port(id)?;
				let (desc, fd_type) = filter_fn(&port).ok_or(SRV_ERR_NOENT)?;
				port_fn(&dir_name, secondary_bufs, secondary_len, &port, desc, fd_type)
			}else{
				dir_fn(&dir_name, secondary_bufs, secondary_len)
			}
		}else{
			//info!("match_path: matching as other");
			other_fn(&path, secondary_bufs, secondary_len)
		}
	}
	///Adds a port to this filesystem
	pub fn add_port(&self, port_type: InternalPortType, flags: u64) -> Result<Arc<Port>, VFSError> {
		let context = self.context.upgrade().ok_or(VFSError::InternalError)?;
		let port = Port::new(port_type, &context, flags)?;
		let (_, port) = self.ports.add(port).or(Err(VFSError::TooManyFiles))?;
		let global_id = GlobalPortID::new(port.id as u32, self.context_id as u32).value;
		port.base_desc.set_global_id(global_id);
		Ok(port)
	}
	///Unlinks a port from this filesystem. If it is unused it will be
	///deleted right away; if it is in use it will only be deleted after the
	///last file descriptor is closed.
	pub fn unlink_port(&self, id: i32) -> Result<(), ServerStatus> {
		if let Some(port) = self.ports.get(id){
			//info!("PortFSMount::handle_unlinkat: unlinked port");
			port.unlinked.store(true, Ordering::Relaxed);
			let client_res = port.base_desc.clunk_on_no_clients(true);
			let server_res = port.base_desc.clunk_on_no_servers(true);
			client_res?;
			server_res?;
			Ok(())
		}else{
			Err(SRV_ERR_NOENT)
		}
	}
	///Gets a resource manager mount for a port iff it is a mount channel
	pub fn get_resmgr(&self, id: i32) -> Result<Arc<dyn Mount + Send + Sync>, VFSError> {
		//info!("PortFSMount::get_resmgr: {} {}", self.context_id, id);
		let port = self.ports.get(id).ok_or(VFSError::FileNotFound)?;
		match port.port_type {
			InternalPortType::MountChannel(..) | InternalPortType::SecondaryMountChannel(..) => {
				if port.unlinked.load(Ordering::Relaxed){
					Err(VFSError::FileNotFound)
				}else{
					//info!("port is valid");
					Ok(ResMgrMount::new(port))
				}
			},
			_ => Err(VFSError::FileNotFound),
		}
	}
	///Returns true for a valid port (regardless of whether it has been
	///unlinked). Mostly intended for use in tests
	pub fn port_is_valid(&self, id: i32) -> bool {
		if let Some(_) = self.ports.get(id) {
			true
		}else{
			false
		}
	}
	///Returns true for a unlinked or invalid port. Mostly intended for use
	///in tests
	pub fn port_is_unlinked(&self, id: i32) -> bool {
		if let Some(port) = self.ports.get(id) {
			port.unlinked.load(Ordering::Relaxed)
		}else{
			true
		}
	}
	///Passes an open through to the inner resource manager mount handler,
	///replacing the port ID with the one of this handler
	fn handle_openat2_inner(&self, transport_header: &MsgHeader, fs_context: &FSContext, path: &UnixPath, how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<OpenResult, ServerStatus> {
		let res = self.inner.handle_openat2(transport_header, fs_context, path, how, secondary_bufs, secondary_len)?;
		match res {
			OpenResult::Finish(desc, fd_type, badge, _, inode, secondary_size) => Ok(OpenResult::Finish(desc, fd_type, badge, self.get_self_port_id(), inode, secondary_size)),
			_ => Ok(res),
		}
	}
	///Gets the reserved port ID for this PortFS mount
	fn get_self_port_id(&self) -> dev_t {
		GlobalPortID::new(PORTFS_PORT_ID as u32, self.context_id as u32).value
	}
}

const PORTFS_PORT_ID: i32 = i32::MIN as i32;

impl Mount for PortFSMount {
	fn handle_openat2(&self, transport_header: &MsgHeader, fs_context: &FSContext, path: &UnixPath, how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize]) -> Result<OpenResult, ServerStatus> {
		//info!("secondary len: {}", secondary_bufs.len());
		//info!("PortFSMount::handle_openat2: {}", path);
		let res = RefCell::new(Err(SRV_ERR_IO));
		self.match_path(path, secondary_bufs, secondary_len,
		|_dir_name, secondary_bufs, secondary_len, port, port_desc, fd_type| {
			//info!("PortFSMount::handle_openat2: handling request as port");
			let mut how_inner = how;
			let mut how_clone;
			if how.flags & O_DIRECTORY as u64 != 0 {
				return Err(SRV_ERR_ISDIR);
			}

			let mut open_inner = !port_desc.has_alt_rpc_io_desc(fd_type) || how.flags & O_TRANSIENT as u64 != 0;

			if secondary_bufs.len() > 0 {
				how_clone = how.clone();
				if !open_inner {
					how_clone.flags |= O_TRANSIENT as u64;
				}
				how_inner = &how_clone;
				open_inner = true;
			}

			if open_inner {
				//info!("PortFS::handle_openat2: forwarding request to resource manager");
				let open_res = self.inner.handle_openat2(transport_header, fs_context, path, how_inner, secondary_bufs, secondary_len)?;
				match open_res {
					OpenResult::Finish(desc_opt, _, _, _, _, _) => {
						if let Some(alt_desc) = desc_opt {

							//info!("PortFS::handle_openat2: got response");
							let connection_id = alt_desc.get_connection_id();
							port_desc.set_rpc_io_desc(fd_type, Some((alt_desc, connection_id)));
						} else if how.flags & O_TRANSIENT as u64 == 0 {
							return Err(SRV_ERR_IO);
						}

					}
					_ => return Err(SRV_ERR_IO),
				};
			}
			let inode = match fd_type {
				FileDescriptorType::Client => PortFSInode::new(CLIENT_INODE_BASE as u32, port.id as u32),
				FileDescriptorType::Server => PortFSInode::new(SERVER_INODE_BASE as u32, port.id as u32),
			};
			let _ = res.replace(Ok(OpenResult::Finish(Some(port_desc.clone()), fd_type, 0, self.get_self_port_id(), inode.value(), None)));
			Ok(())
		},
		|_, secondary_bufs, secondary_len| {
			//info!("PortFSMount::handle_openat2: handling request as directory");
			let _ = res.replace(self.handle_openat2_inner(transport_header, fs_context, path, how, secondary_bufs, secondary_len));
			Ok(())
		},
		|_, secondary_bufs, secondary_len|{
			//info!("PortFSMount::handle_openat2: handling request as other");
			let _ = res.replace(self.handle_openat2_inner(transport_header, fs_context, path, how, secondary_bufs, secondary_len));
			Ok(())
		})?;
		//info!("PortFSMount::openat2: done");
		res.into_inner()
	}
	fn handle_unlinkat(&self, transport_header: &MsgHeader, fs_context: &  FSContext, pathname: &UnixPath, flags: i32) -> Result<ConnectResult, ServerStatus> {
		//info!("PortFSMount::handle_unlinkat: {}", pathname);
		let res = RefCell::new(Err(SRV_ERR_IO));
		self.match_path(pathname, &mut [], &mut [],
		|_, _, _, port, _, _| {
			self.unlink_port(port.id)?;
			Ok(())
		},
		|_, _, _| {
			let _ = res.replace(self.inner.handle_unlinkat(transport_header, fs_context, pathname, flags));
			Ok(())
		},
		|_, _, _|{
			let _ = res.replace(self.inner.handle_unlinkat(transport_header, fs_context, pathname, flags));
			Ok(())
		},
		)?;
		Ok(ConnectResult::Finish)
	}
}

impl fmt::Debug for PortFSMount {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		for port in self.ports.read().iter() {
			write!(f, "{:?}", port)?;
		}
		Ok(())
	}
}

const SERVER_INODE_BASE: i32 = 0;
const CLIENT_INODE_BASE: i32 = 1;
const DIR_INODE_BASE: i32 = 2;
const OTHER_INODE_BASE: i32 = 3;

///Type-specific contents of a `PortFSOCB`
#[derive(Copy, Clone)]
enum PortFSOCBContents {
	ClientPort(i32),
	Create,
	Dir(i32),
	ServerPort(i32),
}

impl PortFSOCBContents {
	///Gets the inode number for this OCB
	fn as_ino(&self) -> ino_t {
		let (base, ino) = match self {
			Self::ClientPort(ino) => (CLIENT_INODE_BASE, *ino),
			Self::Create => (OTHER_INODE_BASE, 0),
			Self::Dir(ino) => (DIR_INODE_BASE, *ino),
			Self::ServerPort(ino) => (SERVER_INODE_BASE, *ino),
		};
		PortFSInode::new(base as u32, ino as u32).value()
	}
	///Gets the open result for this OCB
	fn as_res(&self) -> (VFSServerOpenResult, usize) {
		VFSServerOpenResult::inode(self.as_ino())
	}
}

///An OCB for the inner resource manager handler of PortFS
struct PortFSOCB {
	context_id: i32,
	contents: PortFSOCBContents,
	common_ocb: CommonOCB,
}

impl PortFSOCB {
	///Creates a new `PortFSOCB`
	pub fn new(context_id: i32, contents: PortFSOCBContents) -> Self {
		Self {
			context_id,
			contents,
			common_ocb: CommonOCB::new(),
		}
	}
}

impl OpenControlBlock for PortFSOCB {
	fn get_common<'a>(&'a self) -> &'a CommonOCB {
		&self.common_ocb
	}
	fn get_common_mut<'a>(&'a mut self) -> &'a mut CommonOCB {
		&mut self.common_ocb
	}
}

///Dummy dispatch context state for PortFS
struct PortFSContextExtra {}
impl ResMgrContextExtra for PortFSContextExtra {
	fn new() -> Self {
		Self {}
	}
}

///Inner resource manager handler for PortFS
struct PortFSResMgr {
}

impl PortFSResMgr {
	///Creates a new `PortFSResMgr`
	///
	///A single instance handles requests from all `PortFSMount`s,
	///distinguishing between them by the context ID provided in the open
	///request and saved on the OCB
	fn new() -> Arc<Self> {
		Arc::new(Self {})
	}
	///Gets the base `PortFSMount` associated with this handler
	fn get_base_mount(&self, context_id: i32) -> Result<Arc<PortFSMount>, IOError> {
		let context = get_fs_context(context_id).ok_or(IOError::ServerError(SRV_ERR_NOENT))?;
		Ok(context.get_ports())
	}
	///Handles a request to create a port
	fn handle_create_port<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, fs_context_id: i32, dispatch_context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>) -> Result<(usize, Offset), IOError> {
		//info!("PortFSResMgr::handle_create_port");
		//TODO: handle creating multiple ports in one request (the input should be a simple array of requests and the output should be an array of IDs
		let transport_header = dispatch_context.get_transport_header();
		if transport_header.client_read_size != size_of::<create_port_response>() {
			return Err(IOError::InvalidMessage);
		}
		let cw_size = transport_header.client_write_size;
		if cw_size != size_of::<create_port_request>() {
			//info!("handle_create_port: invalid size {}; should be {}", cw_size, size_of::<create_port_request>());
			return Err(IOError::InvalidMessage);
		}

		let msg_fd = dispatch_context.get_message_fd().ok_or(SRV_ERR_IO)?;

		let mut request = create_port_request::default();

		let read_size = msg_fd.read(request.as_bytes_mut())?;
		if read_size != size_of::<create_port_request>() {
			//info!("handle_create_port: invalid read size: {}", read_size);
			return Err(IOError::ServerError(SRV_ERR_IO));
		}

		let request_type = PortType::from_u64(request.port_type).ok_or(SRV_ERR_PROTO)?;
		let mut channel_opts = ServerChannelOptions::new();
		if request.flags & PORT_CHANNEL_GET_REPLY_SIZE as u64 != 0 {
			channel_opts = channel_opts.get_reply_size();
		}
		if request.flags & PORT_CHANNEL_GET_RECV_TOTAL_SIZE as u64 != 0 {
			channel_opts = channel_opts.get_recv_total_size();
		}
		let (valid_flags, internal_type) = match request_type {
			PortType::DirectChannel => {
				(PORT_DIRECT_CHANNEL_VALID_FLAGS, InternalPortType::DirectChannel(channel_opts))
			},
			PortType::MountChannel => {
				let multiple = request.flags & PORT_MOUNT_MULTIPLE as u64 != 0;
				let private = request.flags & PORT_MOUNT_PRIVATE_CHANNEL as u64 != 0;
				(PORT_MOUNT_CHANNEL_VALID_FLAGS, InternalPortType::MountChannel(channel_opts, multiple, private))
			},
			PortType::Notification => {
				(PORT_NOTIFICATION_VALID_FLAGS, InternalPortType::Notification)
			},
			PortType::MessageDescriptor => {
				(PORT_MESSAGE_VALID_FLAGS, InternalPortType::MessageDescriptor)
			},
		};
		if request.flags & !(valid_flags as u64) != 0 {
			//info!("handle_create_port: invalid flags: {:x} {:x}", request.flags, valid_flags);
			return Err(IOError::InvalidMessage);
		}
		let fs_context = get_fs_context(fs_context_id).ok_or(SRV_ERR_IO)?;
		let ports = fs_context.get_ports();
		let port = ports.add_port(internal_type, request.flags as u64)?;
		let fd = if request.flags & PORT_OPEN_AFTER_CREATE as u64 != 0 {
			let fdspace = fs_context.get_fdspace();
			let id = fdspace.write()
				.insert(&port.base_desc, FileDescriptorType::Server, AccessMode::ReadWrite, 0, None)?;
			id
		}else{
			i32::MIN
		};

		let res = create_port_response {
			port: port.id,
			fd,
		};
		msg_fd.mwpwrite(res.as_bytes(), 0, OffsetType::Start, 0)?;
		Ok((request.as_bytes().len(), 0))
	}
	///Base callback for primitive messages
	fn handle_primitive_base<F>(&self, ocb: &PortFSOCB, mut create_fn: F) -> Result<(usize, i64), IOError> where F: FnMut() -> Result<(usize, i64), IOError>{
		match ocb.contents {
			PortFSOCBContents::ClientPort(_) => Err(IOError::ServerError(SRV_ERR_IO)),
			PortFSOCBContents::Create => create_fn(),
			PortFSOCBContents::Dir(_) => Err(IOError::ServerError(SRV_ERR_ISDIR)),
			PortFSOCBContents::ServerPort(_) => Err(IOError::ServerError(SRV_ERR_IO)),
		}
	}
	///Handles an fstat() request for a port
	fn handle_fstat_port(&self, context_id: i32, port_id: i32, ocb_contents: &PortFSOCBContents, statbuf: &mut stat) -> Result<(), ServerStatus>{
		//info!("handle_fstat_port: {} {}", context_id, port_id);
		let port = self.get_port(context_id, port_id)?;
		let stat_info = match ocb_contents {
			PortFSOCBContents::ClientPort(_) => {
				//info!("port is client");
				port.client_stat_info.read()
			},
			PortFSOCBContents::ServerPort(_) => {
				//info!("port is server");
				port.server_stat_info.read()
			},
			_ => return Err(SRV_ERR_IO),
		};
		statbuf.st_mode = stat_info.mode;
		statbuf.st_uid = stat_info.uid;
		statbuf.st_gid = stat_info.gid;
		statbuf.st_ino = ocb_contents.as_ino();
		Ok(())
	}
	///Gets a port
	fn get_port(&self, context_id: i32, port_id: i32) -> Result<Arc<Port>, ServerStatus> {
		let mount = self.get_base_mount(context_id)?;
		mount.get_port(port_id)
	}
	///Gets the stat info `RwLock` to use when writing to the stat info
	fn get_stat_info_write(&self, ocb: &PortFSOCB) -> Result<Arc<RwLock<PortStatInfo>>, ServerStatus> {
		match ocb.contents {
			PortFSOCBContents::ClientPort(id) => {
				Ok(self.get_port(ocb.context_id, id)?.client_stat_info.clone())
			},
			PortFSOCBContents::ServerPort(id) => {
				Ok(self.get_port(ocb.context_id, id)?.server_stat_info.clone())
			},
			PortFSOCBContents::Create => {
				Err(SRV_ERR_INVAL)
			},
			PortFSOCBContents::Dir(id) => {
				let base_mount = self.get_base_mount(ocb.context_id)?;
				if id == 0 {
					Ok(base_mount.root_dir_stat.clone())
				} else if id == SERVER_INODE_BASE {
					Ok(base_mount.server_dir_stat.clone())
				} else if id == CLIENT_INODE_BASE {
					Ok(base_mount.client_dir_stat.clone())
				} else {
					Err(SRV_ERR_IO)
				}
			}
		}
	}
}

impl RPCConnectHandler<PortFSOCB, PortFSContextExtra> for PortFSResMgr {
	fn handle_openat2<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>,  path: &mut [u8], path_size: usize, how: &open_how_server, _res_offset: usize) -> Result<(VFSServerOpenResult, usize), ServerStatus>{
		//info!("PortFSResMgr::handle_openat2");
		let contents_opt = RefCell::new(None);
		//TODO: support multiple mounts (one per context)
		let context_id = 0;
		self.get_base_mount(context_id)?.match_path(UnixPath::new(&path[..path_size]), &mut [], &mut [],
		|_dir_name, _, _, port, _port_desc, fd_type| {
			contents_opt.replace(match fd_type {
				FileDescriptorType::Client => Some(PortFSOCBContents::ClientPort(port.id)),
				FileDescriptorType::Server => Some(PortFSOCBContents::ServerPort(port.id)),
			});
			Ok(())
		},
		|dir_path, _, _| {
			contents_opt.replace(if dir_path == UnixPath::new("client") {
				Some(PortFSOCBContents::Dir(CLIENT_INODE_BASE + 1))
			}else if dir_path == UnixPath::new("server") {
				Some(PortFSOCBContents::Dir(SERVER_INODE_BASE + 1))
			}else{
				return Err(SRV_ERR_NOENT)
			});
			Ok(())
		},
		|file_path, _, _| {
			//info!("PortFSResMgr::handle_openat2: {:?}", core::str::from_utf8(file_path.as_bytes()));
			if file_path == UnixPath::new("create") {
				contents_opt.replace(Some(PortFSOCBContents::Create));
			}else if path_size == 0 {
				contents_opt.replace(Some(PortFSOCBContents::Dir(0)));
			}else{
				return Err(SRV_ERR_NOENT);
			}
			Ok(())
		}
		)?;
		//info!("PortFS::handle_openat2: done");
		if let Some(contents) = contents_opt.take() {
			context.bind_ocb(PortFSOCB::new(how.context, contents)).or(Err(SRV_ERR_IO))?;
			Ok(contents.as_res())
		}else{
			Err(SRV_ERR_IO)
		}
	}
}

impl RPCIOHandler<PortFSOCB, PortFSContextExtra> for PortFSResMgr {
	fn handle_fstat<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, _dispatch_context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, ocb: &mut PortFSOCB, statbuf: &mut stat) -> Result<(), ServerStatus> {
		//info!("PortFSResMgr::handle_fstat");
		match ocb.contents {
			PortFSOCBContents::ClientPort(id) | PortFSOCBContents::ServerPort(id) => {
				//info!("handle_fstat: port");
				self.handle_fstat_port(ocb.context_id, id, &ocb.contents, statbuf)?;
			},
			PortFSOCBContents::Create => {
				//info!("handle_fstat: create");
				statbuf.st_nlink = 1;
				//TODO: set these from the context
				statbuf.st_uid = 0;
				statbuf.st_gid = 0;
				statbuf.st_mode = S_IFMSG as mode_t | 0o600;
			},
			PortFSOCBContents::Dir(id) => {
				//info!("handle_fstat: dir");
				let base_mount = self.get_base_mount(ocb.context_id)?;
				let base_info = if id == 0 {
					base_mount.root_dir_stat.read()
				}else if id == SERVER_INODE_BASE {
					base_mount.server_dir_stat.read()
				}else if id == CLIENT_INODE_BASE {
					base_mount.client_dir_stat.read()
				}else{
					return Err(SRV_ERR_IO);
				};
				statbuf.st_mode = base_info.mode;
				statbuf.st_uid = base_info.uid;
				statbuf.st_gid = base_info.gid;
				statbuf.st_nlink = 2;
			}
		};
		statbuf.st_dev = 0;
		statbuf.st_blksize = 0;
		statbuf.st_blocks = 0;
		statbuf.st_ino = ocb.contents.as_ino();

		statbuf.st_atim.tv_sec = 0;
		statbuf.st_atim.tv_nsec = 0;
		statbuf.st_mtim.tv_sec = 0;
		statbuf.st_mtim.tv_nsec = 0;
		statbuf.st_ctim.tv_sec = 0;
		statbuf.st_ctim.tv_nsec = 0;

		Ok(())
	}
	fn handle_fchmod<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, _dispatch_context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, ocb: &mut PortFSOCB, mode: mode_t) -> Result<(), ServerStatus> {
		let b = self.get_stat_info_write(ocb)?;
		let mut base_info = b.write();
		base_info.mode = (base_info.mode & S_IFMT as mode_t) | (mode & !S_IFMT as mode_t);
		Ok(())
	}
	fn handle_fchown<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, _dispatch_context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, ocb: &mut PortFSOCB, uid: uid_t, gid: gid_t) -> Result<(), ServerStatus> {
		//info!("PortFSResMgr::handle_fchown: {} {}", uid, gid);
		let b = self.get_stat_info_write(ocb)?;
		let mut base_info = b.write();
		base_info.uid = uid;
		base_info.gid = gid;
		Ok(())
	}
}

impl DirectoryIOHandler<PortFSOCB, PortFSContextExtra> for PortFSResMgr {
	fn handle_preaddir<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, _ocb: &mut PortFSOCB, recs: usize, rec_size: usize, pos: off_t, whence: OffsetType, data_offset: usize) -> Result<usize, IOError> {
		warn!("TODO: implement directory reading for PortFS");
		Err(IOError::ServerError(SRV_ERR_IO))
	}
	fn handle_rewinddir<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, _ocb: &mut PortFSOCB) -> Result<(), IOError> {
		warn!("TODO: implement directory reading for PortFS");
		Err(IOError::ServerError(SRV_ERR_IO))
	}
}

impl PrimitiveIOHandler<PortFSOCB, PortFSContextExtra> for PortFSResMgr {
	fn handle_wpreadv_outer<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, _context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, ocb: &mut PortFSOCB, _size: usize, _offset: i64, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.handle_primitive_base(ocb, || { Err(IOError::InvalidOperation) })
	}
	fn handle_wpwritev_outer<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, _context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, ocb: &mut PortFSOCB, _size: usize, _offset: i64, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.handle_primitive_base(ocb, || { Err(IOError::InvalidOperation) })
	}
	fn handle_writereadv_outer<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, dispatch_context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, ocb: &mut PortFSOCB, _w_size: usize, _r_size: usize, offset: i64, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.handle_primitive_base(ocb, || {
			//info!("PortFSResMgr::handle_writereadv_outer: {} {:?}", offset, whence);
			if offset == 0 && whence.base() == OffsetType::Current {
				self.handle_create_port(ocb.context_id, dispatch_context)
			}else{
				Err(IOError::InvalidArgument)
			}
		})
	}
	fn handle_clunk<H: ResMgrMsgHandler<PortFSOCB, PortFSContextExtra>>(&self, _context: &ResMgrContext<PortFSOCB, H, PortFSContextExtra>, _ocb: &mut PortFSOCB) {
	}
}

static PORTFS_THREAD_POOL: OnceCell<ResMgrThreadPool<PortFSOCB, PortFSResMgr, PortFSContextExtra, RootPoolThread>> = OnceCell::new();
static PORTFS_RESMGR_PORT: OnceCell<Arc<Port>> = OnceCell::new();

///Initializes PortFS
pub fn init_portfs() {
	let mut resmgr_port = Port::new_base(InternalPortType::MountChannel(ServerChannelOptions::new().get_recv_total_size().get_reply_size(), true, false), 0, 0).expect("could not create PortFS server port");
	resmgr_port.set_id(-1);
	let f = get_root_fdspace();
	let mut fdspace = f.write();
	let (desc, fd_type) = resmgr_port.get_server_desc().expect("PortFS server port has no server file description");
	let fd = fdspace.insert(&desc, fd_type, AccessMode::ReadWrite, 0, None).expect("could not add file description for PortFS server port");

	let factory = ResMgrPort::new_base(-1, fd, PortFSResMgr::new(), BasicMessageFDAllocator::new()).expect("could not create factory for PortFS");

	let thread_pool = new_resmgr_thread_pool(&factory, 2, 10, 20, 2, "portfs").expect("cannot create PortFS thread pool");
	PORTFS_THREAD_POOL.set(thread_pool).or(Err(())).expect("could not set PortFS thread pool");
	PORTFS_RESMGR_PORT.set(Arc::new(resmgr_port)).or(Err(())).expect("could not set PortFS resource manager port");
}

///Starts PortFS handler threads
pub fn start_portfs() {
	PORTFS_THREAD_POOL.get().as_ref().expect("PortFS handler unset").start().expect("cannot start PortFS handler");
}

pub fn add_custom_slabs() {
	add_arc_slab!(ResMgrMount, 512, 4, 4, 2, "resource manager mount");
	add_arc_list_slab!(Port, 512, 4, 4, 2, "port");
	add_arc_slab!(PortFSMount, 512, 4, 4, 2, "PortFS mount");
}
