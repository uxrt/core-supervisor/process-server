/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///Thread-related structs and functions

use core::fmt;
use core::cell::{
	Cell,
	RefCell,
};
use alloc::string::String;
use alloc::sync::Arc;
use alloc::vec::Vec;

use intrusive_collections::UnsafeRef;

use sel4_sys::CONFIG_SELFE_ROOT_STACK;

use sel4::{
	Endpoint,
	FromCap,
	Thread,
	ToCap,
};

use sel4_alloc::{
	AllocatorBundle,
	cspace::CSpaceManager,
};

use sel4_thread::{
	BaseThread,
	CommonThreadConfig,
	LocalThread,
	LocalThreadConfig,
	SchedParams,
	ThreadError,
	ThreadReturn,
	WrappedThread,
};

use uxrt_transport_layer::FDArray;

use crate::global_heap_alloc::get_kobj_alloc;
use super::{
	INIT_THREAD_NAME,
	ROOT_PID,
	Task,
	task_state_initialized,
};

use crate::vfs::{
	VFSError,
	get_root_fd_arrays,
	vfs_root_thread_init,
};
use crate::vfs::rpc::{
	FSContext,
	get_root_default_fs_context,
};
use crate::vfs::transport::FDSpaceContainer;
use crate::vfs::fsspace::MountTableContainer;
use crate::vm::get_root_alloc;
use crate::vm::vspace::{
	get_root_vspace,
	VSpaceContainer,
};

static DEFAULT_LOCAL_CONFIG: LocalThreadConfig = LocalThreadConfig {
	stack_size: CONFIG_SELFE_ROOT_STACK as usize,
	allocate_ipc_buffer: true,
	create_reply: false,
	exit_endpoint: None,
};

///A thread-related error
#[derive(Clone, Copy, Debug, Fail)]
pub enum SysThreadError {
	#[fail(display = "Thread error")]
	Base(ThreadError),
	#[fail(display = "VFS error")]
	VFSError(VFSError),
}

///Converts ThreadError to SysThreadError
fn base_to_sys(res: Result<(), ThreadError>) -> Result<(), SysThreadError> {
	if let Err(err) = res {
		return Err(SysThreadError::Base(err));
	}
	Ok(())
}

///Gets the global ID for a thread given the PID and TID
fn get_gtid(pid: i32, tid: i32) -> u64 {
	(pid as u64) << 32 | (tid as u64)
}

///A handle to a thread, used where acquiring a lock to the full thread is
///undesirable
#[derive(Clone)]
pub struct ThreadHandle {
	tcb: Thread,
	fds: Option<UnsafeRef<FDArray>>,
	vspace: Arc<VSpaceContainer>,
	pid: i32,
	tid: i32,
}

impl ThreadHandle {
	///Creates a new `ThreadHandle`
	fn new(tcb: Thread, fds: Option<UnsafeRef<FDArray>>, vspace: Arc<VSpaceContainer>, pid: i32, tid: i32) -> ThreadHandle {
		ThreadHandle {
			tcb,
			fds,
			vspace,
			pid,
			tid,
		}
	}
	///Gets the process ID of this thread
	pub fn get_pid(&self) -> i32 {
		self.pid
	}
	///Gets the ID of this thread
	pub fn get_tid(&self) -> i32 {
		self.tid
	}
	///Gets the global ID of this thread
	pub fn get_gtid(&self) -> u64 {
		get_gtid(self.pid, self.tid)
	}
	//TODO?: if necessary, either return an error in the kernel when a thread is already paused or track whether a thread is runnable with a lock around the flag
	///Pauses this thread.
	pub fn pause(&self) -> Result<(), sel4::Error> {
		self.tcb.pause()
	}
	///Suspends this thread.
	pub fn suspend(&self) -> Result<(), sel4::Error> {
		self.tcb.suspend()
	}
	///Resumes this thread.
	pub fn resume(&self) -> Result<(), sel4::Error> {
		self.tcb.resume()
	}
	///Returns true if this thread is not the calling thread
	pub fn is_other(&self) -> bool {
		let res = self.pid != ROOT_PID || self.tid != get_current_tid();
		//info!("is_other: {} {} {} {}", self.pid, self.tid, get_current_tid(), res);
		res
	}
	///Pauses this thread if it is not the calling thread.
	pub fn pause_if_other(&self) -> Result<(), sel4::Error> {
		if self.is_other(){
			self.pause()
		}else{
			Ok(())
		}
	}
	///Suspends this thread if it is not the calling thread.
	pub fn suspend_if_other(&self) -> Result<(), sel4::Error> {
		if self.is_other(){
			self.suspend()
		}else{
			Ok(())
		}
	}
	///Resumes the thread if it was not the calling thread.
	pub fn resume_if_other(&self) -> Result<(), sel4::Error> {
		if self.is_other(){
			self.resume()
		}else{
			Ok(())
		}
	}
	///Gets the VSpace of this thread
	pub fn get_vspace(&self) -> Arc<VSpaceContainer> {
		self.vspace.clone()
	}
}

///The base thread object of a `SysThread`.
enum InnerThread {
	Kernel(BaseThread),
	RootInit(BaseThread),
	Root(LocalThread),
	User(BaseThread),
}

///Enum for exit endpoint argument; selects whether to create a new private
///endpoint or use an existing shared one
pub enum ExitEndpoint {
	Private,
	Shared(Endpoint),
}

impl ExitEndpoint {
	///Creates a new ExitEndpoint::Private wrapped in Some
	pub fn private() -> Option<Self> {
		Some(Self::Private)
	}
	///Creates a new ExitEndpoint::Shared wrapped in Some
	pub fn shared(endpoint: Endpoint) -> Option<Self> {
		Some(Self::Shared(endpoint))
	}
}

///The system-level wrapper for threads.
pub struct SysThread {
	inner: InnerThread,
	pub(crate) pid: i32,
	pub(crate) tid: i32,
	fs_context: Option<Arc<FSContext>>,
	vspace: Option<Arc<VSpaceContainer>>,
	private_exit_endpoint: bool,
}

impl WrappedThread for SysThread {
	fn get_base_thread(&self) -> &BaseThread {
		match self.inner {
			InnerThread::Kernel(ref inner) => &inner,
			InnerThread::RootInit(ref inner) => &inner,
			InnerThread::Root(ref inner) => inner.get_base_thread(),
			InnerThread::User(ref inner) => &inner,
		}
	}
	fn get_base_thread_mut(&mut self) -> &mut BaseThread {
		match self.inner {
			InnerThread::Kernel(ref mut inner) => inner,
			InnerThread::RootInit(ref mut inner) => inner,
			InnerThread::Root(ref mut inner) => inner.get_base_thread_mut(),
			InnerThread::User(ref mut inner) => inner,
		}
	}
	fn get_tcb(&self) -> Thread {
		self.get_base_thread().get_tcb()
	}
	fn suspend(&self) -> sel4::Result {
		self.get_base_thread().suspend()
	}
}

impl SysThread {
	///Creates a new dummy kernel idle thread object
	pub fn new_kernel(pid: i32, tid: i32) -> Result<SysThread, SysThreadError> {
		Ok(SysThread {
			inner: InnerThread::Kernel(BaseThread::new_from_tcb(Thread::from_cap(0))),
			pid,
			tid,
			fs_context: None,
			vspace: None,
			private_exit_endpoint: false,
		})
	}
	///Creates the thread object for the initial root server thread.
	pub fn new_root_init(mut common_config: CommonThreadConfig, tcb: Thread, pid: i32, base_tid: i32, fs_context: Arc<FSContext>) -> Result<SysThread, SysThreadError> {
		let kobj_alloc = get_kobj_alloc();
		let fault_endpoint = get_root_alloc().allocate_fault_endpoint(pid, base_tid, &kobj_alloc);
		if let Err(err) = fault_endpoint {
			return Err(SysThreadError::Base(ThreadError::CSpaceAllocationError { details: err }));
		}
		common_config.fault_endpoint = fault_endpoint.unwrap();

		let mut base_thread = BaseThread::new_from_tcb(tcb);

		if let Err(err) = base_thread.set_space(common_config){
			return Err(SysThreadError::Base(err))
		}

		let vspace = get_root_vspace();

		assert!(get_current_tid() == 0, "Thread::new_root_init called from thread other than the initial thread");

		vfs_root_thread_init(fs_context.get_context_fdspace().read().get_user_fds());

		Ok(SysThread {
			inner: InnerThread::RootInit(base_thread),
			pid,
			tid: base_tid,
			fs_context: Some(fs_context),
			vspace: Some(vspace),
			private_exit_endpoint: false,
		})
	}
	///Creates a new root server thread
	pub fn new_root(mut common_config: CommonThreadConfig, sched_params: SchedParams, pid: i32, base_tid: i32, mut fs_context: Option<Arc<FSContext>>, exit_endpoint: Option<ExitEndpoint>) -> Result<SysThread, SysThreadError> {
		let kobj_alloc = get_kobj_alloc();
		let fault_endpoint = get_root_alloc().allocate_fault_endpoint(pid, base_tid, &kobj_alloc);
		if let Err(err) = fault_endpoint {
			return Err(SysThreadError::Base(ThreadError::CSpaceAllocationError { details: err }));
		}
		common_config.fault_endpoint = fault_endpoint.unwrap();

		let mut local_config = DEFAULT_LOCAL_CONFIG.clone();
		let mut private_exit_endpoint = false;
		local_config.exit_endpoint = if let Some(opt) = exit_endpoint {
			if let ExitEndpoint::Shared(ep) = opt {
				Some(ep)
			}else{
				private_exit_endpoint = true;
				let ep = kobj_alloc.cspace().allocate_slot_with_object_fixed::<Endpoint, _>(&kobj_alloc);
				if let Err(err) = ep {
					return Err(SysThreadError::Base(ThreadError::CSpaceAllocationError { details: err }));
				}
				Some(ep.unwrap())
			}
		}else{
			Some(Endpoint::from_cap(0))
		};

		let inner = LocalThread::new(common_config, local_config, sched_params, &kobj_alloc)
			.or_else(|err| { Err(SysThreadError::Base(err)) })?;

		let vspace = get_root_vspace();
		if fs_context.is_none() {
			fs_context = Some(get_root_default_fs_context());
		}

		Ok(SysThread {
			inner: InnerThread::Root(inner),
			pid,
			tid: base_tid,
			fs_context,
			vspace: Some(vspace),
			private_exit_endpoint,
		})

	}
	///Gets the process ID of this thread
	pub fn get_pid(&self) -> i32 {
		self.pid
	}
	///Gets the ID of this thread
	pub fn get_tid(&self) -> i32 {
		self.tid
	}
	///Gets the global ID of this thread
	pub fn get_gtid(&self) -> u64 {
		get_gtid(self.pid, self.tid)
	}
	///Sets the core on which this thread runs
	pub fn set_core(&mut self, core: usize) -> Result<(), SysThreadError>{
		let mut sched_params = self.get_sched_params().expect("no scheduler parameters for thread (this shouldn't happen)");
		sched_params.core = core;
		let kobj_alloc = get_kobj_alloc();
		base_to_sys(self.set_sched_params(sched_params, &kobj_alloc))
	}
	///Runs this thread with the given closure if it is a root server
	///thread
	///
	///Always fails if this is not a root server thread
	pub fn run<F>(&mut self, f: F) -> Result<(), SysThreadError>
			where F: FnOnce() -> Option<ThreadReturn> + Send + 'static {
		let inner = if let InnerThread::Root(ref mut i) = self.inner {
			i
		}else{
			return Err(SysThreadError::Base(ThreadError::InternalError));
		};
		//info!("tid: {}", self.tid);
		let tid = self.tid;
		let name = String::from(inner.get_name());
		base_to_sys(inner.run(move || {
			unsafe { set_current_id_and_name(tid, name) };
			vfs_root_thread_init(get_root_fd_arrays());
			let ret = f();
			unsafe { unset_thread_name() };
			ret
		}))
	}
	///Gets the exit endpoint of this thread (only for root server
	///threads)
	pub fn get_exit_endpoint(&self) -> Option<Endpoint> {
		match self.inner {
			InnerThread::Root(ref inner) => inner.get_exit_endpoint(),
			_ => None,
		}
	}
	///Gets the FDSpace associated with this thread
	pub fn get_fs_context(&self) -> Option<Arc<FSContext>> {
		self.fs_context.clone()
	}
	///Gets the FDSpace associated with this thread
	pub fn get_fdspace(&self) -> Option<Arc<FDSpaceContainer>> {
		if let Some(ref fs_context) = self.fs_context {
			Some(fs_context.get_fdspace())
		}else{
			None
		}
	}
	///Gets the root mount table of the FSSpace associated with this thread
	pub fn get_fsspace_root(&self) -> Option<Arc<MountTableContainer>> {
		if let Some(ref fs_context) = self.fs_context {
			Some(fs_context.get_fsspace_root())
		}else{
			None
		}
	}
}

impl Drop for SysThread {
	fn drop(&mut self) {
		let common_config;
		let kobj_alloc = get_kobj_alloc();
		match self.inner {
			InnerThread::Kernel(_) => panic!("attempted to drop kernel thread"),
			InnerThread::RootInit(_) => panic!("attempted to drop root server initial thread"),
			InnerThread::Root(ref mut inner) => {
				//info!("dropping thread with ID {}", self.id);
				if let Some(endpoint) = inner.get_exit_endpoint() && self.private_exit_endpoint {
					kobj_alloc.cspace().free_and_delete_slot_with_object_fixed(&endpoint, &kobj_alloc).expect("failed to deallocate exit endpoint");
				}
				common_config = inner.get_space();
				inner.deallocate_objects(&kobj_alloc).expect("failed to deallocate thread");

			},
			InnerThread::User(ref mut inner) => {
				common_config = inner.get_space();
				inner.deallocate_objects(&kobj_alloc).expect("failed to deallocate thread");
			},
		}
		if let Some(config) = common_config {
			let cap = config.fault_endpoint.to_cap();
			if cap != 0 {
				kobj_alloc.cspace().free_and_delete_slot_raw(cap, &kobj_alloc).expect("failed to deallocate fault endpoint");
			}
		}
	}
}

impl Task for SysThread {
}

impl fmt::Debug for SysThread {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "[SysThread: id: {}, name: {:?}]", self.tid, self.get_name())
	}
}

#[thread_local]
static mut THREAD_ID: i32 = 0;

///Gets the ID of the calling thread
pub fn get_current_tid() -> i32 {
	unsafe { THREAD_ID }
}

#[thread_local]
static mut THREAD_NAME: Option<String> = None;

///Gets the name of the calling thread
pub fn get_current_name() -> Option<&'static str> {
	unsafe {
		if let Some(name) = THREAD_NAME.as_ref(){
			Some(&name)
		}else if !task_state_initialized() {
			Some(&INIT_THREAD_NAME)
		}else{
			None
		}
	}
}

///Sets the ID and name returned by the corresponding getter functions
pub unsafe fn set_current_id_and_name(id: i32, name: String){
	THREAD_ID = id;
	THREAD_NAME.replace(name);
}

///Unsets the name returned by get_current_name()
pub unsafe fn unset_thread_name(){
	THREAD_NAME = None;
}

///Adds thread-related slabs
pub fn add_custom_slabs() {
}
/* vim: set softtabstop=8 tabstop=8 noexpandtab: */
