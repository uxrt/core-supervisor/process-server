/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///Thread pools (currently only for I/O threads)

use alloc::sync::Arc;

use sparse_array::{
	SparseArray,
	add_custom_slabs_nonalloc
};

use sel4::Endpoint;

use sel4_thread::{
	ThreadError,
	ThreadReturn,
	WrappedThread,
};
use usync::{
	Mutex,
	MutexGuard,
	RwLock,
};

use uxrt_thread_pool::{
	PoolThread,
	ResMgrThreadPool,
	ThreadPool,
};
use uxrt_transport_layer::IOError;
use uxrt_vfs_resmgr::{
	DispatchContext,
	DispatchContextFactory,
	OpenControlBlock,
	ResMgrContextExtra,
	ResMgrMsgHandler,
	ResMgrPort,
};

use crate::global_heap_alloc::get_kobj_alloc;
use sel4_alloc::AllocatorBundle;
use sel4_alloc::cspace::CSpaceManager;

use crate::task::get_task_tree;
use crate::task::thread::{
	ExitEndpoint,
	SysThread,
	SysThreadError,
};

use crate::utils::IOPoolAllocator;

const MIN_IO_THREADS: usize = 16;
const ALLOC_STEP: usize = 4;

///A thread pool intended for running blocking I/O-bound jobs like polling or
///sending clunk messages on shared channels. Unlike thread pools intended for
///more CPU-intensive jobs, this one always runs any job submitted to it
///immediately, adding more threads if all threads are busy (putting jobs into a
///queue like many other thread pools would break the kinds of jobs for which
///this is meant to be used).
pub struct IOThreadPoolAllocator {
	contents: SparseArray<Option<Arc<RwLock<SysThread>>>>,
	reclaim_thread: Arc<RwLock<SysThread>>,
	exit_endpoint: Endpoint,
}

impl IOThreadPoolAllocator {
	///Internal constructor (only one instance of this is needed)
	fn new() -> Self {
		let kobj_alloc = get_kobj_alloc();
		let exit_endpoint = kobj_alloc.cspace().allocate_slot_with_object_fixed::<Endpoint, _>(&kobj_alloc).expect("could not allocated exit endpoint for I/O pool threads");
		let reclaim_thread = get_task_tree().new_root_thread(ExitEndpoint::shared(exit_endpoint)).expect("could not create I/O pool reclaimer thread");
		let mut pool = IOThreadPoolAllocator {
			contents: SparseArray::new(),
			reclaim_thread,
			exit_endpoint,
		};
		pool.add_items(MIN_IO_THREADS).expect("could not create initial I/O pool threads");
		let mut t = pool.reclaim_thread.write();
		t.set_name("io_thread_reclaimer");
		t.run(move || {
			Self::reclaim_loop(exit_endpoint);
		}).expect("could not start I/O pool reclaimer thread");
		drop(t);
		pool
	}
	///Main loop of the reclaim thread
	fn reclaim_loop(endpoint: Endpoint) -> !{
		loop {
			//info!("IOThreadPool::reclaim_loop: waiting for message");
			let msg = endpoint.recv_refuse_reply();
			let mut data = [0usize; 1];
			if msg.get_data(&mut data).is_err() {
				warn!("IOThreadPool::reclaim_loop: invalid data received");
				continue;
			}
			let thread_id = data[0];
			//info!("IOThreadPool::reclaim_loop: deallocating thread {}", thread_id);
			let mut pool = get_io_pool();
			pool.reclaim_thread(thread_id);
		}
	}
	///Marks a single idle thread as available
	fn reclaim_thread(&mut self, id: usize){
		if let Some(t) = self.contents.get_hidden(id) {
			let mut thread = t.write();
			thread.set_name("io_pool_idle");
			let suspend_res = thread.suspend();
			drop(thread);
			if let Err(err) = suspend_res {
				warn!("failed to suspend I/O pool thread {}: {:?}", id, err);
			}else{
				self.deallocate(id);
			}
		}
	}
	///Run a function in one of the threads in the pool
	pub fn run<F>(&mut self, f: F, name: &str) -> Result<(), SysThreadError>
			where F: FnOnce() + Send + 'static {
		//info!("IOThreadPool::run");
		let (tid, opt) = self.allocate()?;
		let t = opt.expect("no threads available in pool (this shouldn't happen)");
		//info!("IOThreadPool::run: got thread");
		let mut thread = t.write();
		thread.set_name(name);
		thread.run(move ||{
			//info!("I/O pool thread {} started", tid);
			f();
			//info!("I/O pool thread {} finished", tid);
			Some(ThreadReturn::Word(tid))
		})?;
		//info!("IOThreadPool::run: done");
		Ok(())
	}
}

impl IOPoolAllocator<Arc<RwLock<SysThread>>, SysThreadError> for IOThreadPoolAllocator {
	fn get_alloc_step(&self) -> usize {
		ALLOC_STEP
	}
	fn get_min_items(&self) -> usize {
		MIN_IO_THREADS
	}
	fn get_contents(&mut self) -> &mut SparseArray<Option<Arc<RwLock<SysThread>>>> {
		&mut self.contents
	}
	///Internal method to add threads to the pool
	fn allocate_item(&mut self) -> Result<(usize, Arc<RwLock<SysThread>>), SysThreadError> {
		let task_tree = get_task_tree();
		let thread = task_tree.new_root_thread(ExitEndpoint::shared(self.exit_endpoint))?;
		let mut guard = thread.write();
		let tid = guard.get_tid() as usize;
		guard.set_name("io_pool_idle");
		drop(guard);
		Ok((tid, thread))
	}
	fn free_item(&mut self, id: usize) {
		let task_tree = get_task_tree();
		if let Err(err) = task_tree.delete_root_thread(id as i32) {
			warn!("could not delete I/O pool thread with ID {}: {:?}", id, err);
		}
	}

}

static mut IO_THREAD_POOL: Option<Mutex<IOThreadPoolAllocator>> = None;

///Get the I/O thread pool
pub fn get_io_pool() -> MutexGuard<'static, IOThreadPoolAllocator> {
	unsafe { IO_THREAD_POOL.as_ref().expect("I/O thread pool unset").lock() }
}

///Run a thread in the I/O pool
pub fn run_io_thread<F>(f: F, name: &str) -> Result<(), SysThreadError>
		where F: FnOnce() + Send + 'static {
	//info!("run_io_thread: {:p}", &f);
	get_io_pool().run(f, name)
}

///Initialize thread pools
pub fn init_thread_pools() {
	unsafe { IO_THREAD_POOL.replace(Mutex::new(IOThreadPoolAllocator::new())) };
}

pub struct RootPoolThread {
}

impl PoolThread for RootPoolThread {
	fn new() -> Self {
		Self {}
	}
	fn run<F>(&mut self, f: F, name: &str) -> Result<(), ()>
		where F: FnOnce() + Send + 'static {
			if run_io_thread(f, name).is_ok() {
				Ok(())
			}else{
				Err(())
			}
	}

}

pub type RootThreadPool<F, C> = ThreadPool<F, RootPoolThread, C>;

///Convenience function to create a new thread pool for non-resource-manager
///handlers
pub fn new_thread_pool<F: DispatchContextFactory<C> + Send + Sync + 'static, C: DispatchContext + 'static>(factory: F, low_water: usize, high_water: usize, max: usize, increment: usize, thread_name: &str) -> Result<Arc<RootThreadPool<F, C>>, IOError> {
	ThreadPool::<F, RootPoolThread, C>::new(factory, low_water, high_water, max, increment, thread_name)
}

///Convenience function to create a new thread pool for resource managers
pub fn new_resmgr_thread_pool<O: OpenControlBlock + Send + 'static, H: ResMgrMsgHandler<O, E> + Send + Sync + 'static, E: ResMgrContextExtra + 'static>(port: &Arc<ResMgrPort<O, H, E>>, low_water: usize, high_water:  usize, max: usize, increment: usize, thread_name: &str) -> Result<ResMgrThreadPool<O, H, E, RootPoolThread>, IOError> {
	ResMgrThreadPool::<O, H, E, RootPoolThread>::new(port, low_water, high_water, max, increment, thread_name)
}

///Adds custom slabs related to thread pools
pub fn add_custom_slabs(){
	let kobj_alloc = get_kobj_alloc();
	add_custom_slabs_nonalloc::<Option<Arc<RwLock<SysThread>>>, _>(&kobj_alloc, "thread pool");
}

/* vim: set softtabstop=8 tabstop=8 noexpandtab: */
