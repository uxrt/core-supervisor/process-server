/*
 * Copyright (c) 2022-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */
///Miscellaneous utility functions, structs, and traits

use core::sync::atomic::{
	AtomicI32,
	Ordering,
};
use core::mem::size_of;
use alloc::sync::Arc;
use alloc::boxed::Box;
use custom_slab_allocator::CustomSlabAllocator;
use crate::global_heap_alloc::get_kobj_alloc;
use sparse_array::SparseArray;
use intrusive_collections::rbtree::Iter;
use intrusive_collections::{
	KeyAdapter,
	RBTree,
	RBTreeAtomicLink,
	UnsafeRef
};
use global_id_allocator::GlobalIDAllocator;
use usync::{
	RwLock,
	RwLockReadGuard,
	RwLockWriteGuard,
};

///Trait for I/O pool allocators such as the thread allocator
pub trait IOPoolAllocator<T: Clone, E> {
	///Returns the number of items to allocate at once
	fn get_alloc_step(&self) -> usize;
	///Returns the minimum number of items this pool should contain
	fn get_min_items(&self) -> usize;
	///Returns the `SparseArray` with the contents of the pool
	fn get_contents(&mut self) -> &mut SparseArray<Option<T>>;
	///Allocates a single item from the pool
	fn allocate_item(&mut self) -> Result<(usize, T), E>;
	///Frees a single item from the pool
	fn free_item(&mut self, id: usize);
	///Internal method to add items to the pool
	fn add_items(&mut self, count: usize) -> Result<(), E>{
		for _ in 0..count {
			let (id, item) = self.allocate_item()?;
			self.get_contents().put(id, Some(item));
		}
		Ok(())
	}

	///Allocate an item from the pool
	fn allocate(&mut self) -> Result<(usize, Option<T>), E> {
		if self.get_contents().visible_len() == 0 {
			self.add_items(self.get_alloc_step())?;
		}
		Ok(self.get_contents().hide_first())
	}
	///Deallocate an item from the pool
	fn deallocate(&mut self, id: usize) {
		self.get_contents().show(id);
		if self.get_contents().len() > self.get_min_items() && self.get_contents().visible_len() > self.get_alloc_step() {
			for _ in 0..self.get_contents().visible_len() - self.get_alloc_step() {
				let (free_id, _) = self.get_contents().take_first();
				self.free_item(free_id);
			}
		}
	}
}

///Adds a slab size for an Arc of the given type
///
///This is a macro since it uses a fixed array as a substitute, and it is not
///possible to use the size of a generic type as a constant to define/initialize
///an array (which would require generic statics)
#[macro_export]
macro_rules! add_arc_slab {
	($slab_type: ty, $slab_size: expr, $min_free: expr, $max_dealloc_slabs: expr, $max_drop_rounds: expr, $description: expr) => {
		{
			use crate::global_heap_alloc::get_kobj_alloc;
			let kobj_alloc = get_kobj_alloc();
			custom_slab_allocator::add_arc_slab!(&kobj_alloc, $slab_type, $slab_size, $min_free, $max_dealloc_slabs, $max_drop_rounds, $description);
		}
	}
}

pub const ARC_NODE_SLAB_SIZE: usize = 512;
///Adds custom slabs for an `ArcList` (one for the list nodes and one for the items they contain)
#[macro_export]
macro_rules! add_arc_list_slab {
	($slab_type: ty, $slab_size: expr, $min_free:expr, $max_dealloc_slabs: expr, $max_drop_rounds: expr, $name: expr) => {
		{
			crate::utils::add_slab::<crate::utils::ArcListNode<$slab_type>>(crate::utils::ARC_NODE_SLAB_SIZE, $min_free, $max_dealloc_slabs, $max_drop_rounds, $name);
			crate::add_arc_slab!($slab_type, $slab_size, $min_free, $max_dealloc_slabs, $max_drop_rounds, $name);
		}
	}
}

impl<T: ArcListItem> GlobalIDAllocator for ArcList<T> {
	fn has_id(&self, id: i32) -> bool {
		!self.contents.find(&id).is_null()
	}
	fn get_next_id(&self) -> i32 {
		self.next_id.load(Ordering::Relaxed)
	}
	fn increment_id(&self) -> i32 {
		self.next_id.fetch_add(1, Ordering::Relaxed)
	}
}

///Trait for items in an ArcList to get their IDs
pub trait ArcListItem {
	///Gets the ID of this item
	fn get_id(&self) -> i32;
	///Sets the ID of this item (called when adding to the list)
	fn set_id(&mut self, id: i32);
}

intrusive_adapter!(ArcListAdapter<T> = UnsafeRef<ArcListNode<T>>: ArcListNode<T> { link: RBTreeAtomicLink } where T: ArcListItem );

impl<'a, T: ArcListItem> KeyAdapter<'a> for ArcListAdapter<T>{
	type Key = i32;
	fn get_key(&self, node: &'a ArcListNode<T>) -> i32 {
		node.contents.get_id()
	}
}


///Container for items in an `ArcList`, in order to allow easily giving out
///`Arc`s to them while in a tree
pub struct ArcListNode<T: ArcListItem> {
	contents: Arc<T>,
	link: RBTreeAtomicLink,
}

impl<T: ArcListItem> ArcListNode<T> {
	///Creates a new VSpaceContainer
	fn new(contents: Arc<T>) -> UnsafeRef<ArcListNode<T>> {
		UnsafeRef::from_box(Box::new(ArcListNode {
			contents,
			link: Default::default(),
		}))
	}
}

///Wrapper for ArcList that handles locking
pub struct LockedArcList<T: ArcListItem> {
	contents: RwLock<ArcList<T>>,
}

impl<T: ArcListItem> LockedArcList<T> {
	///Creates a new VSpaceList
	pub fn new() -> LockedArcList<T> {
		LockedArcList {
			contents: RwLock::new(ArcList::new()),
		}
	}
	///Adds an item to the list
	pub fn add(&self, item: T) -> Result<(i32, Arc<T>), ()> {
		self.contents.write().add(item)
	}
	///Gets an item from the list
	pub fn get(&self, id: i32) -> Option<Arc<T>> {
		self.contents.read().get(id)
	}
	///Removes an item from the list
	pub fn remove(&self, id: i32) -> Result<Arc<T>, ()> {
		self.contents.write().remove(id)
	}
	///Locks the wrapped list for reading and returns a guard
	pub fn read(&self) -> RwLockReadGuard<'_, ArcList<T>> {
		self.contents.read()
	}
	///Locks the wrapped list for writing and returns a guard
	pub fn write(&self) -> RwLockWriteGuard<'_, ArcList<T>> {
		self.contents.write()
	}
}

///The list of all VSpaces in the system.
pub struct ArcList<T: ArcListItem> {
	next_id: AtomicI32,
	contents: RBTree<ArcListAdapter<T>>,
}

impl<T: ArcListItem> ArcList<T> {
	///Creates a new VSpaceList
	pub fn new() -> ArcList<T> {
		ArcList {
			next_id: AtomicI32::new(0),
			contents: Default::default(),
		}
	}
	///Adds an item to the list
	pub fn add(&mut self, mut item: T) -> Result<(i32, Arc<T>), ()> {
		let id = self.allocate_id()?;
		item.set_id(id);
		let arc = Arc::new(item);
		self.contents.insert(ArcListNode::new(arc.clone()));
		Ok((id, arc))
	}
	///Gets an item from the list
	pub fn get(&self, id: i32) -> Option<Arc<T>> {
		if let Some(wrapper) = self.contents.find(&id).get(){
			Some(wrapper.contents.clone())
		}else{
			None
		}
	}
	///Removes an item from the list
	pub fn remove(&mut self, id: i32) -> Result<Arc<T>, ()> {
		let mut cursor = self.contents.find_mut(&id);
		if let Some(wrapper) = cursor.remove(){
			let arc = wrapper.contents.clone();
			unsafe { drop(UnsafeRef::into_box(wrapper)) };
			Ok(arc)
		}else{
			Err(())
		}
	}
	///Gets an iterator over the contents of the list
	pub fn iter<'a>(&'a self) -> ArcListIter<'a, T> {
		ArcListIter(self.contents.iter())
	}
}

pub struct ArcListIter<'a, T: ArcListItem>(Iter<'a, ArcListAdapter<T>>);

impl<'a, T: ArcListItem> Iterator for ArcListIter<'a, T> {
	type Item = &'a T;
	fn next(&mut self) -> Option<&'a T> {
		Some(&self.0.next()?.contents)
	}
}

///Adds a slab to the heap allocator
pub fn add_slab<T: Sized>(slab_size: usize, min_free: usize, max_dealloc_slabs: usize, max_drop_rounds: usize, name: &str) {
	let kobj_alloc = get_kobj_alloc();
	kobj_alloc.add_custom_slab(size_of::<T>(), slab_size, min_free, max_dealloc_slabs, max_drop_rounds, name);
}

/* vim: set softtabstop=8 tabstop=8 noexpandtab:: */
