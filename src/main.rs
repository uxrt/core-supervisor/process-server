/*
 * Copyright (c) 2018-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 */

#![no_std]

#![feature(allocator_api, core_intrinsics)]
#![feature(thread_local)]
#![feature(let_chains)]
#![feature(round_char_boundary)]
#![feature(trait_alias)]
#[macro_use]
extern crate alloc;

#[macro_use]
extern crate static_assertions;

#[macro_use]
extern crate intrusive_collections;

extern crate arrayvec;
extern crate bilge;
extern crate sparse_array;
extern crate bitmap;
extern crate sel4_sys;
extern crate sel4_start;
#[macro_use]
extern crate sel4;
#[macro_use]
extern crate sel4_alloc;
extern crate usync;
extern crate usync_once_cell;
#[macro_use]
extern crate slab_allocator_core;
#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate scan_fmt;

extern crate global_id_allocator;

extern crate dir_map;
extern crate uxrt_transport_layer;
extern crate uxrt_vfs_resmgr;
extern crate uxrt_vfs_rpc;
extern crate uxrt_vfs_rpc_client;

#[macro_use]
mod logger;

mod bootinfo;
mod drivers;
mod task;
mod vfs;
mod vm;
mod utils;

mod tests;

use bootinfo::process_bootinfo;
use sel4_alloc::heap::NUM_GENERIC_SLABS;
use sel4_alloc::AllocatorBundle;

mod bootstrap_heap {
	use sel4::PAGE_SIZE;
	use sel4_alloc::heap::NUM_GENERIC_SLABS;
	const SCRATCH_LEN_BYTES: usize = 1712128;
	bootstrap_heap!(SCRATCH_LEN_BYTES, PAGE_SIZE);
}

use bootstrap_heap::bootstrap_heap_info;

mod global_heap_alloc {
	use sel4_alloc::{
		cspace::SwitchingAllocator,
		vspace::Hier,
	};
	use crate::vm::ut_alloc::SwappingUtAllocator;
	global_alloc!((SwitchingAllocator, SwappingUtAllocator, Hier));
}
use global_heap_alloc::{
	GlobalSlabAllocator,
	get_kobj_alloc,
};

#[global_allocator]
static ALLOCATOR: GlobalSlabAllocator = GlobalSlabAllocator::new();

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

///Dumps the heap status to the log
pub fn dump_heap() {
	info!("{:?})", ALLOCATOR);
}

///Dumps the UTSpace status to the log
pub fn dump_utspace() {
	let kobj_alloc = get_kobj_alloc();
	let utspace_str = { format!("{:?}", kobj_alloc.utspace()) };
	drop(kobj_alloc);
	info!("{}", utspace_str);
}

const BOOTSTRAP_SLAB_SIZES: [usize; NUM_GENERIC_SLABS] = [
	256, //16
	128, //32
	512, //64
	512, //128
	128, //256
	128, //512
	128, //1024
	32,  //2048
	32,  //4096
	64,  //8192
	8,   //16384
	16,  //32768
];

pub fn add_custom_slabs() {
	task::add_custom_slabs();
	vfs::add_custom_slabs();
	vm::add_custom_slabs();
	drivers::add_custom_slabs();
}

pub fn main() {
	println!("\nUX/RT version {}", VERSION);
	logger::init().expect("failed to initialize logging");

	let bootinfo: &'static sel4_sys::seL4_BootInfo = unsafe { &*sel4_start::BOOTINFO };
	unsafe { ALLOCATOR.init(bootstrap_heap_info(BOOTSTRAP_SLAB_SIZES)); }

	let mbi_handler = process_bootinfo(bootinfo);
	vm::init_root_alloc(bootinfo, mbi_handler.user_start_addr, mbi_handler.user_end_addr, mbi_handler.root_server_end_addr);
	add_custom_slabs();

	vfs::init_fdspace_list();
	vfs::init_fsspaces();

	vfs::init_rpc_handler();
	task::init_task_tree(bootinfo, mbi_handler);
	task::thread_pool::init_thread_pools();

	vfs::start_rpc_handler();

	vm::fault::get_fault_handler().init();

	vfs::init_dir_handler();

	vfs::tmp_setup_mounts();

	drivers::start_resmgr_drivers();

	vfs::init_cwd();

	#[cfg(feature = "test_task")]
	tests::task::test_threads_base();
	#[cfg(any(feature = "test_task", feature = "test_vfs"))]
	let mut local_threads = tests::create_local_test_threads();
	#[cfg(feature = "test_task")]
	tests::task::test_threads_local(&mut local_threads);
	#[cfg(feature = "test_vfs")]
	tests::vfs::test_vfs(&mut local_threads);
	#[cfg(any(feature = "test_task", feature = "test_vfs"))]
	tests::deallocate_local_threads(&mut local_threads);

	info!("processes after initialization:");
	task::get_task_tree().dump_processes();

	vfs::rpc::dump_ports();
	info!("end of main reached; halting");
	loop {
		unsafe { sel4_sys::seL4_DebugHalt() };
	}
}

/* vim: set softtabstop=8 tabstop=8 noexpandtab: */
